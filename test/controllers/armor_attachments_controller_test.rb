require 'test_helper'

class ArmorAttachmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @armor_attachment = armor_attachments(:one)
  end

  test "should get index" do
    get armor_attachments_url
    assert_response :success
  end

  test "should get new" do
    get new_armor_attachment_url
    assert_response :success
  end

  test "should create armor_attachment" do
    assert_difference('ArmorAttachment.count') do
      post armor_attachments_url, params: { armor_attachment: { description: @armor_attachment.description, hard_points: @armor_attachment.hard_points, modifiers: @armor_attachment.modifiers, name: @armor_attachment.name, price: @armor_attachment.price, rarity: @armor_attachment.rarity, use_with: @armor_attachment.use_with } }
    end

    assert_redirected_to armor_attachment_url(ArmorAttachment.last)
  end

  test "should show armor_attachment" do
    get armor_attachment_url(@armor_attachment)
    assert_response :success
  end

  test "should get edit" do
    get edit_armor_attachment_url(@armor_attachment)
    assert_response :success
  end

  test "should update armor_attachment" do
    patch armor_attachment_url(@armor_attachment), params: { armor_attachment: { description: @armor_attachment.description, hard_points: @armor_attachment.hard_points, modifiers: @armor_attachment.modifiers, name: @armor_attachment.name, price: @armor_attachment.price, rarity: @armor_attachment.rarity, use_with: @armor_attachment.use_with } }
    assert_redirected_to armor_attachment_url(@armor_attachment)
  end

  test "should destroy armor_attachment" do
    assert_difference('ArmorAttachment.count', -1) do
      delete armor_attachment_url(@armor_attachment)
    end

    assert_redirected_to armor_attachments_url
  end
end
