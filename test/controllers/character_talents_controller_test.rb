require 'test_helper'

class CharacterTalentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @character_talent = character_talents(:one)
  end

  test "should get index" do
    get character_talents_url
    assert_response :success
  end

  test "should get new" do
    get new_character_talent_url
    assert_response :success
  end

  test "should create character_talent" do
    assert_difference('CharacterTalent.count') do
      post character_talents_url, params: { character_talent: { character_id: @character_talent.character_id, options: @character_talent.options, talent_id: @character_talent.talent_id } }
    end

    assert_redirected_to character_talent_url(CharacterTalent.last)
  end

  test "should show character_talent" do
    get character_talent_url(@character_talent)
    assert_response :success
  end

  test "should get edit" do
    get edit_character_talent_url(@character_talent)
    assert_response :success
  end

  test "should update character_talent" do
    patch character_talent_url(@character_talent), params: { character_talent: { character_id: @character_talent.character_id, options: @character_talent.options, talent_id: @character_talent.talent_id } }
    assert_redirected_to character_talent_url(@character_talent)
  end

  test "should destroy character_talent" do
    assert_difference('CharacterTalent.count', -1) do
      delete character_talent_url(@character_talent)
    end

    assert_redirected_to character_talents_url
  end
end
