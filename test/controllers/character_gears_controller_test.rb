require 'test_helper'

class CharacterGearsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @character_gear = character_gears(:one)
  end

  test "should get index" do
    get character_gears_url
    assert_response :success
  end

  test "should get new" do
    get new_character_gear_url
    assert_response :success
  end

  test "should create character_gear" do
    assert_difference('CharacterGear.count') do
      post character_gears_url, params: { character_gear: { carried: @character_gear.carried, character_id: @character_gear.character_id, equipped: @character_gear.equipped, gear_id: @character_gear.gear_id, location: @character_gear.location, modifications: @character_gear.modifications, packed: @character_gear.packed, qty: @character_gear.qty } }
    end

    assert_redirected_to character_gear_url(CharacterGear.last)
  end

  test "should show character_gear" do
    get character_gear_url(@character_gear)
    assert_response :success
  end

  test "should get edit" do
    get edit_character_gear_url(@character_gear)
    assert_response :success
  end

  test "should update character_gear" do
    patch character_gear_url(@character_gear), params: { character_gear: { carried: @character_gear.carried, character_id: @character_gear.character_id, equipped: @character_gear.equipped, gear_id: @character_gear.gear_id, location: @character_gear.location, modifications: @character_gear.modifications, packed: @character_gear.packed, qty: @character_gear.qty } }
    assert_redirected_to character_gear_url(@character_gear)
  end

  test "should destroy character_gear" do
    assert_difference('CharacterGear.count', -1) do
      delete character_gear_url(@character_gear)
    end

    assert_redirected_to character_gears_url
  end
end
