require 'test_helper'

class AdversariesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @adversary = adversaries(:one)
  end

  test "should get index" do
    get adversaries_url
    assert_response :success
  end

  test "should get new" do
    get new_adversary_url
    assert_response :success
  end

  test "should create adversary" do
    assert_difference('Adversary.count') do
      post adversaries_url, params: { adversary: { abilities: @adversary.abilities, agility: @adversary.agility, brawn: @adversary.brawn, cunning: @adversary.cunning, defense_melee: @adversary.defense_melee, defense_ranged: @adversary.defense_ranged, description: @adversary.description, equipment: @adversary.equipment, intellect: @adversary.intellect, name: @adversary.name, presence: @adversary.presence, skills: @adversary.skills, soak: @adversary.soak, spells: @adversary.spells, strain_th: @adversary.strain_th, talents: @adversary.talents, type: @adversary.type, willpower: @adversary.willpower, wound_th: @adversary.wound_th } }
    end

    assert_redirected_to adversary_url(Adversary.last)
  end

  test "should show adversary" do
    get adversary_url(@adversary)
    assert_response :success
  end

  test "should get edit" do
    get edit_adversary_url(@adversary)
    assert_response :success
  end

  test "should update adversary" do
    patch adversary_url(@adversary), params: { adversary: { abilities: @adversary.abilities, agility: @adversary.agility, brawn: @adversary.brawn, cunning: @adversary.cunning, defense_melee: @adversary.defense_melee, defense_ranged: @adversary.defense_ranged, description: @adversary.description, equipment: @adversary.equipment, intellect: @adversary.intellect, name: @adversary.name, presence: @adversary.presence, skills: @adversary.skills, soak: @adversary.soak, spells: @adversary.spells, strain_th: @adversary.strain_th, talents: @adversary.talents, type: @adversary.type, willpower: @adversary.willpower, wound_th: @adversary.wound_th } }
    assert_redirected_to adversary_url(@adversary)
  end

  test "should destroy adversary" do
    assert_difference('Adversary.count', -1) do
      delete adversary_url(@adversary)
    end

    assert_redirected_to adversaries_url
  end
end
