require 'test_helper'

class AdventureLogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @adventure_log = adventure_logs(:one)
  end

  test "should get index" do
    get adventure_logs_url
    assert_response :success
  end

  test "should get new" do
    get new_adventure_log_url
    assert_response :success
  end

  test "should create adventure_log" do
    assert_difference('AdventureLog.count') do
      post adventure_logs_url, params: { adventure_log: { character_id: @adventure_log.character_id, date: @adventure_log.date, experience: @adventure_log.experience, log: @adventure_log.log, user_id: @adventure_log.user_id } }
    end

    assert_redirected_to adventure_log_url(AdventureLog.last)
  end

  test "should show adventure_log" do
    get adventure_log_url(@adventure_log)
    assert_response :success
  end

  test "should get edit" do
    get edit_adventure_log_url(@adventure_log)
    assert_response :success
  end

  test "should update adventure_log" do
    patch adventure_log_url(@adventure_log), params: { adventure_log: { character_id: @adventure_log.character_id, date: @adventure_log.date, experience: @adventure_log.experience, log: @adventure_log.log, user_id: @adventure_log.user_id } }
    assert_redirected_to adventure_log_url(@adventure_log)
  end

  test "should destroy adventure_log" do
    assert_difference('AdventureLog.count', -1) do
      delete adventure_log_url(@adventure_log)
    end

    assert_redirected_to adventure_logs_url
  end
end
