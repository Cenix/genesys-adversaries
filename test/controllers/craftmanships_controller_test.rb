require 'test_helper'

class CraftmanshipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @craftmanship = craftmanships(:one)
  end

  test "should get index" do
    get craftmanships_url
    assert_response :success
  end

  test "should get new" do
    get new_craftmanship_url
    assert_response :success
  end

  test "should create craftmanship" do
    assert_difference('Craftmanship.count') do
      post craftmanships_url, params: { craftmanship: { armor_mods: @craftmanship.armor_mods, description: @craftmanship.description, name: @craftmanship.name, price_multiplier: @craftmanship.price_multiplier, rarity_adjustment: @craftmanship.rarity_adjustment, weapon_mods: @craftmanship.weapon_mods } }
    end

    assert_redirected_to craftmanship_url(Craftmanship.last)
  end

  test "should show craftmanship" do
    get craftmanship_url(@craftmanship)
    assert_response :success
  end

  test "should get edit" do
    get edit_craftmanship_url(@craftmanship)
    assert_response :success
  end

  test "should update craftmanship" do
    patch craftmanship_url(@craftmanship), params: { craftmanship: { armor_mods: @craftmanship.armor_mods, description: @craftmanship.description, name: @craftmanship.name, price_multiplier: @craftmanship.price_multiplier, rarity_adjustment: @craftmanship.rarity_adjustment, weapon_mods: @craftmanship.weapon_mods } }
    assert_redirected_to craftmanship_url(@craftmanship)
  end

  test "should destroy craftmanship" do
    assert_difference('Craftmanship.count', -1) do
      delete craftmanship_url(@craftmanship)
    end

    assert_redirected_to craftmanships_url
  end
end
