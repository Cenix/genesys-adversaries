require 'test_helper'

class CharactersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @character = characters(:one)
  end

  test "should get index" do
    get characters_url
    assert_response :success
  end

  test "should get new" do
    get new_character_url
    assert_response :success
  end

  test "should create character" do
    assert_difference('Character.count') do
      post characters_url, params: { character: { age: @character.age, agility: @character.agility, archetype_id: @character.archetype_id, background: @character.background, brawn: @character.brawn, build: @character.build, career_id: @character.career_id, cunning: @character.cunning, desire_text: @character.desire_text, eyes: @character.eyes, fear_text: @character.fear_text, flaw_text: @character.flaw_text, gender: @character.gender, hair: @character.hair, height: @character.height, image_url: @character.image_url, intellect: @character.intellect, name: @character.name, notable_features: @character.notable_features, presence: @character.presence, slug: @character.slug, strain_current: @character.strain_current, strength_text: @character.strength_text, user_id: @character.user_id, weight: @character.weight, willpower: @character.willpower, wounds_current: @character.wounds_current } }
    end

    assert_redirected_to character_url(Character.last)
  end

  test "should show character" do
    get character_url(@character)
    assert_response :success
  end

  test "should get edit" do
    get edit_character_url(@character)
    assert_response :success
  end

  test "should update character" do
    patch character_url(@character), params: { character: { age: @character.age, agility: @character.agility, archetype_id: @character.archetype_id, background: @character.background, brawn: @character.brawn, build: @character.build, career_id: @character.career_id, cunning: @character.cunning, desire_text: @character.desire_text, eyes: @character.eyes, fear_text: @character.fear_text, flaw_text: @character.flaw_text, gender: @character.gender, hair: @character.hair, height: @character.height, image_url: @character.image_url, intellect: @character.intellect, name: @character.name, notable_features: @character.notable_features, presence: @character.presence, slug: @character.slug, strain_current: @character.strain_current, strength_text: @character.strength_text, user_id: @character.user_id, weight: @character.weight, willpower: @character.willpower, wounds_current: @character.wounds_current } }
    assert_redirected_to character_url(@character)
  end

  test "should destroy character" do
    assert_difference('Character.count', -1) do
      delete character_url(@character)
    end

    assert_redirected_to characters_url
  end
end
