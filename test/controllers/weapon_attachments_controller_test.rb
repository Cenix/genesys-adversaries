require 'test_helper'

class WeaponAttachmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @weapon_attachment = weapon_attachments(:one)
  end

  test "should get index" do
    get weapon_attachments_url
    assert_response :success
  end

  test "should get new" do
    get new_weapon_attachment_url
    assert_response :success
  end

  test "should create weapon_attachment" do
    assert_difference('WeaponAttachment.count') do
      post weapon_attachments_url, params: { weapon_attachment: { description: @weapon_attachment.description, hard_points: @weapon_attachment.hard_points, modifiers: @weapon_attachment.modifiers, name: @weapon_attachment.name, price: @weapon_attachment.price, rarity: @weapon_attachment.rarity, use_with: @weapon_attachment.use_with } }
    end

    assert_redirected_to weapon_attachment_url(WeaponAttachment.last)
  end

  test "should show weapon_attachment" do
    get weapon_attachment_url(@weapon_attachment)
    assert_response :success
  end

  test "should get edit" do
    get edit_weapon_attachment_url(@weapon_attachment)
    assert_response :success
  end

  test "should update weapon_attachment" do
    patch weapon_attachment_url(@weapon_attachment), params: { weapon_attachment: { description: @weapon_attachment.description, hard_points: @weapon_attachment.hard_points, modifiers: @weapon_attachment.modifiers, name: @weapon_attachment.name, price: @weapon_attachment.price, rarity: @weapon_attachment.rarity, use_with: @weapon_attachment.use_with } }
    assert_redirected_to weapon_attachment_url(@weapon_attachment)
  end

  test "should destroy weapon_attachment" do
    assert_difference('WeaponAttachment.count', -1) do
      delete weapon_attachment_url(@weapon_attachment)
    end

    assert_redirected_to weapon_attachments_url
  end
end
