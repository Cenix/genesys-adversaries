require 'test_helper'

class ItemQualitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @item_quality = item_qualities(:one)
  end

  test "should get index" do
    get item_qualities_url
    assert_response :success
  end

  test "should get new" do
    get new_item_quality_url
    assert_response :success
  end

  test "should create item_quality" do
    assert_difference('ItemQuality.count') do
      post item_qualities_url, params: { item_quality: { activation: @item_quality.activation, description: @item_quality.description, name: @item_quality.name } }
    end

    assert_redirected_to item_quality_url(ItemQuality.last)
  end

  test "should show item_quality" do
    get item_quality_url(@item_quality)
    assert_response :success
  end

  test "should get edit" do
    get edit_item_quality_url(@item_quality)
    assert_response :success
  end

  test "should update item_quality" do
    patch item_quality_url(@item_quality), params: { item_quality: { activation: @item_quality.activation, description: @item_quality.description, name: @item_quality.name } }
    assert_redirected_to item_quality_url(@item_quality)
  end

  test "should destroy item_quality" do
    assert_difference('ItemQuality.count', -1) do
      delete item_quality_url(@item_quality)
    end

    assert_redirected_to item_qualities_url
  end
end
