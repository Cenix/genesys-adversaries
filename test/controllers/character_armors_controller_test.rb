require 'test_helper'

class CharacterArmorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @character_armor = character_armors(:one)
  end

  test "should get index" do
    get character_armors_url
    assert_response :success
  end

  test "should get new" do
    get new_character_armor_url
    assert_response :success
  end

  test "should create character_armor" do
    assert_difference('CharacterArmor.count') do
      post character_armors_url, params: { character_armor: { armor_id: @character_armor.armor_id, carried: @character_armor.carried, character_id: @character_armor.character_id, craftmanship_id: @character_armor.craftmanship_id, equipped: @character_armor.equipped, modifications: @character_armor.modifications } }
    end

    assert_redirected_to character_armor_url(CharacterArmor.last)
  end

  test "should show character_armor" do
    get character_armor_url(@character_armor)
    assert_response :success
  end

  test "should get edit" do
    get edit_character_armor_url(@character_armor)
    assert_response :success
  end

  test "should update character_armor" do
    patch character_armor_url(@character_armor), params: { character_armor: { armor_id: @character_armor.armor_id, carried: @character_armor.carried, character_id: @character_armor.character_id, craftmanship_id: @character_armor.craftmanship_id, equipped: @character_armor.equipped, modifications: @character_armor.modifications } }
    assert_redirected_to character_armor_url(@character_armor)
  end

  test "should destroy character_armor" do
    assert_difference('CharacterArmor.count', -1) do
      delete character_armor_url(@character_armor)
    end

    assert_redirected_to character_armors_url
  end
end
