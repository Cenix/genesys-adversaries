require "application_system_test_case"

class CharacterArmorsTest < ApplicationSystemTestCase
  setup do
    @character_armor = character_armors(:one)
  end

  test "visiting the index" do
    visit character_armors_url
    assert_selector "h1", text: "Character Armors"
  end

  test "creating a Character armor" do
    visit character_armors_url
    click_on "New Character Armor"

    fill_in "Armor", with: @character_armor.armor_id
    fill_in "Carried", with: @character_armor.carried
    fill_in "Character", with: @character_armor.character_id
    fill_in "Craftmanship", with: @character_armor.craftmanship_id
    fill_in "Equipped", with: @character_armor.equipped
    fill_in "Modifications", with: @character_armor.modifications
    click_on "Create Character armor"

    assert_text "Character armor was successfully created"
    click_on "Back"
  end

  test "updating a Character armor" do
    visit character_armors_url
    click_on "Edit", match: :first

    fill_in "Armor", with: @character_armor.armor_id
    fill_in "Carried", with: @character_armor.carried
    fill_in "Character", with: @character_armor.character_id
    fill_in "Craftmanship", with: @character_armor.craftmanship_id
    fill_in "Equipped", with: @character_armor.equipped
    fill_in "Modifications", with: @character_armor.modifications
    click_on "Update Character armor"

    assert_text "Character armor was successfully updated"
    click_on "Back"
  end

  test "destroying a Character armor" do
    visit character_armors_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Character armor was successfully destroyed"
  end
end
