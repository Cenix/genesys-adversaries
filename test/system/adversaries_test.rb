require "application_system_test_case"

class AdversariesTest < ApplicationSystemTestCase
  setup do
    @adversary = adversaries(:one)
  end

  test "visiting the index" do
    visit adversaries_url
    assert_selector "h1", text: "Adversaries"
  end

  test "creating a Adversary" do
    visit adversaries_url
    click_on "New Adversary"

    fill_in "Abilities", with: @adversary.abilities
    fill_in "Agility", with: @adversary.agility
    fill_in "Brawn", with: @adversary.brawn
    fill_in "Cunning", with: @adversary.cunning
    fill_in "Defense melee", with: @adversary.defense_melee
    fill_in "Defense ranged", with: @adversary.defense_ranged
    fill_in "Description", with: @adversary.description
    fill_in "Equipment", with: @adversary.equipment
    fill_in "Intellect", with: @adversary.intellect
    fill_in "Name", with: @adversary.name
    fill_in "Presence", with: @adversary.presence
    fill_in "Skills", with: @adversary.skills
    fill_in "Soak", with: @adversary.soak
    fill_in "Spells", with: @adversary.spells
    fill_in "Strain th", with: @adversary.strain_th
    fill_in "Talents", with: @adversary.talents
    fill_in "Type", with: @adversary.type
    fill_in "Willpower", with: @adversary.willpower
    fill_in "Wound th", with: @adversary.wound_th
    click_on "Create Adversary"

    assert_text "Adversary was successfully created"
    click_on "Back"
  end

  test "updating a Adversary" do
    visit adversaries_url
    click_on "Edit", match: :first

    fill_in "Abilities", with: @adversary.abilities
    fill_in "Agility", with: @adversary.agility
    fill_in "Brawn", with: @adversary.brawn
    fill_in "Cunning", with: @adversary.cunning
    fill_in "Defense melee", with: @adversary.defense_melee
    fill_in "Defense ranged", with: @adversary.defense_ranged
    fill_in "Description", with: @adversary.description
    fill_in "Equipment", with: @adversary.equipment
    fill_in "Intellect", with: @adversary.intellect
    fill_in "Name", with: @adversary.name
    fill_in "Presence", with: @adversary.presence
    fill_in "Skills", with: @adversary.skills
    fill_in "Soak", with: @adversary.soak
    fill_in "Spells", with: @adversary.spells
    fill_in "Strain th", with: @adversary.strain_th
    fill_in "Talents", with: @adversary.talents
    fill_in "Type", with: @adversary.type
    fill_in "Willpower", with: @adversary.willpower
    fill_in "Wound th", with: @adversary.wound_th
    click_on "Update Adversary"

    assert_text "Adversary was successfully updated"
    click_on "Back"
  end

  test "destroying a Adversary" do
    visit adversaries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Adversary was successfully destroyed"
  end
end
