require "application_system_test_case"

class CraftmanshipsTest < ApplicationSystemTestCase
  setup do
    @craftmanship = craftmanships(:one)
  end

  test "visiting the index" do
    visit craftmanships_url
    assert_selector "h1", text: "Craftmanships"
  end

  test "creating a Craftmanship" do
    visit craftmanships_url
    click_on "New Craftmanship"

    fill_in "Armor Mods", with: @craftmanship.armor_mods
    fill_in "Description", with: @craftmanship.description
    fill_in "Name", with: @craftmanship.name
    fill_in "Price Multiplier", with: @craftmanship.price_multiplier
    fill_in "Rarity Adjustment", with: @craftmanship.rarity_adjustment
    fill_in "Weapon Mods", with: @craftmanship.weapon_mods
    click_on "Create Craftmanship"

    assert_text "Craftmanship was successfully created"
    click_on "Back"
  end

  test "updating a Craftmanship" do
    visit craftmanships_url
    click_on "Edit", match: :first

    fill_in "Armor Mods", with: @craftmanship.armor_mods
    fill_in "Description", with: @craftmanship.description
    fill_in "Name", with: @craftmanship.name
    fill_in "Price Multiplier", with: @craftmanship.price_multiplier
    fill_in "Rarity Adjustment", with: @craftmanship.rarity_adjustment
    fill_in "Weapon Mods", with: @craftmanship.weapon_mods
    click_on "Update Craftmanship"

    assert_text "Craftmanship was successfully updated"
    click_on "Back"
  end

  test "destroying a Craftmanship" do
    visit craftmanships_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Craftmanship was successfully destroyed"
  end
end
