require "application_system_test_case"

class ItemQualitiesTest < ApplicationSystemTestCase
  setup do
    @item_quality = item_qualities(:one)
  end

  test "visiting the index" do
    visit item_qualities_url
    assert_selector "h1", text: "Item Qualities"
  end

  test "creating a Item quality" do
    visit item_qualities_url
    click_on "New Item Quality"

    fill_in "Activation", with: @item_quality.activation
    fill_in "Description", with: @item_quality.description
    fill_in "Name", with: @item_quality.name
    click_on "Create Item quality"

    assert_text "Item quality was successfully created"
    click_on "Back"
  end

  test "updating a Item quality" do
    visit item_qualities_url
    click_on "Edit", match: :first

    fill_in "Activation", with: @item_quality.activation
    fill_in "Description", with: @item_quality.description
    fill_in "Name", with: @item_quality.name
    click_on "Update Item quality"

    assert_text "Item quality was successfully updated"
    click_on "Back"
  end

  test "destroying a Item quality" do
    visit item_qualities_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Item quality was successfully destroyed"
  end
end
