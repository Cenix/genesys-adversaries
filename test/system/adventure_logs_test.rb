require "application_system_test_case"

class AdventureLogsTest < ApplicationSystemTestCase
  setup do
    @adventure_log = adventure_logs(:one)
  end

  test "visiting the index" do
    visit adventure_logs_url
    assert_selector "h1", text: "Adventure Logs"
  end

  test "creating a Adventure log" do
    visit adventure_logs_url
    click_on "New Adventure Log"

    fill_in "Character", with: @adventure_log.character_id
    fill_in "Date", with: @adventure_log.date
    fill_in "Experience", with: @adventure_log.experience
    fill_in "Log", with: @adventure_log.log
    fill_in "User", with: @adventure_log.user_id
    click_on "Create Adventure log"

    assert_text "Adventure log was successfully created"
    click_on "Back"
  end

  test "updating a Adventure log" do
    visit adventure_logs_url
    click_on "Edit", match: :first

    fill_in "Character", with: @adventure_log.character_id
    fill_in "Date", with: @adventure_log.date
    fill_in "Experience", with: @adventure_log.experience
    fill_in "Log", with: @adventure_log.log
    fill_in "User", with: @adventure_log.user_id
    click_on "Update Adventure log"

    assert_text "Adventure log was successfully updated"
    click_on "Back"
  end

  test "destroying a Adventure log" do
    visit adventure_logs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Adventure log was successfully destroyed"
  end
end
