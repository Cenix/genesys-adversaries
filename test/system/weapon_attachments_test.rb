require "application_system_test_case"

class WeaponAttachmentsTest < ApplicationSystemTestCase
  setup do
    @weapon_attachment = weapon_attachments(:one)
  end

  test "visiting the index" do
    visit weapon_attachments_url
    assert_selector "h1", text: "Weapon Attachments"
  end

  test "creating a Weapon attachment" do
    visit weapon_attachments_url
    click_on "New Weapon Attachment"

    fill_in "Description", with: @weapon_attachment.description
    fill_in "Hard points", with: @weapon_attachment.hard_points
    fill_in "Modifiers", with: @weapon_attachment.modifiers
    fill_in "Name", with: @weapon_attachment.name
    fill_in "Price", with: @weapon_attachment.price
    fill_in "Rarity", with: @weapon_attachment.rarity
    fill_in "Use with", with: @weapon_attachment.use_with
    click_on "Create Weapon attachment"

    assert_text "Weapon attachment was successfully created"
    click_on "Back"
  end

  test "updating a Weapon attachment" do
    visit weapon_attachments_url
    click_on "Edit", match: :first

    fill_in "Description", with: @weapon_attachment.description
    fill_in "Hard points", with: @weapon_attachment.hard_points
    fill_in "Modifiers", with: @weapon_attachment.modifiers
    fill_in "Name", with: @weapon_attachment.name
    fill_in "Price", with: @weapon_attachment.price
    fill_in "Rarity", with: @weapon_attachment.rarity
    fill_in "Use with", with: @weapon_attachment.use_with
    click_on "Update Weapon attachment"

    assert_text "Weapon attachment was successfully updated"
    click_on "Back"
  end

  test "destroying a Weapon attachment" do
    visit weapon_attachments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Weapon attachment was successfully destroyed"
  end
end
