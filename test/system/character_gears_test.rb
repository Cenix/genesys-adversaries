require "application_system_test_case"

class CharacterGearsTest < ApplicationSystemTestCase
  setup do
    @character_gear = character_gears(:one)
  end

  test "visiting the index" do
    visit character_gears_url
    assert_selector "h1", text: "Character Gears"
  end

  test "creating a Character gear" do
    visit character_gears_url
    click_on "New Character Gear"

    fill_in "Carried", with: @character_gear.carried
    fill_in "Character", with: @character_gear.character_id
    fill_in "Equipped", with: @character_gear.equipped
    fill_in "Gear", with: @character_gear.gear_id
    fill_in "Location", with: @character_gear.location
    fill_in "Modifications", with: @character_gear.modifications
    fill_in "Packed", with: @character_gear.packed
    fill_in "Qty", with: @character_gear.qty
    click_on "Create Character gear"

    assert_text "Character gear was successfully created"
    click_on "Back"
  end

  test "updating a Character gear" do
    visit character_gears_url
    click_on "Edit", match: :first

    fill_in "Carried", with: @character_gear.carried
    fill_in "Character", with: @character_gear.character_id
    fill_in "Equipped", with: @character_gear.equipped
    fill_in "Gear", with: @character_gear.gear_id
    fill_in "Location", with: @character_gear.location
    fill_in "Modifications", with: @character_gear.modifications
    fill_in "Packed", with: @character_gear.packed
    fill_in "Qty", with: @character_gear.qty
    click_on "Update Character gear"

    assert_text "Character gear was successfully updated"
    click_on "Back"
  end

  test "destroying a Character gear" do
    visit character_gears_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Character gear was successfully destroyed"
  end
end
