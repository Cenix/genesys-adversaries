require "application_system_test_case"

class ArmorAttachmentsTest < ApplicationSystemTestCase
  setup do
    @armor_attachment = armor_attachments(:one)
  end

  test "visiting the index" do
    visit armor_attachments_url
    assert_selector "h1", text: "Armor Attachments"
  end

  test "creating a Armor attachment" do
    visit armor_attachments_url
    click_on "New Armor Attachment"

    fill_in "Description", with: @armor_attachment.description
    fill_in "Hard points", with: @armor_attachment.hard_points
    fill_in "Modifiers", with: @armor_attachment.modifiers
    fill_in "Name", with: @armor_attachment.name
    fill_in "Price", with: @armor_attachment.price
    fill_in "Rarity", with: @armor_attachment.rarity
    fill_in "Use with", with: @armor_attachment.use_with
    click_on "Create Armor attachment"

    assert_text "Armor attachment was successfully created"
    click_on "Back"
  end

  test "updating a Armor attachment" do
    visit armor_attachments_url
    click_on "Edit", match: :first

    fill_in "Description", with: @armor_attachment.description
    fill_in "Hard points", with: @armor_attachment.hard_points
    fill_in "Modifiers", with: @armor_attachment.modifiers
    fill_in "Name", with: @armor_attachment.name
    fill_in "Price", with: @armor_attachment.price
    fill_in "Rarity", with: @armor_attachment.rarity
    fill_in "Use with", with: @armor_attachment.use_with
    click_on "Update Armor attachment"

    assert_text "Armor attachment was successfully updated"
    click_on "Back"
  end

  test "destroying a Armor attachment" do
    visit armor_attachments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Armor attachment was successfully destroyed"
  end
end
