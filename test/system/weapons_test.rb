require "application_system_test_case"

class WeaponsTest < ApplicationSystemTestCase
  setup do
    @weapon = weapons(:one)
  end

  test "visiting the index" do
    visit weapons_url
    assert_selector "h1", text: "Weapons"
  end

  test "creating a Weapon" do
    visit weapons_url
    click_on "New Weapon"

    fill_in "Crit", with: @weapon.crit
    fill_in "Damage", with: @weapon.damage
    fill_in "Description", with: @weapon.description
    fill_in "Encumbrance", with: @weapon.encumbrance
    fill_in "Hard Points", with: @weapon.hard_points
    fill_in "Image Url", with: @weapon.image_url
    fill_in "Name", with: @weapon.name
    fill_in "Price", with: @weapon.price
    fill_in "Range", with: @weapon.range
    fill_in "Rarity", with: @weapon.rarity
    fill_in "Skill", with: @weapon.skill_id
    fill_in "Slug", with: @weapon.slug
    fill_in "Weapon Type", with: @weapon.weapon_type
    click_on "Create Weapon"

    assert_text "Weapon was successfully created"
    click_on "Back"
  end

  test "updating a Weapon" do
    visit weapons_url
    click_on "Edit", match: :first

    fill_in "Crit", with: @weapon.crit
    fill_in "Damage", with: @weapon.damage
    fill_in "Description", with: @weapon.description
    fill_in "Encumbrance", with: @weapon.encumbrance
    fill_in "Hard Points", with: @weapon.hard_points
    fill_in "Image Url", with: @weapon.image_url
    fill_in "Name", with: @weapon.name
    fill_in "Price", with: @weapon.price
    fill_in "Range", with: @weapon.range
    fill_in "Rarity", with: @weapon.rarity
    fill_in "Skill", with: @weapon.skill_id
    fill_in "Slug", with: @weapon.slug
    fill_in "Weapon Type", with: @weapon.weapon_type
    click_on "Update Weapon"

    assert_text "Weapon was successfully updated"
    click_on "Back"
  end

  test "destroying a Weapon" do
    visit weapons_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Weapon was successfully destroyed"
  end
end
