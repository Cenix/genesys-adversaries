require "application_system_test_case"

class CharacterTalentsTest < ApplicationSystemTestCase
  setup do
    @character_talent = character_talents(:one)
  end

  test "visiting the index" do
    visit character_talents_url
    assert_selector "h1", text: "Character Talents"
  end

  test "creating a Character talent" do
    visit character_talents_url
    click_on "New Character Talent"

    fill_in "Character", with: @character_talent.character_id
    fill_in "Options", with: @character_talent.options
    fill_in "Talent", with: @character_talent.talent_id
    click_on "Create Character talent"

    assert_text "Character talent was successfully created"
    click_on "Back"
  end

  test "updating a Character talent" do
    visit character_talents_url
    click_on "Edit", match: :first

    fill_in "Character", with: @character_talent.character_id
    fill_in "Options", with: @character_talent.options
    fill_in "Talent", with: @character_talent.talent_id
    click_on "Update Character talent"

    assert_text "Character talent was successfully updated"
    click_on "Back"
  end

  test "destroying a Character talent" do
    visit character_talents_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Character talent was successfully destroyed"
  end
end
