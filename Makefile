deploy-stage:
	git push
	git push heroku master
	heroku run --app stage-genesys-adversary-bank rails db:migrate
	heroku run --app stage-genesys-adversary-bank rails db:seed

migrate-prod:
	heroku run --app genesys-adversary-bank rails db:migrate

seed-prod:
	heroku run --app genesys-adversary-bank rails db:seed

logs-stage:
	heroku logs --app stage-genesys-adversary-bank --tail

logs-prod:
	heroku logs --app genesys-adversary-bank --tail

db-export-stage:
	heroku pg:backups capture --app stage-genesys-adversary-bank
	curl -o ../dump/genesys-adversary-bank-stage.dump `heroku pg:backups public-url`

db-export-prod:
	heroku pg:backups capture --app genesys-adversary-bank
	curl -o ../dump/genesys-adversary-bank-prod.dump `heroku pg:backups public-url --app genesys-adversary-bank`

db-import-from-prod:
	heroku pg:backups capture --app genesys-adversary-bank
	curl -o ../dump/genesys-adversary-bank-prod.dump `heroku pg:backups public-url --app genesys-adversary-bank`
	docker-compose exec -d db bash -c "pg_restore -d adversaries_development -c -U postgres docker-entrypoint-initdb.d/genesys-adversary-bank-prod.dump"
