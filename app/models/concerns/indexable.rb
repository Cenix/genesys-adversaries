module Indexable
  extend ActiveSupport::Concern

  def setting_indices
    GenesysApi.new.get_index(self.class.name, id)
  end
end