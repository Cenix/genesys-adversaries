module Searchable
  extend ActiveSupport::Concern

  included do
    class_attribute :search_facets
  end

  class_methods do
    def facets_search(params)
      query = params[:search][:query].presence || "*"
      conditions = {}
      conditions[:activation] = params[:search][:activation] if params[:search][:activation].present?
      conditions[:tier] = params[:search][:tier] if params[:search][:tier].present?
      conditions[:career] = params[:search][:career] if params[:search][:career].present?
      #conditions[:gear_category_id] = params[:search][:category] if params[:search][:category].present?
      #conditions[:hard_points] = params[:search][:hard_points] if params[:search][:hard_points].present?
      #conditions[:quality] = params[:search][:quality] if params[:search][:quality].present?
      #conditions[:range] = params[:search][:range] if params[:search][:range].present?
      conditions[:ranked] = params[:search][:ranked] if params[:search][:ranked].present?
      conditions[:skill_id] = params[:search][:skill] if params[:search][:skill].present?
      #conditions[:talent] = params[:search][:talent] if params[:search][:talent].present?
      #conditions[:tree] = params[:search][:tree] if params[:search][:tree].present?
      #conditions[:trigger] = params[:search][:trigger] if params[:search][:trigger].present?
      #conditions[:weapon_category_id] = params[:search][:category] if params[:search][:category].present?
      #conditions[:category] = params[:search][:category] if params[:search][:category].present?

      #if params[:search][:price_range_low].present?
      #  conditions[:price] = {
      #    gte: params[:search][:price_range_low],
      #    lte: params[:search][:price_range_high]
      #  }
      #end

      self.search query, where: conditions,
        aggs: search_facets,
        page: params[:page], suggest: true, highlight: true,
        per_page: 50,
        order: :name,
        type: self
    end
  end
end

