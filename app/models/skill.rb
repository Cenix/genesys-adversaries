class Skill < ApplicationRecord
  include FriendlyId
  friendly_id :name, use: :slugged

  validates :name, :characteristic, :skill_type, presence: true
  validates :name, uniqueness: true

  default_scope { order(skill_type: :asc, name: :asc, created_at: :asc) }
  scope :social_skills_ordered_by_skill_name, -> { where("skills.skill_type = ?", 'Social') }
  scope :general_skills_ordered_by_skill_name, -> { where("skill_type = ?", 'General') }
  scope :knowledge_skills_ordered_by_skill_name, -> { where("skills.skill_type = ?", 'Knowledge') }
  scope :combat_skills_ordered_by_skill_name, -> { where("skills.skill_type = ?", 'Combat') }
  scope :magic_skills_ordered_by_skill_name, -> { where("skills.skill_type = ?", 'Magic') }

  def should_generate_new_friendly_id?
    name_changed?
  end

  def is_career_skill(character, talent_select = false)
    # Build an array of career skill ids granted by character's career.
    career_skill_ids = Array.new
    if character.career
      character.career.bonus_skills.each do |bs|
        career_skill_ids << bs.skill_id
      end
    end

    # Add career skills granted by character's archetype.
    character.archetype.bonus_skills.each do |bs|
      if bs.career_skill
        career_skill_ids << bs.skill_id
      end
    end

    # And finally add career skill ids granted by talents.
    character.character_talents.each do |ct|
      ct.bonus_skills.each do |bs|
        if bs.career_skill
          career_skill_ids << bs.skill_id
        end
      end
    end

    career_skill_ids.include?(id)
  end

  def search_data
    {
      name: name,
      skill_type: skill_type,
      created_at: created_at,
      updated_at: updated_at
    }
  end

  def self.facets_search(params)
    query = params[:search][:query].presence || "*"
    conditions = {}
    conditions[:skill_type] = params[:search][:skill_type] if params[:search][:skill_type].present?

    search query, where: conditions,
      aggs: {:skill_type => {}},
      page: params[:page],
      per_page: 50,
      order: {
        skill_type: :asc,
        name: :asc
      },
      type: self
  end
end
