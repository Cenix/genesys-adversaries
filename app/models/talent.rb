class Talent < ApplicationRecord
  #searchkick index_name: 'genesys', settings: {number_of_shards: 1}, highlight: [:name, :description]

  #include Searchable
  #include Indexable
  include FriendlyId
  friendly_id :name, use: :slugged

  validates :name, :tier, :activation, presence: true
  validates :name, uniqueness: true

  scope :tier, -> (tier) { where(tier: tier) }
  scope :ranked, -> { where(ranked: true) }
  #scope :unknown, -> (known_ids) { not(id: known_ids) }
  scope :unknown, -> (known_ids) { where(['id NOT IN (?)', known_ids]) if known_ids.any? }

  default_scope { order(tier: :asc, name: :asc, created_at: :asc) }

  def should_generate_new_friendly_id?
    name_changed?
  end

  #def search_data
  #  {
  #    name: name,
  #    tier: tier,
  #    activation: activation,
  #    created_at: created_at,
  #    updated_at: updated_at
  #  }
  #end

  #def self.facets_search(params)
  #  query = params[:search][:query].presence || "*"
  #  conditions = {}
  #  conditions[:tier] = params[:search][:tier] if params[:search][:tier].present?
  #  #conditions[:activation] = params[:search][:activation] if params[:search][:activation].present?
  #
  #  search query, where: conditions,
  #    aggs: {:tier => {}},
  #    page: params[:page],
  #    per_page: 25,
  #    order: {
  #      tier: :asc,
  #      name: :asc
  #    },
  #    type: self
  #end
end
