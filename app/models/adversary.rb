class Adversary < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  acts_as_taggable_on :settings, :types, :tags

  include PgSearch
  pg_search_scope :search, against: [:name, :description, :adversary_type]

  #searchkick
  default_scope { order(name: :asc) }

  def should_generate_new_friendly_id?
    name_changed?
  end

  def ordered_skill_list
    ordered_list = []
    general_skills = []
    knowledge_skills = []
    combat_skills = []
    magic_skills = []
    social_skills = []

    settings = setting_list
    settings << 'All'
    Skill.all.each do |skill|
      next if (skill.settings & settings).empty?
      adversary_skill_ranks =  skills.select {|ad_skill| ad_skill.id == skill.id }.first
      group_only = false

      ranks = 0
      unless adversary_skill_ranks.nil?
        ranks = adversary_skill_ranks.rank
        if adversary_type == 'Minion'
          group_only = true
        end
      end

      skill_stats = {
        id: skill.id,
        name: skill.name,
        ranks: ranks,
        characteristic: skill.characteristic,
        group: group_only
      }

      case skill.skill_type
      when 'Combat'
        combat_skills << skill_stats
      when 'Knowledge'
        knowledge_skills << skill_stats
      when 'Magic'
        magic_skills << skill_stats
      when 'Social'
        social_skills << skill_stats
      else
        general_skills << skill_stats
      end
    end
    {
      general: general_skills,
      combat: combat_skills,
      knowledge: knowledge_skills,
      magic: magic_skills,
      social: social_skills
    }
  end

  def ordered_skill_list_trained
    general_skills = []
    knowledge_skills = []
    combat_skills = []
    magic_skills = []
    social_skills = []

    ordered_skill_list.each_with_index do |(key, skills), index|
      skills.each do |adversary_skill|
        next if adversary_skill[:ranks].nil? || adversary_skill[:ranks] == 0 && !adversary_skill[:group]

        case key.to_s
        when 'combat'
          combat_skills << adversary_skill
        when 'knowledge'
          knowledge_skills << adversary_skill
        when 'magic'
          knowledge_skills << adversary_skill
        when 'social'
          knowledge_skills << adversary_skill
        else
          general_skills << adversary_skill
        end
      end
    end

    {
      general: general_skills,
      combat: combat_skills,
      knowledge: knowledge_skills,
      magic: magic_skills,
      social: social_skills
    }
  end

  def attacks
    results = []

    weapons.each do |weapon|
      qualities = []
      unless weapon.qualities.nil?
        weapon.qualities.each do |q|
          if !q['id'].blank?
            item_quality = ItemQuality.find(q['id'])
            qualities << {
              quality: item_quality,
              ranks: q['rank'],
              text: "#{item_quality.name}#{' ' if q['rank'].to_i > 0}#{q['rank'] if q['rank'].to_i > 0}"
            }
          elsif q['text'].present?
            qualities << {
              text: q['text']
            }
          end
        end
      end
      combat_skill = Skill.find(weapon.skill_id)
      adversary_skill = skills.select {|ad_skill| ad_skill.id == weapon.skill_id }.first

      case weapon.range
      when 'Engaged'
        min_difficulty = 2
      else
        min_difficulty = 1
      end

      results << {
        name: weapon.name,
        damage: weapon.damage,
        critical: weapon.critical,
        range: weapon.range,
        skill: combat_skill,
        ranks: adversary_skill.nil? ? 0 : adversary_skill.rank,
        qualities: qualities,
        min_diff: min_difficulty
      }
    end
    results
  end

  def qualities_list
    qualities = []
    attacks.each do |attack|
      attack[:qualities].each do |q|
        next if q[:quality].nil?
        qualities << q[:quality]
      end
    end
    qualities.uniq
  end

  def talent_list
    results = []
    talents.each do |ct|
      results << {
        talent: Talent.find(ct.id),
        ranks: ct.rank,
      }
    end
    results
  end

  # Ability subclassing.
  def abilities
    unless read_attribute(:abilities).nil?
      read_attribute(:abilities).map {|v| Ability.new(v) }
    end
  end

  def abilities_attributes=(attributes)
    abilities = []
    attributes.each do |index, attrs|
      next if '1' == attrs.delete("_destroy")
      #attrs[:percentage] = attrs[:percentage].try(:to_i)
      abilities << attrs
    end
    write_attribute(:abilities, abilities)
  end

  def build_ability
    v = self.abilities.dup
    v << Ability.new({name: '', description: ''})
    self.abilities = v
  end

  class Ability
    attr_accessor :name, :description

    def initialize(hash)
      @name          = hash['name']
      @description    = hash['description']
    end

    def persisted?() false; end
    def new_record?() false; end
    def marked_for_destruction?() false; end
    def _destroy() false; end
  end

  # Spell subclassing.
  def spells
    unless read_attribute(:spells).nil?
      read_attribute(:spells).map {|v| Spell.new(v) }
    end
  end

  def spells_attributes=(attributes)
    spells = []
    attributes.each do |index, attrs|
      next if '1' == attrs.delete("_destroy")
      #attrs[:percentage] = attrs[:percentage].try(:to_i)
      spells << attrs
    end
    write_attribute(:spells, spells)
  end

  def build_spell
    v = self.spells.dup
    v << Spell.new({name: '', description: ''})
    self.spells = v
  end

  class Spell
    attr_accessor :name, :description

    def initialize(hash)
      @name          = hash['name']
      @description    = hash['description']
    end

    def persisted?() false; end
    def new_record?() false; end
    def marked_for_destruction?() false; end
    def _destroy() false; end
  end

  # Talent subclassing.
  def talents
    unless read_attribute(:talents).nil?
      read_attribute(:talents).map {|v| AdversaryTalent.new(v) }
    end
  end

  def talents_attributes=(attributes)
    talents = []
    attributes.each do |index, attrs|
      next if '1' == attrs.delete("_destroy")
      #attrs[:percentage] = attrs[:percentage].try(:to_i)
      talents << attrs
    end
    write_attribute(:talents, talents)
  end

  def build_talent
    v = self.talents.dup
    v << AdversaryTalent.new({id: 0, rank: 0})
    self.talents = v
  end

  class AdversaryTalent
    attr_accessor :id, :rank

    def initialize(hash)
      @id   = hash['id'].to_i
      @rank = hash['rank'].to_i
    end

    def persisted?() false; end
    def new_record?() false; end
    def marked_for_destruction?() false; end
    def _destroy() false; end
  end

  # Skill subclassing.
  def skills
    unless read_attribute(:skills).nil?
      read_attribute(:skills).map {|v| AdversarySkill.new(v) }
    end
  end

  def skills_attributes=(attributes)
    skills = []
    attributes.each do |index, attrs|
      next if '1' == attrs.delete("_destroy")
      #attrs[:percentage] = attrs[:percentage].try(:to_i)
      skills << attrs
    end
    write_attribute(:skills, skills)
  end

  def build_skill
    v = self.skills.dup
    v << AdversarySkill.new({id: 0, rank: 0})
    self.skills = v
  end

  class AdversarySkill
    attr_accessor :id, :rank

    def initialize(hash)
      @id   = hash['id'].to_i
      @rank = hash['rank'].to_i
    end

    def persisted?() false; end
    def new_record?() false; end
    def marked_for_destruction?() false; end
    def _destroy() false; end
  end

  # Weapons subclassing.
  def weapons
    unless read_attribute(:weapons).nil?
      read_attribute(:weapons).map {|v| Weapon.new(v) }
    end
  end

  def weapons_attributes=(attributes)
    weapons = []
    attributes.each do |index, attrs|
      next if '1' == attrs.delete("_destroy")
      #attrs[:percentage] = attrs[:percentage].try(:to_i)
      weapons << attrs
    end
    write_attribute(:weapons, weapons)
  end

  def build_weapon
    v = self.weapons.dup
    v << Weapon.new({name: '', skill_id: 0, damage: 0, critical: 0, range: 'Engaged', qualities: []})
    self.weapons = v
  end

  class Weapon
    attr_accessor :name, :skill_id, :damage, :critical, :range, :qualities

    def initialize(hash)
      @name   = hash['name']
      @skill_id   = hash['skill_id']
      @damage   = hash['damage']
      @critical   = hash['critical']
      @range   = hash['range']
      @qualities   = hash['qualities']
    end

    def persisted?() false; end
    def new_record?() false; end
    def marked_for_destruction?() false; end
    def _destroy() false; end
  end

  # Search config
  def search_data
    {
      name: name,
      skill: adversary_skills.map(&:skill_id),
      talent: adversary_talents.map(&:talent_id),
      trait: adversary_traits.map(&:trait_id),
      tags: tag_list,
      created_at: created_at,
      updated_at: updated_at
    }
  end

  def self.facets_search(params)
    query = params[:search][:query].presence || "*"
    conditions = {}
    conditions[:tags] = params[:search][:tags] if params[:search][:tags].present?
    conditions[:skill] = params[:search][:skill] if params[:search][:skill].present?
    conditions[:talent] = params[:search][:talent] if params[:search][:talent].present?
    conditions[:trait] = params[:search][:trait] if params[:search][:trait].present?

    adversaries = Adversary.search query, where: conditions,
      aggs: {:tags => {}, :skill => {}, :talent => {}, :trait => {}},
      page: params[:page],
      per_page: 50,
      order: :name,
      type: Adversary
    adversaries
  end
end
