json.extract! skill, :id, :name, :characteristic, :skill_type, :description, :slug, :created_at, :updated_at
json.url skill_url(skill, format: :json)
