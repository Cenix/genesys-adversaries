json.extract! item_quality, :id, :name, :description, :activation, :created_at, :updated_at
json.url item_quality_url(item_quality, format: :json)
