json.extract! adversary, :id, :name, :description, :brawn, :agility, :intellect, :cunning, :willpower, :presence, :adversary_type, :soak, :wound_th, :strain_th, :defense_melee, :defense_ranged, :skills, :talents, :abilities, :equipment, :spells, :created_at, :updated_at, :combat_rating, :social_rating, :general_rating
json.link_name_and_type "#{link_to(adversary.name, adversary)} <span class='badge badge-#{adversary.adversary_type.downcase}'>#{adversary.adversary_type}</span>"
json.settings adversary.settings.to_sentence(last_word_connector: ', ', two_words_connector: ', ')
json.edit ''
json.destroy ''
if can? :manage, adversary
  json.edit link_to 'Edit', edit_adversary_path(adversary)
  json.destroy link_to 'Destroy', adversary, method: :delete, data: { confirm: 'Are you sure?' }
end
