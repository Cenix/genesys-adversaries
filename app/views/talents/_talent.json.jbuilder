json.extract! talent, :id, :name, :description, :tier, :activation, :ranked, :slug, :created_at, :updated_at
json.url talent_url(talent, format: :json)
