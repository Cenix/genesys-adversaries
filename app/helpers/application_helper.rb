module ApplicationHelper

  def current_class?(test_path)
    return 'active' if request.path == test_path
    ''
  end

  def current_page(page_icon)
    @page == page_icon
  end

  def set_search_params_cookiee(params)
    if params[:clearsearch].present?
      cookies.delete :search_params
      params[:search] = {}
    elsif params[:search]
      cookies[:search_params] = {
        value: params[:search].to_json,
        expires: 1.hour.from_now
      }
    elsif cookies[:search_params]
      params[:search] =  JSON.parse(cookies[:search_params])
    else
      params[:search] = {}
    end
    params[:search]
  end

  def text_replace_tokens(text)
    return if text.nil?
    text = text.gsub(/\[(advantage|ad)\]/i, '<span class="symbols">a</span>')
    text = text.gsub(/\[(threat|t)\]/i, '<span class="symbols">h</span>')
    text = text.gsub(/\[(failure|f)\]/i, '<span class="symbols">f</span>')
    text = text.gsub(/\[(success|su)\]/i, '<span class="symbols">s</span>')
    text = text.gsub(/\[(triumph|tri)\]/i, '<span class="symbols">t</span>')
    text = text.gsub(/\[(despair|des)\]/i, '<span class="symbols">d</span>')

    text = text.gsub(/\[(setback|s)\]/i, '<span class="setback">b</span>')
    text = text.gsub(/\[(boost|b|bonus)\]/i, '<span class="boost">b</span>')
    text = text.gsub(/\[(difficulty|d)\]/i, '<span class="difficulty">d</span>')
    text = text.gsub(/\[(ability|ab)\]/i, '<span class="ability">d</span>')
    text = text.gsub(/\[(challenge|c)\]/i, '<span class="challenge">c</span>')
    text = text.gsub(/\[(proficiency|p)\]/i, '<span class="proficiency">c</span>')

    text.html_safe
  end

  def talent_replace_tokens(text, ranks = 1)
    result = text.scan(/\[(\d*x )*num_*ranks*( \[.*\])*\]/i)
    if result[0]
      dice = []
      multiplier = result[0].first.nil? ? 1 : result[0].first.gsub(/x/, '').to_i
      if result[0].last
        (multiplier * ranks).times do
          dice << result[0].last.match(/\[.*\]/).to_s
        end
        text = text.gsub(/\[(\d*x )*num_*ranks*( \[.*\])*\]/i, dice.join())
      else
        text = text.gsub(/\[(\d*x )*num_*ranks*\]/i, (multiplier * ranks).to_s)
      end
    end
    text_replace_tokens(text)
  end

  def markdown(text)
    options = {
      filter_html: false,
      hard_wrap: true,
      space_after_headers: true,
      fenced_code_blocks: true
    }

    extensions = {
      autolink: true,
      superscript: true,
      tables: true,
      disable_indented_code_blocks: true
    }

    markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, extensions)

    markdown.render(text).html_safe
  end

  def one_line(&block)
    haml_concat capture_haml(&block).gsub("\n", '').gsub('\\n', "\n").html_safe
  end

  def to_roman(number)
    result = ""
    roman_mapping.keys.each do |divisor|
      quotient, modulus = number.divmod(divisor)
      result << roman_mapping[divisor] * quotient
      number = modulus
    end
    result
  end

  def roman_mapping
    {
      1000 => "M",
      900 => "CM",
      500 => "D",
      400 => "CD",
      100 => "C",
      90 => "XC",
      50 => "L",
      40 => "XL",
      10 => "X",
      9 => "IX",
      5 => "V",
      4 => "IV",
      1 => "I"
    }
  end

  def link_to_add_skill_fields(name, f, association, path = '')
    #new_object = f.object.class.reflect_on_association(association).klass.new
    new_object = Adversary::AdversarySkill.new(id: 0, rank: 0)
    fields = f.fields_for(association, new_object, child_index: "new_#{association}") do |builder|
      render(path + association.to_s.singularize + '_fields', f: builder)
    end
    link_to name, '', onclick: h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\");return false;")
  end

  def link_to_add_talent_fields(name, f, association, path = '')
    new_object = Adversary::AdversaryTalent.new(id: 0, rank: 0)
    fields = f.fields_for(association, new_object, child_index: "new_#{association}") do |builder|
      render(path + association.to_s.singularize + '_fields', f: builder)
    end
    link_to name, '', onclick: h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\");return false;")
  end

  def link_to_add_ability_fields(name, f, association, path = '')
    new_object = Adversary::Ability.new(name: '', description: '')
    fields = f.fields_for(association, new_object, child_index: "new_#{association}") do |builder|
      render(path + association.to_s.singularize + '_fields', f: builder)
    end
    link_to name, '', onclick: h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\");return false;")
  end

  def link_to_add_spell_fields(name, f, association, path = '')
    new_object = Adversary::Spell.new(name: '', description: '')
    fields = f.fields_for(association, new_object, child_index: "new_#{association}") do |builder|
      render(path + association.to_s.singularize + '_fields', f: builder)
    end
    link_to name, '', onclick: h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\");return false;")
  end

  def link_to_add_weapon_fields(name, f, association, path = '')
    new_object = Adversary::Weapon.new(name: '', description: '')
    fields = f.fields_for(association, new_object, child_index: "new_#{association}") do |builder|
      render(path + association.to_s.singularize + '_fields', f: builder)
    end
    link_to name, '', onclick: h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\");return false;")
  end
end


#<span class='symbols'>a</span> = Advantage<br>
#<span class='symbols'>h</span> = Threat<br>
#<span class='symbols'>f</span> = Failure<br>
#<span class='symbols'>s</span> = Success<br>
#<span class='symbols'>t</span> = Triumph<br>
#<span class='symbols'>d</span> = Despair<br>
#<span class='setback'>b</span> = Setback Dice<br>
#<span class='boost'>b</span> = Boost Dice<br>
#<span class='difficulty'>d</span> = Difficulty Dice<br>
#<span class='ability'>d</span> = Ability Dice<br>
#<span class='challenge'>c</span> = Challenge Dice<br>
#<span class='proficiency'>c</span> = Proficiency Dice <br>