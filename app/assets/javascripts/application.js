// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require jquery3
//= require jquery.knob
//= require popper
//= require cocoon
//= require bootstrap-sprockets
//= require jquery.dataTables.min
//= require feather.min
//= require bootstrap
//= require_tree .
//= require selectize


$(function () {
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()
  $('.collapse').collapse('hide')
})

function openModal(url, size){
  if (size === undefined) {
    size = '';
  }
  var $modal = $('#globalModal' + size);

  $.ajax(url).done(function(data) {
    $modal.html(data).foundation("open");
  });
}

function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g");
  $(link).before(content.replace(regexp, new_id));
}

$(document).ready(function() {
  $(".roll button").click(function(e) {
    e.preventDefault();
    var url = $(this).attr("data-url");
    $("#rollModal iframe").attr("src", url);
    $("#rollModal").modal("show");
  });

  function resizeIFrameToFitContent( iFrame ) {
    iFrame.width  = iFrame.contentWindow.document.body.scrollWidth;
    iFrame.height = iFrame.contentWindow.document.body.scrollHeight;
  }

  window.addEventListener('DOMContentLoaded', function(e) {
    var iFrame = document.getElementById( 'rollFrame' );
    resizeIFrameToFitContent( iFrame );
  });
});
