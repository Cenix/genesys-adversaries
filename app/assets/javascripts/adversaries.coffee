# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'turbolinks:before-cache', ->
  $('[data-selectize]').each ->
    this.selectize.destroy()

$(document).on 'turbolinks:load', ->
  if window.location.pathname == '/'
    dataUrl = '/adversaries.json' + window.location.search
  else
    dataUrl = window.location.pathname + '.json' + window.location.search

  table = $('#adversaries-index').DataTable(
    #'processing': true
    #'serverSide': true
    'ajax':
      'url': dataUrl
      'dataSrc': ''
    #'stateSave': true
    'deferRender': true
    'pageLength': 50
    'ordering':  false
    'columns': [
      { 'data': 'link_name_and_type' }
      { 'data': 'combat_rating' }
      { 'data': 'social_rating' }
      { 'data': 'general_rating' }
      { 'data': 'settings' }
    ]
  )

  $('#skill-toggle').click ->
    $('#skill-toggle').toggleClass('btn-outline-info')
    $('#skill-toggle').toggleClass('btn-info')
    $('#ranked-skill-list').toggle()
    $('#full-skill-list').toggle();

    text = $('#skill-toggle').text().trim();
    if text == 'All skills'
      new_text = '<i class="fa fa-compress-alt"></i> Trained skills'
    else
      new_text = '<i class="fa fa-expand-alt"></i> All skills'
    $('#skill-toggle').html(new_text)

  $('#minion-increase').click ->
    $('#minion-decrease').show()
    $('#current-wounds').val(0)
    $('#current-strain').val(0)
    $('#minion-count').data('dead', 0)
    minion_count = $('#minion-count').data('value') + 1
    $('#minion-count').data('value', minion_count)
    $('#minion-count').empty()
    i = 0
    while i < minion_count
      $('#minion-count').append('<i class="fas fa-male"></i>')
      i++

    # Update minion group wound threshold.
    update_wounds(minion_count)
    # Update minion dice pools.
    update_dice_pool(minion_count)
    if minion_count >= 8
      $('#minion-increase').hide()
    if minion_count >= 5
      $('#combat-rating .card-title').text($('#combat-rating').data('value') + 1)
      $('#social-rating .card-title').text($('#social-rating').data('value') + 1)
    else
      $('#combat-rating .card-title').text($('#combat-rating').data('value'))
      $('#social-rating .card-title').text($('#social-rating').data('value'))

  $('#minion-decrease').click ->
    decrease_minions()

  $('#wound-increase').click ->
    num_minions = $('#minion-count').data('value')
    single_adversary_wound_threshold = $('#wounds').data('value')
    total_wound_threshold = $('#minion-count').data('value') * single_adversary_wound_threshold

    kill_points = []
    i = 1
    while (i < 9)
      total_wound_threshold = total_wound_threshold - single_adversary_wound_threshold
      kill_points.push((i * single_adversary_wound_threshold) + 1)
      i++

    wounds = parseInt($('#current-wounds').val()) + 1
    $('#current-wounds').val(wounds)

    if $.inArray(wounds, kill_points) isnt -1
      decrease_minions(true, true)

  $('#wound-decrease').click ->
    wounds = parseInt($('#current-wounds').val()) - 1
    if wounds < 0
      wounds = 0
    $('#current-wounds').val(wounds)

  $('#strain-increase').click ->
    strain = parseInt($('#current-strain').val())
    $('#current-strain').val(strain + 1)
    # Strain on Rivals and Minions count as Wounds.
    if $('#adversary-type').data('value') != 'Nemesis'
      $('#wound-increase').click()

  $('#strain-decrease').click ->
    strain = parseInt($('#current-strain').val())
    $('#current-strain').val(strain - 1)

  # Criticals
  $('#critical-increase').click ->
    criticals = parseInt($('#current-criticals').val())
    $('#current-criticals').val(criticals + 1)

  $('#critical-decrease').click ->
    criticals = parseInt($('#current-criticals').val())
    $('#current-criticals').val(criticals - 1)

  # States
$(document).on 'click', '#states .custom-control-input', ->
  $(this).toggleClass('on')
  set_state(this, $(this).hasClass('on'))

set_state = (state, apply) ->
  slot_id = $(state).closest('.initiative-slot').attr 'data-pos'
  if apply
    switch $(state).attr 'data-state'
      when 'blind'
        text = '<strong>Blind:</strong> Upgrade the difficulty of all checks twice. Upgrade the difficulty of Perception and Vigilance checks three times.'
      when 'disorient'
        text = '<strong>Disoriented:</strong> Add a Setback die to all checks.'
      when 'stagger'
        text = '<strong>Staggered:</strong> Cannot perform actions, including downgrading actions to maneuvers.'
      when 'immobilize'
        text = '<strong>Immobilized:</strong> Cannot perform any maneuvers.'
      when 'prone'
        text = '<strong>Prone:</strong> Add 1 Setback to all ranged attack agains this character. Add 1 Boost to all melee attacks against this character.'
        value = $('#ranged-def .card-title').data('value') + 1
        $('#ranged-def .card-title').data('value', value)
        $('#ranged-def .card-title').text(value)
      when 'off-balance'
        text = '<strong>Off Balance:</strong> Add 1 Setback to next skill check.'
      when 'cover'
        text = '<strong>In Cover:</strong> Gain ranged defense 1.'
        value = $('#ranged-def .card-title').data('value') + 1
        $('#ranged-def .card-title').data('value', value)
        $('#ranged-def .card-title').text(value)
      when 'maimed'
        text = '<strong>Maimed:</strong> Lost a limb. All actions gain 1 Setback.'

    node = document.createElement('div')
    node.className = 'state-info-' + $(state).attr 'data-state'
    node.innerHTML = '<li>' + text + '</li>'
    $("#states-info").append node
  else
    switch $(state).attr 'data-state'
      when 'prone'
        value = $('#ranged-def .card-title').data('value') - 1
        $('#ranged-def .card-title').data('value', value)
        $('#ranged-def .card-title').text(value)
      when 'cover'
        value = $('#ranged-def .card-title').data('value') - 1
        $('#ranged-def .card-title').data('value', value)
        $('#ranged-def .card-title').text(value)
    $(".state-info-" + $(state).attr 'data-state').remove()


decrease_minions = (keep_wth = false, kill_minion = false) ->
  $('#minion-increase').show()
  minion_count = $('#minion-count').data('value') - 1
  if kill_minion
    dead_minions = $('#minion-count').data('dead') + 1
    $('#minion-count').data('dead', dead_minions)

  if minion_count < 0
    minion_count = 0

  $('#minion-count').data('value', minion_count)
  #$('#minion-count').text('x' + minion_count)
  $('#minion-count').empty()
  i = 0
  while i < minion_count
    $('#minion-count').append('<i class="fas fa-male"></i>')
    i++
  if kill_minion
    i = 0
    while i < dead_minions
      $('#minion-count').append('<i class="fas fa-skull"></i>')
      i++

  if minion_count == 1
    #  $('#minion-count').text('')
    $('#minion-decrease').hide()

  if !keep_wth
    # Update minion group wound threshold.
    update_wounds(minion_count)

  # Update minion dice pools.
  update_dice_pool(minion_count, false)

  if minion_count >= 5
    $('#combat-rating .card-title').text($('#combat-rating').data('value') + 1)
    $('#social-rating .card-title').text($('#social-rating').data('value') + 1)
  else
    $('#combat-rating .card-title').text($('#combat-rating').data('value'))
    $('#social-rating .card-title').text($('#social-rating').data('value'))


update_wounds = (count) ->
  $('#group_wounds .card-title').text($('#group_wounds').data('value') * count)

update_dice_pool = (count, increase = true) ->
  $('tr[data-group-only]').each ->
    data = $(this).data()
    if increase
      data.skillRanks += 1
    else
     data.skillRanks -= 1

    if data.skillRanks < 0
      data.skillRanks = 0

    $(this).find('.ranks').html(data.skillRanks)

    if data.skillScore < data.skillRanks
      ability = data.skillRanks - data.skillScore
      proficiency = data.skillScore
    else
      ability = data.skillScore - data.skillRanks
      proficiency = data.skillRanks

    if ability <= 8 && ability > -1
      $(this).find('.ability').removeClass (index, css) ->
        (css.match(/x\d/g) or []).join ' '
      $(this).find('.ability').addClass('x' + ability)
      $(this).find('.ability').text('d'.repeat(ability))

    if proficiency <= 8 && proficiency > -1
      $(this).find('.proficiency').removeClass (index, css) ->
        (css.match(/x\d/g) or []).join ' '
      $(this).find('.proficiency').addClass('x' + proficiency)
      $(this).find('.proficiency').text('c'.repeat(proficiency))
