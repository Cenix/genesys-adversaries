class AdversariesController < ApplicationController
  include ApplicationHelper
  include AdversariesHelper
  before_action :set_adversary, only: [:show, :edit, :update, :destroy, :roll]
  load_and_authorize_resource
  skip_authorize_resource :only => [:index, :show]
  after_action :allow_dice_roller_iframe


  # GET /adversaries
  # GET /adversaries.json
  def index
    #params[:search] = set_search_params_cookiee(params)
    if params[:query].present?
      @adversaries = Adversary.search(params[:query])
    elsif params[:tag]
      @adversaries = Adversary.tagged_with(params[:tag], :match_all => false)
    else
      #@adversaries = Adversary.facets_search(params)
      @adversaries = Adversary.all
    end
  end

  # GET /adversaries/1
  # GET /adversaries/1.json
  def show
  end

  # GET /adversaries/new
  def new
    @adversary = Adversary.new
    @adversary.build_ability
    @adversary.build_adversary_talent

    @tags = ActsAsTaggableOn::Tag.for_context(:tags).map{|tag| {name: tag.name}}
    @settings = ActsAsTaggableOn::Tag.for_context(:settings).map{|tag| {name: tag.name}}
  end

  # GET /adversaries/1/edit
  def edit
    @tags = ActsAsTaggableOn::Tag.for_context(:tags).map{|tag| {name: tag.name}}
    @settings = ActsAsTaggableOn::Tag.for_context(:settings).map{|tag| {name: tag.name}}
  end

  # POST /adversaries
  # POST /adversaries.json
  def create
    @adversary = Adversary.new(adversary_params)

    respond_to do |format|
      if @adversary.save
        format.html { redirect_to @adversary, notice: 'Adversary was successfully created.' }
        format.json { render :show, status: :created, location: @adversary }
      else
        format.html { render :new }
        format.json { render json: @adversary.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adversaries/1
  # PATCH/PUT /adversaries/1.json
  def update
    respond_to do |format|
      if @adversary.update(adversary_params)
        format.html { redirect_to @adversary, notice: 'Adversary was successfully updated.' }
        format.json { render :show, status: :ok, location: @adversary }
      else
        format.html { render :edit }
        format.json { render json: @adversary.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adversaries/1
  # DELETE /adversaries/1.json
  def destroy
    @adversary.destroy
    respond_to do |format|
      format.html { redirect_to adversaries_url, notice: 'Adversary was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def roll
    @roll_response = HTTParty.get("https://stage-rpg-dice-roller.herokuapp.com/api/v1/genesys/roll/a1/p3")
    #logger.info roll_response
    respond_to do |format|
      #format.html { redirect_to @adversary, notice: 'Dice was rolled' }
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_adversary
      id = params[:id].nil? ? params[:adversary_id] : params[:id]
      @adversary = Adversary.friendly.find(id)
    end

    def allow_dice_roller_iframe
      response.headers['X-Frame-Options'] = 'ALLOW-FROM https://stage-rpg-dice-roller.herokuapp.com'
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def adversary_params
      # Apparently rails strong parameters have problems with arrays of hash values like the weapon qualities.
      # So... no strong paramters here then.
      params.require(:adversary).permit!
      #params.require(:adversary).permit(:name, :description, :brawn, :agility, :intellect, :cunning, :willpower, :presence, :type, :soak, :wound_th, :strain_th, :defense_melee, :defense_ranged, :skills, :talents, :abilities, :equipment, :spells,
      #    abilities_attributes: [:name, :description, :_destroy],
      #    spells_attributes: [:name, :description, :_destroy],
      #    talents_attributes: [:id, :rank, :_destroy],
      #    skills_attributes: [:id, :rank, :_destroy],
      #    weapons_attributes: [:name, :skill_id, :damage, :critical, :range, :qualities, :_destroy])
    end
end
