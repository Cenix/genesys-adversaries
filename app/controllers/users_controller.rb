# Users Controller
class UsersController < ApplicationController
  before_action :set_user, only: [:edit, :update]
  before_action :set_page
  load_and_authorize_resource

  def index
    @users = User.all
  end

  def set_up
    @page = 'users'
    @title = 'User'
  end

  def edit
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to users_path, notice:  "#{@user.username} was successfully updated." }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_user
    #@user = User.friendly.find(params[:id])
    @user = User.find(params[:id])
  end

  def set_page
    #@top_page = 'character_options'
    @page = 'users'
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :remember_me, :first_name, :last_name, :username, :enabled, :role, :superadmin_role, :editor_role, :player_role)
  end
end
