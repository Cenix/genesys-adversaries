class SkillsController < ApplicationController
  include ApplicationHelper
  include SkillsHelper

  before_action :set_skill, only: [:show, :edit, :update, :destroy]
  #after_action :update_setting_indices, only: [:create, :update]

  load_and_authorize_resource
  skip_authorize_resource only: [:index]

  # GET /skills
  # GET /skills.json
  def index
    params[:search] = set_search_params_cookiee(params)
    @skills = Skill.all #.facets_search(params)
  end

  # GET /skills/1
  # GET /skills/1.json
  def show
  end

  # GET /skills/new
  def new
    @skill = Skill.new
  end

  # GET /skills/1/edit
  def edit
  end

  # POST /skills
  # POST /skills.json
  def create
    @skill = Skill.new(skill_params)

    respond_to do |format|
      if @skill.save
        format.html { redirect_to @skill, notice: 'Skill was successfully created.' }
        format.json { render :show, status: :created, location: @skill }
      else
        format.html { render :new }
        format.json { render json: @skill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /skills/1
  # PATCH/PUT /skills/1.json
  def update
    respond_to do |format|
      if @skill.update(skill_params)
        format.html { redirect_to @skill, notice: 'Skill was successfully updated.' }
        format.json { render :show, status: :ok, location: @skill }
      else
        format.html { render :edit }
        format.json { render json: @skill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /skills/1
  # DELETE /skills/1.json
  def destroy
    #GenesysApi.new.delete_indices_for_item('Skill', @skill.id)
    @skill.destroy
    respond_to do |format|
      format.js
      format.html { redirect_to skills_url, notice: 'Skill was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def update_setting_indices
      GenesysApi.new.set_object_indices(@skill)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_skill
      @skill = Skill.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def skill_params
      params.require(:skill).permit(:name, :description, :characteristic, :skill_type, :slug)
    end
end
