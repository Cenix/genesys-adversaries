class PagesController < ApplicationController
  def about
    @page = 'about'
  end

  def updates
    @page = 'updates'
  end
end