class ItemQualitiesController < ApplicationController
  before_action :set_item_quality, only: [:show, :edit, :update, :destroy]

  load_and_authorize_resource
  skip_authorize_resource only: [:index]

  # GET /item_qualities
  # GET /item_qualities.json
  def index
    @item_qualities = ItemQuality.all
  end

  # GET /item_qualities/1
  # GET /item_qualities/1.json
  def show
  end

  # GET /item_qualities/new
  def new
    @item_quality = ItemQuality.new
  end

  # GET /item_qualities/1/edit
  def edit
  end

  # POST /item_qualities
  # POST /item_qualities.json
  def create
    @item_quality = ItemQuality.new(item_quality_params)

    respond_to do |format|
      if @item_quality.save
        format.html { redirect_to @item_quality, notice: 'Item quality was successfully created.' }
        format.json { render :show, status: :created, location: @item_quality }
      else
        format.html { render :new }
        format.json { render json: @item_quality.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /item_qualities/1
  # PATCH/PUT /item_qualities/1.json
  def update
    respond_to do |format|
      if @item_quality.update(item_quality_params)
        format.html { redirect_to @item_quality, notice: 'Item quality was successfully updated.' }
        format.json { render :show, status: :ok, location: @item_quality }
      else
        format.html { render :edit }
        format.json { render json: @item_quality.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /item_qualities/1
  # DELETE /item_qualities/1.json
  def destroy
    @item_quality.destroy
    respond_to do |format|
      format.html { redirect_to item_qualities_url, notice: 'Item quality was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item_quality
      @item_quality = ItemQuality.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_quality_params
      params.require(:item_quality).permit(:name, :description, :activation)
    end
end
