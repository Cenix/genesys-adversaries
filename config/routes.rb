Rails.application.routes.draw do
  resources :adversaries do
    get 'roll/:skill_id' => 'adversaries#roll', as: :roll
  end

  resources :item_qualities

  resources :talents do
    resources :connections
  end

  resources :skills do
    resources :connections
  end

  get 'generators/adversary', to: 'generators#adversary'

  get 'welcome/index'
  get 'characters', to: 'character_manager#index'

  devise_for :users, :controllers => { registrations: 'registrations' }
  devise_scope :user do
    get "login", to: "devise/sessions#new"
    get "logout", to: "devise/sessions#destroy"
    get "register", to: "devise/registrations#new"
    get "me", to: "devise/registrations#edit"
  end
  get 'tags/:tag', to: 'adversaries#index', as: :tag
  #get '/search', to: 'search#result'
  #get '/search/reindex'

  root 'adversaries#index'

  get 'about' => 'pages#about'
  get 'updates' => 'pages#updates'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Ajax callbacks
  #get 'find/character_selection' => 'encounters#character_selection'
  #get 'find/adversary_selection' => 'encounters#adversary_selection'

end
