class CreateTalents < ActiveRecord::Migration[5.1]
  def change
    create_table :talents do |t|
      t.string :name
      t.text :description
      t.integer :tier
      t.string :activation
      t.boolean :ranked
      t.json :modifications
      t.string :slug

      t.timestamps
    end
  end
end
