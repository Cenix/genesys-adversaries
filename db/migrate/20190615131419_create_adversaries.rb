class CreateAdversaries < ActiveRecord::Migration[5.2]
  def change
    create_table :adversaries do |t|
      t.string :name
      t.text :description
      t.integer :brawn
      t.integer :agility
      t.integer :intellect
      t.integer :cunning
      t.integer :willpower
      t.integer :presence
      t.string :adversary_type
      t.integer :soak
      t.integer :wound_th
      t.integer :strain_th
      t.integer :defense_melee
      t.integer :defense_ranged
      t.json :skills
      t.json :talents
      t.json :abilities
      t.json :weapons
      t.text :equipment
      t.text :spells
      t.string :image_url
      t.string :slug

      t.timestamps
    end
  end
end
