class ChangeSpellsToBeJsonInAdversaries < ActiveRecord::Migration[5.2]
  def change
    remove_column :adversaries, :spells
    add_column :adversaries, :spells, :json, default: []
  end
end
