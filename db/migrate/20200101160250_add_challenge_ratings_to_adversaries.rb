class AddChallengeRatingsToAdversaries < ActiveRecord::Migration[5.2]
  def change
    add_column :adversaries, :combat_rating, :integer
    add_column :adversaries, :social_rating, :integer
    add_column :adversaries, :general_rating, :integer
  end
end
