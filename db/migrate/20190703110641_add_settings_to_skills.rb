class AddSettingsToSkills < ActiveRecord::Migration[5.2]
  def change
    add_column :skills, :settings, :json
  end
end
