class CreateItemQualities < ActiveRecord::Migration[5.2]
  def change
    create_table :item_qualities do |t|
      t.string :name
      t.text :description
      t.string :activation
      t.boolean :ranked

      t.timestamps
    end
  end
end
