puts '################################'
puts '# D&D as Genesys Adversaries.  #'
puts '################################'

adversary = Adversary.find_or_create_by(name: 'Shambling Mound')
adversary.update_attributes(
  brawn: 5,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 7,
  wound_th: 21,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Stealth').id, rank: 4},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  abilities: [
    {
      name: 'Absorb Magic',
      description: 'Spell attacks on a shambling mound reduce damage inflicted (before Soak) by 1 for each [t] generated; if a [des] is generated the spell will heal instead of harm the mound.'
    },
    {name: 'Aquatic', description: 'A shambling mound never treats water as difficult terrain and can breathe underwater.'},
    {name: 'Engulf', description: 'As an action, a shambling will engulf a silhouette 1 or smaller character it has immobilized; the character must succeed at an <b>Average ([d][d]) Athletics check</b> as an put-of-turn incidental to avoid being engulfed. The mound will inflict 8 damage on engulfed characters on each of its turns. Only one character may be engulfed.'},
    {name: 'Inconspicuous', description: 'In a natural environment, a mound resembles an ordinary plant; success on a <b>Hard ([d][d][d]) Perception or Vigilance check</b> will identify a mound.'},
    {name: 'Silhouette', description: '2'},
    {name: 'Sneak Attack', description: 'A mound that has not been identified as a threat may use Stealth to determine initiative.'},
  ],
  weapons: [
    {
      name: 'Battering limbs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [{id: ItemQuality.find_by_name('Ensnare').id, rank: 3}]
    }
  ],
  equipment: "<p></p>",
  spells: [],
  description: "<h5>Large carnivorous plant humanoid</h5><p>Shambling mounds, also called shamblers, appear to be heaps of rotting vegetation. They are actually intelligent, carnivorous plants.</p><p>A shambler’s brain and sensory organs are located in its upper body.</p><small>Source: <a href='https://www.scabard.com/pbs/campaign/235523/folder/330867'>The Rise of Vorakesh</a></small>",
  image_url: 'https://www.scabard.com/user/Gazrok/image/shamblingmound.png?imaged=260'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Fantasy')
adversary.tag_list.add('D&D', 'Elemental')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Basilisk')
adversary.update_attributes(
  brawn: 3,
  agility: 1,
  intellect: 1,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 6,
  wound_th: 14,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Perception').id, rank: 3},
    {id: Skill.find_by_name('Stealth').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
  ],
  abilities: [
    {name: 'Dark Vision', description: 'A basilisk suffers no Perception penalties at night or in complete darkness.'},
    {name: 'Petrifying Gaze', description: 'As a maneuver, a basilisk can force a target within Medium range to meet its deadly gaze. The basilisk makes an opposed <b>Perception vs. Discipline check</b>. If the target has not yet acted this turn, downgrade the difficulty by one. If the check succeeds, the target is immobilized and disoriented until the end of its next turn, when it may make a <b>Hard ([d][d][d]) Resilience check</b> as an incidental. If it succeeds, the effect ends. If it fails, the target is turned to stone and indefinitely Incapacitated. This may be dispelled by an ally with a successful <b>Hard ([d][d][d]) Arcana check</b>, or the character may be restored by a successful <b>Hard ([d][d][d]) Heal check</b> (using Divine or Primal). Potential targets may avoid the basilisk\'s gaze by tightly closing their eyes or averting their gaze. In either case, while avoiding the basilisk\'s gaze, a target is treated as if it is suffering the Blinded Critical Injury.'},
  ],
  weapons: [
    {
      name: 'Bite',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 2},
      ]
    },
  ],
  equipment: "<p></p>",
  spells: [],
  description: "<h5>Eight-legged reptile that can petrify with its gaze</h5><p>A basilisk is an eight-limbed reptilian monster that petrifies living creatures with a mere gaze. A basilisk usually has a dull brown body with a yellowish underbelly. Some specimens sport a short, curved horn atop the nose. An adult basilisk’s body grows to about 6 feet long, not including its tail, which can reach an additional length of 5 to 7 feet. The creature weighs about 300 pounds.</p><small>Source: <a href='https://www.scabard.com/pbs/campaign/235523/folder/330867'>The Rise of Vorakesh</a></small>",
  image_url: 'https://www.scabard.com/user/Gazrok/image/basilisk.jpeg?imaged=260'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Fantasy')
adversary.tag_list.add('D&D')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Beholder')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 4,
  cunning: 4,
  willpower: 4,
  presence: 4,
  adversary_type: 'Nemesis',
  soak: 6,
  wound_th: 22,
  strain_th: 30,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 4},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Coercion').id, rank: 4},
    {id: Skill.find_by_name('Deception').id, rank: 4},
    {id: Skill.find_by_name('Perception').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 4},
    {id: Skill.find_by_name('Vigilance').id, rank: 5},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
  ],
  abilities: [
    {name: 'Anti-Magic Field', description: "A beholder's central eye projects an anti-magic field out to long range, in which no magic will function; this ability may be activated or deactivated as a maneuver at a cost of 2 Strain. The beholder's eye rays do not function in this area."},
    {name: 'Dark Vision', description: 'A beholder removes up to [s][s] imposed due to darkness.'},
    {name: 'Eye Rays', description: 'A beholder can spend an action to attack with up to three eye rays against up to three characters; each ray attack costs 2 Strain to use and is an opposed Arcane check. All rays are effective to long range. See spells section for eye effects and opposing skills.'},
    {name: 'Flyer', description: 'Can fly; see page 100 of Genesys CRB'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Hard ([d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},
  ],
  weapons: [
    {
      name: 'Bite',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 3},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  equipment: "<p></p>",
  spells: [
    {name: 'Charm Ray', description: 'Discipline; target is under the beholder\'s control until the end of its next turn.'},
    {name: 'Paralyzation Ray', description: 'Resilience; target is immobilized until end of its next turn.'},
    {name: 'Fear Ray', description: 'Discipline; target must make a <b>Daunting ([d][d][d][d]) fear check</b>.'},
    {name: 'Slowing Ray', description: 'Resilience; target loses its free maneuver until the end of its next turn.'},
    {name: 'Enervation Ray', description: 'Resilience; target suffers strain damage equal to half of its remaining Strain.'},
    {name: 'Telekinetic Ray', description: 'Resilience; target is moved one range band in any direction.'},
    {name: 'Sleep Ray', description: 'Discipline; target falls asleep until the end its turn) next turn; others may spend an action to awoken.'},
    {name: 'Petrification Ray', description: 'Resilience; target is staggered and immobilized until the end of its next turn; beholder can spend [triumph] to turn target to stone.'},
    {name: 'Disintegration Ray', description: 'Resilience; target suffers 10 wound damage; if wound threshold is exceeded, beholder may spend [triumph] to reduce target to dust.'},
    {name: 'Death Ray', description: 'Resilience; target suffers 15 wound damage; if wound threshold is exceeded, beholder may spend [triumph] to slay the target'},
  ],
  description: "<h5>Eye Tyrant</h5><p>A beholder, sometimes called a sphere of many eyes or an eye tyrant, was a large aberration normally found in the Underdark. These large, orb-shaped beings had ten eyestalks and one central eye, each containing powerful magic. Powerful and intelligent, beholders were among the greatest threats to the world.</p><small>Source: <a href='https://www.scabard.com/pbs/campaign/235523/folder/330867'>The Rise of Vorakesh</a></small>",
  image_url: 'https://www.scabard.com/user/Gazrok/image/beholder2.jpg?imaged=260'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Fantasy')
adversary.tag_list.add('D&D')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Black Pudding')
adversary.update_attributes(
  brawn: 4,
  agility: 1,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Rival',
  soak: 8,
  wound_th: 22,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Perception').id, rank: 0},
    {id: Skill.find_by_name('Vigilance').id, rank: 0},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Amorphous', description: 'A black pudding can flow through narrow spaces.'},
    {name: 'Burn Vulnerability', description: 'Attacks with the Burn quality deal 2 damage per uncanceled [success].'},
    {name: 'Corrosive', description: 'When a black pudding is struck by a weapon, spend [threat] generated by the attack roll to damage the weapon one level.'},
    {name: 'Silhouette', description: '2'},
    {name: 'Split', description: 'If an attack on a black pudding generates [threat][threat][threat], the creature will split into two smaller puddings each with a wound threshold equal to half (rounded down) of the original creature\'s remaining wounds.'},
    {name: 'Wall Climber', description: 'A black pudding can travel along walls and ceilings as well as it does across floors.'},
  ],
  weapons: [
    {
      name: 'Pseudopods',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 3},
        {id: ItemQuality.find_by_name('Sunder').id, rank: 0},
        {id: ItemQuality.find_by_name('Superior').id, rank: 0},
      ]
    },
  ],
  equipment: "<p></p>",
  spells: [],
  description: "<h5>Related to slimes and oozes</h5><p>The typical black pudding measures 15 feet across and 2 feet thick. It weighs about 18,000 pounds.</p><small>Source: <a href='https://www.scabard.com/pbs/campaign/235523/folder/330867'>The Rise of Vorakesh</a></small>",
  image_url: 'https://www.scabard.com/user/Gazrok/image/blackpudding.jpg?imaged=260'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Fantasy')
adversary.tag_list.add('D&D')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Bulette')
adversary.update_attributes(
  brawn: 5,
  agility: 2,
  intellect: 1,
  cunning: 3,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 6,
  wound_th: 24,
  strain_th: nil,
  defense_melee: 2,
  defense_ranged: 2,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 3},
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Perception').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 4},
  ],
  talents: [
  ],
  abilities: [
    {name: 'Burrowing', description: 'A bulette may spend a maneuver to burrow underground or emerge from beneath the ground.'},
    {name: 'Dark Vision', description: 'A bulette may remove up to [s][s] imposed due to darkness.'},
    {name: 'Long Jump', description: 'A bulette that uses its Leaping Attack cannot use any maneuvers on the same turn; a bulette may jump no further than medium range'},
    {name: 'Silhouette', description: '2'},
    {name: 'Taste for Halflings', description: 'Bulettes love the taste of halfling and will preferentially attack halfling characters; a bulette receives [boost] on combat checks made against halflings, and [boost] to Perception or Vigilance checks when a halfling is present'},
    {name: 'Tremorsense', description: 'A bulette receives [boost] to Perception or Vigilance checks when searching for prey.'},
  ],
  weapons: [
    {
      name: 'Bite',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 9,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 3},
      ]
    },
    {
      name: 'Leaping Attack',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Auto-Fire').id, rank: 0},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>Shell (+2 defense).</p>",
  spells: [
  ],
  description: "<h5>Monstrous predator</h5><p>Also known as the landshark, the bulette is a terrifying predator that lives only to eat.</p><small>Source: <a href='https://www.scabard.com/pbs/campaign/235523/folder/330867'>The Rise of Vorakesh</a></small>",
  image_url: 'https://www.scabard.com/user/Gazrok/image/bulete.png?imaged=260'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Fantasy')
adversary.tag_list.add('D&D')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Cave Fisher')
adversary.update_attributes(
  brawn: 4,
  agility: 2,
  intellect: 1,
  cunning: 3,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 6,
  wound_th: 14,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Ranged').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
  ],
  abilities: [
    {name: 'Wall Crawler', description: 'A cave fisher may move along walls or ceilings without needing to make an Athletics check.'},
    {name: 'Anchored', description: 'A cave fisher may use a maneuver to anchor itself. While anchored, it is immune to forced movement and knockdown. It can anchor itself to a floor, wall, or ceiling. It cannot take maneuvers to move while anchored. It may cease being anchored as an incidental.'},
    {name: 'Filaments', description: 'A cave fisher extrudes a long, transparent, highly adhesive filament to capture prey.  It requires a <b>Hard ([d][d][d]) Vigilance check</b> to notice a cave fisher filament. A target that touches the filament is automatically ensnared. A cave fisher may also shoot its filament out to capture prey, though this is not its preferred method of attack. A filament can be escaped with a <b>Hard ([d][d][d]) Athletics check</b>, or severed by dealing 5 wounds to it [Soak 3]. Severing a filament does not injure a cave fisher. A cave fisher can extrude a replacement filament on its turn as a maneuver.'},
    {name: 'Reel', description: 'If a cave fisher Ensnares a target with a filament, it may use a maneuver to drag the target to itself.'},
  ],
  weapons: [
    {
      name: 'Claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Linked').id, rank: 1},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 1},
      ]
    },
    {
      name: 'Filaments',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 0,
      critical: 6,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 3},
      ]
    },
  ],
  equipment: "<p></p>",
  spells: [
  ],
  description: "<h5>Man-sized, crab-like monstrosity</h5><p>These subterranean arachnids resemble large, flat, stone-coloured scorpions with eight legs and two large, shearing claws. They lurk in elevated places in caves and caverns, dangling transparent filaments to ensnare prey. When a living creature blunders into a filament, it is quickly reeled up to the cave fisher to be dismembered and devoured.</p><small>Source: <a href='https://www.scabard.com/pbs/campaign/235523/folder/330867'>The Rise of Vorakesh</a></small>",
  image_url: 'https://www.scabard.com/user/Gazrok/image/cavefisher.jpg?imaged=260'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Fantasy')
adversary.tag_list.add('D&D')
adversary.save
puts "Adversary: #{adversary.name}"
