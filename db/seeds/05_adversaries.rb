puts '#########################'
puts '# Seeding Adversaries   #'
puts '#########################'
puts '#########################'
puts '# Fantasy Adversaries.  #'
puts '#########################'

adversary = Adversary.find_or_create_by(name: 'Skeleton')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 2,
  wound_th: 4,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
    {id: Skill.find_by_name('Perception').id, rank: 0},
    {id: Skill.find_by_name('Ranged').id, rank: 0},
    {id: Skill.find_by_name('Vigilance').id, rank: 0},
  ],
  talents: [],
  abilities: [],
  weapons: [
    {
      name: 'Rusty blade',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 4,
      range: 'Engaged',
      qualities: []
    },
    {
      name: 'Old bow',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 6,
      critical: 5,
      range: 'Medium',
      qualities: []
    }
  ],
  equipment: "<p></p>",
  spells: [],
  description: "<p>In Runebound, most animated skeletons serve in the undead legions of Waiqar the Undying. However, a few lurk in ancient tombs and battlefields across Terrinoth, their bones given a profane semblance of life as magic seeps into these dark and evil places.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/a/a7/Skeleton.jpg/revision/latest?cb=20181119165349'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Fantasy', 'Terrinoth')
adversary.tag_list.add('Undead')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Beastman')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 6,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Perception').id, rank: 0},
    {id: Skill.find_by_name('Vigilance').id, rank: 0},
  ],
  talents: [],
  abilities: [
    {
      name: 'Bestial Rage',
      description: 'A beastman or beastman minion group that has taken damage reduces the Critical rating of its attacks to 1.'
    }
  ],
  weapons: [
    {
      name: 'Fangs and claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: []
    }
  ],
  equipment: "<p></p>",
  spells: [],
  description: "<p>Beastmen are awful hybrids of human and animal. Although their particular features vary, most resemble predatory mammals, with thick fur, long claws, and big fangs. There are countless legends about the origins of beastmen, many attributing their inception to a curse for angering the gods. All are ferociously violent and almost impossible to communicate with.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/c/c6/Beastman.png/revision/latest?cb=20180607145019'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Fantasy', 'Terrinoth')
#adversary.tag_list.add()
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Razorwing')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 1,
  presence: 1,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 13,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 1,
  combat_rating: 4,
  social_rating: 1,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Coordination').id, rank: 2},
    {id: Skill.find_by_name('Perception').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [],
  abilities: [
    {
      name: 'Flyer',
      description: 'Can fly; see page 100 of Genesys CRB',
    },
    {
      name: 'Swoop Attack',
      description: 'After making a Brawl combat check, can move from engaged to short range of the target as an incidental.',
    }
  ],
  weapons: [
    {
      name: 'Fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: []
    },
    {
      name: 'Buffeting wings',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 4,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 3},
        {id: ItemQuality.find_by_name('Stun').id, rank: 3},
      ]
    }
  ],
  equipment: "<p></p>",
  spells: [],
  description: "<p>Although these large, vaguely humanoid, bat-like creatures will eat almost anything, they really prefer the meat of humans and other intelligent races. With some alterations, you can use this profile for other winged monsters, such as harpies and griffons.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/3/3d/Razorwing.png/revision/latest?cb=20181112170519'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Fantasy', 'Terrinoth')
adversary.tag_list.add('Flying')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Bane Spider')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 1,
  presence: 1,
  adversary_type: 'Rival',
  soak: 5,
  wound_th: 16,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 5,
  social_rating: 1,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Coordination').id, rank: 2},
    {id: Skill.find_by_name('Ranged').id, rank: 1},
    {id: Skill.find_by_name('Stealth').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [],
  abilities: [
    {
      name: 'Skitter',
      description: 'Can move across walls, ceilings, and giant spider webs without penalty',
    },
  ],
  weapons: [
    {
      name: 'Venomous fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Burn').id, rank: 2},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 3},
      ]
    },
    {
      name: 'Acid Spit',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 4,
      critical: 4,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Burn').id, rank: 2},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 3},
      ]
    },
    {
      name: 'Webbing',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 1,
      critical: 6,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 3},
      ]
    }
  ],
  equipment: "<p></p>",
  spells: [],
  description: "<p>Giant spiders are a mainstay of the fantasy genre. As horrifying as any huge arachnid, bane spiders have the added ability to spit globs of acidic venom. This venom, which bane spiders can also inject with their fangs, conveniently allows them to start digesting their prey before even closing to striking range.</p><p>Most bane spiders are roving hunters, but you might prefer one that shoots bundles of sticky webbing instead of (or in addition to) venom.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/7/7f/Bane_Spider.jpg/revision/latest?cb=20160112071419'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Fantasy', 'Terrinoth')
#adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Ogre')
adversary.update_attributes(
  brawn: 5,
  agility: 2,
  intellect: 2,
  cunning: 3,
  willpower: 2,
  presence: 1,
  adversary_type: 'Nemesis',
  soak: 7,
  wound_th: 20,
  strain_th: 12,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 9,
  social_rating: 1,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 2},
    {id: Skill.find_by_name('Melee (Heavy)').id, rank: 2},
    {id: Skill.find_by_name('Perception').id, rank: 1},
    {id: Skill.find_by_name('Ranged').id, rank: 2},
  ],
  talents: [],
  abilities: [
    {
      name: 'Regeneration',
      description: 'At the beginning of its turn, this creature automatically heals 3 wounds'
    },
    {
      name: 'Sweep Attack',
      description: 'May spend [triumph] on a successful melee combat check to inflict one hit dealing the weapon’s base damage on everyone (except the ogre!) engaged with the target.'
    },
  ],
  weapons: [
    {
      name: 'Cudgel',
      skill_id: Skill.find_by_name('Melee (Heavy)').id,
      damage: 10,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 2},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
    {
      name: 'Spiked Chain',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 7,
      critical: 5,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 2},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>Scavenged armor (+2 soak).</p>",
  spells: [],
  description: "<p>These huge brutes lair in ancient ruins and caves, eating whatever poor creatures they can get their massive hands on. We’ve included the ogre as a powerful monster able to challenge even experienced characters on its own. However, if you’re looking to make the ogre less challenging, remove Regeneration from its abilities.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/6/62/Thaadd.jpg/revision/latest?cb=20180607142341'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Fantasy', 'Terrinoth')
#adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"


puts '#########################'
puts '# Steampunk Adversaries #'
puts '#########################'

adversary = Adversary.find_or_create_by(name: 'Airship Pirate')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 6,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Coercion').id, rank: 0},
    {id: Skill.find_by_name('Melee').id, rank: 0},
    {id: Skill.find_by_name('Perception').id, rank: 0},
    {id: Skill.find_by_name('Piloting').id, rank: 0},
    {id: Skill.find_by_name('Ranged (Light)').id, rank: 0},
    {id: Skill.find_by_name('Vigilance').id, rank: 0},
  ],
  weapons: [
    {
      name: 'Revolver',
      skill_id: Skill.find_by_name('Ranged (Light)').id,
      damage: 6,
      critical: 3,
      range: 'Medium',
      qualities: [
      ]
    },
    {
      name: 'Cutlass',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 4,
      critical: 3,
      range: 'Engaged',
      qualities: [
      ]
    },
  ],
  equipment: "<p>Vest and storm coat (+1 soak).</p>",
  description: "<p>Air pirates ply the skyways across the Boiling Sea and beyond (and honestly, any setting with airships is going to have airship pirates). This profile is typical of any human opponents and can also serve for a brigand or mercenary.</p>",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Steampunk')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Royal Marine')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 2,
  cunning: 2,
  willpower: 3,
  presence: 2,
  adversary_type: 'Rival',
  soak: 4,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 5,
  social_rating: 2,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Melee').id, rank: 2},
    {id: Skill.find_by_name('Perception').id, rank: 2},
    {id: Skill.find_by_name('Piloting').id, rank: 2},
    {id: Skill.find_by_name('Ranged (Heavy)').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Eagle Eyes').id, rank: 0},
  ],
  abilities: [
    {name: 'Honor and Grit', description: 'When a Royal Marine exceeds their wound threshold, they can immediately attempt a Hard ([d][d][d]) Discipline check as an out-ofturn incidental, healing 1 wound per [su]; if this reduces their current wounds below their wound threshold, the Marine gets back on their feet to continue fighting'},
  ],
  weapons: [
    {
      name: 'Fusil',
      skill_id: Skill.find_by_name('Ranged (Heavy)').id,
      damage: 7,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
      ]
    },
    {
      name: 'Saber',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
      ]
    },
  ],
  equipment: "<p>Armored uniform (+1 soak).</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Steampunk')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Clockwork Animal Automaton')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Rival',
  soak: 5,
  wound_th: 15,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 3,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 1},
    {id: Skill.find_by_name('Perception').id, rank: 1},
    {id: Skill.find_by_name('Survival').id, rank: 1},
    {id: Skill.find_by_name('Vigilance').id, rank: 1},
  ],
  abilities: [
    {name: 'Mechanical', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
    {name: 'Clockwork fragility', description: 'If the automaton suffers a Critical Injury, add +20 to the result.'},
    {name: 'Flyer*', description: '*Some bird automata can fly; see page 100 of Genesys CRB'},
  ],
  weapons: [
    {
      name: 'Fists or claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 2},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  equipment: "<p></p>",
  description: "<p>Clockwork automata tend to be powered by coilsprings, their actions dictated by a complicated series of wax cylinders. Any kind of actual sentience, of course, is impossible, but certain animal behaviors can be worked into their clockwork minds.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Steampunk')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Colonial Governor')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 3,
  cunning: 4,
  willpower: 3,
  presence: 3,
  adversary_type: 'Nemesis',
  soak: 2,
  wound_th: 12,
  strain_th: 16,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 5,
  social_rating: 6,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 3},
    {id: Skill.find_by_name('Coercion').id, rank: 3},
    {id: Skill.find_by_name('Deception').id, rank: 3},
    {id: Skill.find_by_name('Leadership').id, rank: 3},
    {id: Skill.find_by_name('Melee').id, rank: 2},
    {id: Skill.find_by_name('Negotiation').id, rank: 3},
    {id: Skill.find_by_name('Perception').id, rank: 2},
    {id: Skill.find_by_name('Perception').id, rank: 2},
    {id: Skill.find_by_name('Ranged (Light)').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
    {id: Talent.find_by_name('Ruinous Repartee').id, rank: 0},
  ],
  abilities: [
    {
      name: 'For the Colony!',
      description: 'Once per round when targeted by a combat check, may choose one ally within short range, and the attack then targets that character instead of the colonial governor.'
    },
  ],
  weapons: [
    {
      name: 'Dueling Pistol',
      skill_id: Skill.find_by_name('Ranged (Light)').id,
      damage: 6,
      critical: 2,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Limited Ammo').id, rank: 2},
      ]
    },
    {
      name: 'Rapier',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 3,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 2},
      ]
    },
  ],
  equipment: "<p>Fine clothing.</p>",
  spells: [],
  description: "<p>Governors are powerful individuals without exception. Their abilities are primarily focused toward social encounters, and they usually show up for a fight backed by Marines, mercenaries, or even pirates.</p>",
  image_url: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/73e44b08-0259-4a1d-a091-131804caf873/d4e0w2l-7dff2867-f237-442c-a07b-4962e6c03fd7.jpg/v1/fill/w_194,h_250,q_70,strp/steampunk_governor_by_numbbie_d4e0w2l-250t.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9ODAwIiwicGF0aCI6IlwvZlwvNzNlNDRiMDgtMDI1OS00YTFkLWEwOTEtMTMxODA0Y2FmODczXC9kNGUwdzJsLTdkZmYyODY3LWYyMzctNDQyYy1hMDdiLTQ5NjJlNmMwM2ZkNy5qcGciLCJ3aWR0aCI6Ijw9NjIyIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.rcU-pp7n1q9DNlYjV47fujaeuojSNT-E-mjnIwgt72U'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Steampunk')
adversary.tag_list.add('Human')
adversary.save
puts "Adversary: #{adversary.name}"

puts '#########################'
puts '# Weird war Adversaries.#'
puts '#########################'

adversary = Adversary.find_or_create_by(name: 'Infantry')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 2,
  adversary_type: 'Minion',
  soak: 2,
  wound_th: 5,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 3,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Discipline').id, rank: 0},
    {id: Skill.find_by_name('Melee').id, rank: 0},
    {id: Skill.find_by_name('Ranged (Heavy)').id, rank: 0},
  ],
  weapons: [
    {
      name: 'Battle rifle',
      skill_id: Skill.find_by_name('Ranged (Heavy)').id,
      damage: 8,
      critical: 3,
      range: 'Long',
      qualities: [
      ]
    },
    {
      name: 'Bayonet',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 4,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {text: 'If attached to the end of a rifle, gains the Defensive 1 quality.'},
      ]
    },
  ],
  equipment: "<p>Armor vest (+1 soak).</p>",
  description: "<p>This profile presents the kind of basic infantry soldier found on the front lines of all sides, even in weird wars.</p>",
  image_url: 'https://i.ytimg.com/vi/RDEsz8hNb9I/hqdefault.jpg'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Weird war')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Occult Commando')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 2,
  cunning: 3,
  willpower: 3,
  presence: 2,
  adversary_type: 'Rival',
  soak: 5,
  wound_th: 16,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 5,
  social_rating: 2,
  general_rating: 4,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Coercion').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Perception').id, rank: 2},
    {id: Skill.find_by_name('Ranged (Light)').id, rank: 2},
    {id: Skill.find_by_name('Skulduggery').id, rank: 3},
    {id: Skill.find_by_name('Stealth').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  abilities: [
    {name: 'Arcane Weakness', description: 'Silver, blessed, or holy weapons and ammunition count their Crit rating as 1 when used against this character.'},
    {name: 'Unholy Animation', description: 'When hit by a combat check, spend [des] to ignore all damage from the attack.'},
  ],
  weapons: [
    {
      name: 'Rune-covered pistol',
      skill_id: Skill.find_by_name('Ranged (Light)').id,
      damage: 8,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
      ]
    },
    {
      name: 'Wicked knife',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 5,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 3},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  equipment: "<p>Combat armor and curiously tough skin (+2 soak).</p>",
  description: "<p>Whether they’re zombies, magically animated suits of armor, or humans possessed by ancient spirits, all occult supersoldiers are individuals who combine intense training and arcane enhancements to create a terrifying combatant. This one is a human possessed by some sort of malign entity (probably a demon).</p>",
  image_url: 'https://www.irishtimes.com/polopoly_fs/1.3689529.1541601033!/image/image.jpg_gen/derivatives/ratio_1x1_w1200/image.jpg'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Weird war')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Arcane Scientist')
adversary.update_attributes(
  brawn: 1,
  agility: 2,
  intellect: 4,
  cunning: 3,
  willpower: 2,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 1,
  wound_th: 14,
  strain_th: 18,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 4,
  social_rating: 3,
  general_rating: 4,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 2},
    {id: Skill.find_by_name('Coercion').id, rank: 3},
    {id: Skill.find_by_name('Discipline').id, rank: 3},
    {id: Skill.find_by_name('Knowledge').id, rank: 3},
    {id: Skill.find_by_name('Medicine').id, rank: 3},
    {id: Skill.find_by_name('Ranged (Light)').id, rank: 1},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
    {id: Talent.find_by_name('Scathing Tirade').id, rank: 0},
  ],
  abilities: [
    {name: 'Rise, My Minions!', description: 'Once per session, may make a <b>Hard ([d][d][d]) Mechanics</b> or <b>Medicine check</b> (depending on the type of minions the arcane scientist has); for each uncanceled [su], one incapacitated minion within medium range heals all wounds and rejoins the encounter; it may act in the current round).'},
  ],
  weapons: [
    {
      name: 'DEW pistol',
      skill_id: Skill.find_by_name('Ranged (Light)').id,
      damage: 6,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Burn').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Lab coat and goggles.</p>",
  description: "<p>This profile presents an occult researcher who specializes in medical experimentation using arcane abilities. If you want, you can change out the Medicine skill for Mechanics to reflect a mad scientist who creates death rays and walking tanks as opposed to biological horrors.</p>",
  image_url: 'http://1.bp.blogspot.com/-a_xWicFjVAU/Tg3Ka7SKUZI/AAAAAAAAGl0/RN4TKwIs_aI/s400/z.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Weird war')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

puts '##########################'
puts '# Modern day Adversaries.#'
puts '##########################'

adversary = Adversary.find_or_create_by(name: 'Militia')
adversary.update_attributes(
  brawn: 2,
  agility: 1,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 4,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Coercion').id, rank: 0},
    {id: Skill.find_by_name('Ranged (Heavy)').id, rank: 0},
  ],
  weapons: [
    {
      name: 'Assault rifle',
      skill_id: Skill.find_by_name('Ranged (Heavy)').id,
      damage: 8,
      critical: 3,
      range: 'Long',
      qualities: [
        {id: ItemQuality.find_by_name('Auto-Fire').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>Fatigues (+1 soak).</p>",
  description: "<p>Militia are irregular fighters driven by a political or religious cause. They are typically undisciplined, poorly trained, and poorly equipped individuals who, although not a match for professionals, are dangerous in groups.</p>",
  image_url: 'https://media.moddb.com/cache/images/groups/1/1/941/thumb_620x2000/concept-survivors.jpg'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Modern Day')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Corrupt Official')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 3,
  cunning: 3,
  willpower: 3,
  presence: 4,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 1,
  social_rating: 5,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Deception').id, rank: 2},
    {id: Skill.find_by_name('Knowledge').id, rank: 2},
    {id: Skill.find_by_name('Negotiation').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 1},
  ],
  talents: [
    {id: Talent.find_by_name('Natural').id, rank: 0},
  ],
  abilities: [
    {name: 'Local Politics', description: 'Once per session, may choose one character operating in their area of influence [community, precinct, or county, for example]; until the end of the session, the target must upgrade the difficulty of any social skill checks they make to interact with inhabitants of this area once.'},
  ],
  equipment: "<p>Cell phone, tablet.</p>",
  description: "<p>Wherever there is a bureaucracy, corrupt officials flourish. Whether a crooked politician, a bent cop, or a corrupt small-town sheriff, these opponents can make life exceedingly difficult for anyone who crosses them.</p>",
  image_url: 'https://ui-ex.com/images/corrupted-clipart-perjury.gif'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Modern Day')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Intelligence Agent')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 3,
  cunning: 4,
  willpower: 3,
  presence: 3,
  adversary_type: 'Nemesis',
  soak: 3,
  wound_th: 14,
  strain_th: 14,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 5,
  social_rating: 4,
  general_rating: 5,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 2},
    {id: Skill.find_by_name('Coercion').id, rank: 2},
    {id: Skill.find_by_name('Deception').id, rank: 2},
    {id: Skill.find_by_name('Ranged (Light)').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Skulduggery').id, rank: 3},
    {id: Skill.find_by_name('Stealth').id, rank: 3},
    {id: Skill.find_by_name('Streetwise').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
    {id: Talent.find_by_name('Natural').id, rank: 0},
  ],
  abilities: [
    {name: 'One Step Ahead', description: 'Once per round, after an opponent performs an action or maneuver, may spend one Story Point to perform an action or a maneuver as an out-of-turn incidental.'},
  ],
  weapons: [
    {
      name: 'Silenced pistol',
      skill_id: Skill.find_by_name('Ranged (Light)').id,
      damage: 5,
      critical: 4,
      range: 'Short',
      qualities: [
      ]
    },
  ],
  equipment: "<p>Encrypted cell phone.</p>",
  description: "<p>Intelligence agents—or spies, if you prefer—are some of the most cunning and elusive opponents encountered in modern day settings. Our spy is a jack-of-all-trades; they’re not an overly dangerous combatant, but their Adversary talent and One Step Ahead ability should make them a frustrating foe.</p>",
  image_url: 'https://banner2.kisspng.com/20180518/zkq/kisspng-espionage-detective-silhouette-intelligence-agency-secret-agent-5afe6ef43a8437.7687593415266239882397.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Modern Day')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Combat Drone')
adversary.update_attributes(
  brawn: 1,
  agility: 3,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Rival',
  soak: 4,
  wound_th: 10,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 1,
  combat_rating: 4,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Coordination').id, rank: 1},
    {id: Skill.find_by_name('Perception').id, rank: 2},
    {id: Skill.find_by_name('Ranged (Light)').id, rank: 1},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  talents: [
  ],
  abilities: [
    {name: 'Flyer', description: 'Can fly; see page 100 of Genesys CRB'},
    {name: 'Mechanical', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
    {name: 'Telepresence', description: 'Can operate independently, or can be controlled directly by an operator via wireless link; if being controlled, the combat drone counts as having ranks in a skill equal to the controller\'s ranks in that skill.'},
  ],
  weapons: [
    {
      name: 'Rapid-fire flechette gun',
      skill_id: Skill.find_by_name('Ranged (Light)').id,
      damage: 4,
      critical: 2,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Auto-Fire').id, rank: 0},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 2},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  equipment: "<p></p>",
  spells: [
  ],
  description: "<p>Combat drones may be built in any number of shapes and designs. This one is a futuristic quad-rotor drone with weapons and with a weak AI that can let it operate somewhat autonomously.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Science Fiction')
adversary.tag_list.add('Robot')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Exosuit Trooper')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 2,
  adversary_type: 'Rival',
  soak: 6,
  wound_th: 16,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 8,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 1},
    {id: Skill.find_by_name('Ranged (Heavy)').id, rank: 2},
    {id: Skill.find_by_name('Ranged (Light)').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 1},
  ],
  talents: [
  ],
  abilities: [
  ],
  weapons: [
    {
      name: 'Portable laser cannon',
      skill_id: Skill.find_by_name('Ranged (Heavy)').id,
      damage: 10,
      critical: 3,
      range: 'Long',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Burn').id, rank: 1},
        {id: ItemQuality.find_by_name('Cumbersome').id, rank: 4},
      ]
    },
    {
      name: 'Laser pistol',
      skill_id: Skill.find_by_name('Ranged (Light)').id,
      damage: 6,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Burn').id, rank: 1},
      ]
    },
    {
      name: 'Exosuit fists',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 3},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>Exosuit (+1 defense, +2 soak, +1 Brawn, +4 wound threshold; allows wearer to operate underwater and in vacuum for up to 1 hour).</p>",
  spells: [
  ],
  description: "<p>For a hard science fiction setting, we can assume at least some professional soldiers are equipped with full-body exosuits that enhance their strength while protecting them from damage. An exosuit increases the character’s Brawn, soak, and wound threshold, which we’ve incor- porated into the profile stats and noted in the equipment. If you use an exosuit trooper without a suit, make sure you remove the increases from the profile stats.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Science Fiction')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"