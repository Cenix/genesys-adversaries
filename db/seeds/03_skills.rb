puts '#########################'
puts '# Seeding Skills        #'
puts '#########################'

#ActsAsTaggableOn::Tagging.where(taggable_type: 'Skill').delete_all
#ActsAsTaggableOn::Tag.delete_all

skill = Skill.find_or_create_by(name: 'Alchemy')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul><li>Your character tries to identify a potion by taste.</li><li>Your character wants to name the ingredients needed for a certain elixir.</li><li>Your character tries to prepare a potion, elixir, poultice, tonic, or similar compound with wondrous or magical effects.</li><li>Your character attempts to prepare a remedy for a disease or illness.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character attempts to enchant an otherwise mundane liquid.</li><li>Your character desires to heal someone directly through medical treatment of their wounds or laying on hands.</li><li>Your character seeks to transmute lead into gold. That would clearly be magic!</li></ul>", characteristic: 'Intellect', skill_type: 'General')
skill.update_attributes(settings: ['Terrinoth', 'Fantasy', 'Steampunk', 'Weird war'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Arcana')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul><li>Your character wants to throw a fireball at a group of enemies.</li><li>Your character tries to use a crystal ball to observe the movements of a distant individual.</li><li>Your character attempts to erect a magical barrier to block a passage.</li><li>Your character wishes to curse a foe and bring misfortune on their actions.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character attempts to invoke the power of a deity to smite their foes.</li><li>Your character seeks to contact the spirits of the dead to consult them in some business.</li><li>Your character attempts to heal an ally.</li></ul>", characteristic: 'Intellect', skill_type: 'Magic')
skill.update_attributes(settings: ['Terrinoth', 'Fantasy'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Astrocartography')
skill.update_attributes(description: "<h6>Use this skill if...</h6><h6>Don't use this skill if...</h6>", characteristic: 'Intellect', skill_type: 'General')
skill.update_attributes(settings: ['Space opera'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Athletics')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul><li>Your character attempts to climb up or down a structure, particularly when the climb may be tricky or the drop to the bottom is significant.</li><li>Your character tries to swim in difficult conditions. High winds, waves, tides, and currents could all contribute to making swimming difficult enough to require an Athletics check./li><li>Your character tries to jump, either vertically or horizontally. Leaping across a deep chasm or trying to jump up and grab a fire escape to get away from an angry dog are both situations when your character needs to make an Athletics check.</li><li>Your character attempts to run for an extended time.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character attempts an activity without any chances of failure. If your character swims on a calm day in a shallow pond, goes for an early morning jog, or jumps over a small log, they don’t need to bother making a check.</li><li>Your character attempts a physical activity that relies more on hand-eye coordination and general agility than straight strength. Engaging in parkour and freerunning, swinging on a rope and rappelling down a surface, and most forms of gymnastics are activities better represented by the Coordination skill.</li></ul>", characteristic: 'Brawn', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Brawl')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul><li>Your character fights with their bare hands or a weapon specifically designed to augment an unarmed attack, such as cestus or brass knuckles (or even a roll of coins).</li><li>Your character tries to pin, grapple, or hold someone.</li><li>Your character uses some form of unarmed martial art.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character fights with a projectile weapon or a thrown weapon. If your character is targeting someone who is not within arm’s reach, they should be using the Ranged skill.</li><li>Your character tries to fix or modify a melee weapon. Repairing or creating weapons is usually handled by the Mechanics skill.</li></ul>", characteristic: 'Brawn', skill_type: 'Combat')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Charm')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul><li>Your character tries to persuade someone to do your character a favor, especially if it might be inconvenient, expensive, or even dangerous for that person.</li><li>Your character tries to appeal to someone’s better nature (even if it doesn’t exist!) to get them to do something out of character for that person.</li><li>Your character tries to flirt with, seduce, or make a romantic overture to someone.</li><li>Your character tries to make themselves look better to everyone around them. A lot of politicians and public figures have high ranks in Charm.</li><li>Your character performs in front of an audience, acting, playing music, telling jokes, or giving a speech.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character is not at all sincere about what they are saying or doing. If there’s duplicity or lying involved, your character should use the Deception skill.</li><li>Your character is being polite, but subtly implying violence or some other threat. In those cases, your character should use the Coercion skill.</li><li>Your character uses their authority (either through rank, station, or natural force of personality) to give orders. These are times for your character to use the Leadership skill.</li><li>Your character interacts with someone who is already friendly to them, or asks someone to do something that is not at all an inconvenience for them (generally, you don’t need to use Charm to ask your spouse to pick up something from the store on their way home from work).</ul>", characteristic: 'Presence', skill_type: 'Social')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Coercion')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul><li>Your character issues a threat, whether or not accompanied by hostile actions. Even an implied threat—such as gesturing toward a weapon—falls under the Coercion skill.</li><li>Your character questions or interrogates a prisoner.</li><li>Your character uses physical or psychological torture.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character issues orders backed by the threat of their authority (such as threatening troops with courts-martial if they don’t follow your character into battle). In cases like this, Leadership would be a better skill for your character to use.</li><li>Your character tries to drive a hard bargain with someone. As long as both sides are still getting something out of the deal, Negotiation should be the skill to use.</li><li>Your character interacts with someone who is already terrified of or completely cowed by your character. In these cases, any further threats would be superfluous.</li></ul>", characteristic: 'Willpower', skill_type: 'Social')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Computers')
skill.update_attributes(description: "<h6>Use this skill if...</h6><h6>Don't use this skill if...</h6>", characteristic: 'Intellect', skill_type: 'General')
skill.update_attributes(settings: ['Modern Day', 'Science Fiction', 'Space opera'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Cool')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character begins laying a trap, staging an ambush, or otherwise setting up a combat encounter in which your character initiates the combat and has to judge the right time to do so.</li>
<li>Your character needs to stay calm and unaffected when being flattered or charmed by someone.</li>
<li>Your character needs to refrain from saying or doing something foolish during a tense situation.</li>
<li>Your character needs to keep their nerve in a tense situation, such as when piloting one of two vehicles headed toward each other at high speed.</li>
<li>Your character plays a card game or other game of chance in which bluffing, luck, and gambling are all intertwined.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character tries to prevent being surprised. The Vigilance skill would work better in that situation.</li>
<li>Your character tries to maintain inner self-control, such as when meditating or resisting the effects of fear. When your character is concerned with inner composure, they should use the Discipline skill.</li>
</ul>", characteristic: 'Presence', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Coordination')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character tries to swing back and forth on a rope or rappel down a structure.</li>
<li>Your character walks across a narrow surface while trying to keep their balance.</li>
<li>Your character tries to squeeze into a tiny or cramped space such as a crawlspace, sewer pipe, air duct, or narrow crevice.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character falls and needs to try to slow the fall or land safely.</li>
<li>Your character needs to escape from physical restraints (such as handcuffs or ropes) and wants to contort their limbs or hands so that they can slip out of their bindings.</li>
</ul>", characteristic: 'Agility', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Deception')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character tells a lie.</li>
<li>Your character tries to mislead someone through clever wordplay or selective omission of certain facts.</li>
<li>Your character wears a disguise and pretends to be someone else.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character actually believes the things they are saying (even if they are objectively untrue).</li>
<li>Your character tells a “white lie,” a minor falsehood to make someone feel better.</li>
</ul>", characteristic: 'Cunning', skill_type: 'Social')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Discipline')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character confronts something terrifying and wants to avoid fleeing in horror (or to avoid other debilitating effects of fear).</li>
<li>Your character tries to keep their sanity in the face of something that defies reality and rational thought.</li>
<li>Your character wants to heal strain they are suffering from at the end of an encounter.</li>
<li>Your character wants to meditate, calm their mind, and reach a mental equilibrium.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character tries to keep their composure in a social setting and avoid letting their emotions show. Your character would make a Cool check instead.</li>
<li>Your character catches a lie as it is being told. Noticing a lie depends on your character’s Vigilance.</li></ul>", characteristic: 'Willpower', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Divine')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character lays hands on someone to heal their wounds or disease.</li>
<li>Your character calls on the strength of their faith to bolster an ally.</li>
<li>Your character prays to their deity for a sign.</li>
<li>Your character presents their holy symbol in an effort to turn back the undead.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character wants to fling arcane energy at a living enemy.</li>
<li>Your character recites scripture in an attempt to sway a crowd. This would require the Charm or Leadership skill.</li>
<li>Your character tries to counter an enemy’s spell.</li>
</ul>", characteristic: 'Willpower', skill_type: 'Magic')
skill.update_attributes(settings: ['Terrinoth', 'Fantasy'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Driving')
skill.update_attributes(description: "<h6>Use this skill if...</h6><h6>Don't use this skill if...</h6>", characteristic: 'Agility', skill_type: 'General')
skill.update_attributes(settings: ['Steampunk', 'Weird war', 'Modern Day', 'Science Fiction', 'Space opera'])
puts "Skill: #{skill.name}"


skill = Skill.find_or_create_by(name: 'Gunnery')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character shoots a machine gun, cannon, missile launcher, or other “crewed” weapon.</li>
<li>While in a vehicle or spaceship, your character wants to fire the vehicle’s weapon systems.</li>
</ul>
<h6>Don't use this skill if...</h6><ul>
<li>Your character is firing a pistol, submachine gun, flechette pistol, laser pistol, or similar weapon.</li>
<li>Your character shoots a shotgun, assault rifle, flamethrower, flechette launcher, sniper rifle, or similar weapon.</li>
</ul>", characteristic: 'Agility', skill_type: 'Combat')
skill.update_attributes(settings: ['Steampunk', 'Weird war', 'Modern Day', 'Science Fiction', 'Space opera'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Knowledge')
skill.update_attributes(characteristic: 'Intellect', skill_type: 'General', description: "")
skill.update_attributes(settings: ['Steampunk', 'Weird war', 'Modern Day', 'Science Fiction', 'Space opera', 'Fantasy'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Leadership')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character’s allies are suffering from fear (see page 243), and you want to try to rally them.</li>
<li>Your character tries to convince a crowd of citizens to take political action.</li>
<li>Your character leads troops into battle and wants to make sure they follow your character’s orders.</li>
<li>Your character tries to convince a mob of rioters to stand down and return to their homes.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character threatens to hurt or kill someone if they don’t obey. This would be a good use of Coercion, instead.</li>
<li>Your character tries to convince someone to do something simply by being friendly and appealing. Your character should use Charm here.</li>
<li>Your character has formal authority and issues routine orders, especially outside of combat or other stressful situations. If there is no good reason not to obey your character (and your character has the rank or station to issue orders), other people are simply going to obey most mundane commands automatically.</li>
</ul>", characteristic: 'Presence', skill_type: 'Social')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Mechanics')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character needs to repair a damaged weapon, vehicle, or other piece of equipment.</li>
<li>Your character needs to identify any parts or tools necessary prior to completing a job. This can save time and money on the project.</li>
<li>Your character has access to a supply of components and tools and wants to design a completely new device.</li>
<li>Your character needs to sabotage an enemy’s vehicle or find a weak point in their defenses.</li>
<li>Your character needs to build an item or modify it.</li>
<li>Your character tries to install and modify cybernetic implants (although if you want, working on a cybernetic implant may require a Medicine and a Mechanics check to represent the fusion of human and machine).</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character has just a simple task like refueling a vehicle, hanging a door, or changing the batteries in a flashlight.</li>
<li>Your character wants to program an application for a device. That would require a Computers check.</li>
</ul>", characteristic: 'Intellect', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Medicine')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>They or another character has suffered wounds, and your character wants to heal those wounds.</li>
<li>Your character tries to counteract or administer a poison.</li>
<li>Your character needs to cure a disease.</li>
<li>Your character creates a new pharmaceutical (or recreational) drug.</li>
<li>They or another character has suffered a Critical Injury, and your character wants to heal it.</li>
<li>Your character performs a complex medical procedure such as surgery, cybernetic augmentation, or psychotherapy.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character researches a disease or poison. While studying a disease or poison directly might require Medicine, the act of researching requires a Knowledge check.</li>
<li>Your character tries to heal their own strain at the end of an encounter. Recovering from strain at the end of an encounter requires Discipline or Cool.</li>
<li>Your character tries to administer poison through slight of hand, such as by dropping it in a drinking cup or surreptitiously injecting it into an unsuspecting target. The inherent subterfuge in this activity makes that a Skulduggery check.</li>
</ul>", characteristic: 'Intellect', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Melee')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character fights with a sword, rapier, dirk, mace, axe, spear, halberd, knife…we could go on, but you get the idea.</li>
<li>Your character fights in a duel.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character fights with a projectile weapon or a thrown weapon. If your character targets someone not at arm’s reach, they should use the Ranged skill.</li>
</ul>", characteristic: 'Brawn', skill_type: 'Combat')
skill.update_attributes(settings: ['Steampunk', 'Weird war', 'Modern Day', 'Science Fiction', 'Space opera'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Melee (Heavy)')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character fights with a halberd, greatsword, flail, maul, or other large weapon that requires two hands to wield.</li>
<li>Your character picks up a heavy tree branch and tries to crush someone’s skull with it.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character fights with a rapier, dirk, mace, onehanded axe, light spear, sword, katana, knife, or other weapon that can be swung easily with one hand.</li>
<li>Your character tries to fix or modify a melee weapon. Repairing or creating weapons is usually handled by the Mechanics skill.</li>
</ul>", characteristic: 'Brawn', skill_type: 'Combat')
skill.update_attributes(settings: ['Fantasy', 'Terrinoth'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Melee (Light)')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character fights with a rapier, dirk, mace, one-handed axe, light spear, sword, katana, knife, or other weapon easily wielded in one hand.</li>
<li>Your character wants to hit someone with their shield.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character fights with a halberd, greatsword, flail, maul, or other large weapon that requires two hands to wield.</li>
</ul>", characteristic: 'Brawn', skill_type: 'Combat')
skill.update_attributes(settings: ['Fantasy', 'Terrinoth'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Negotiation')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character tries to purchase goods or services and wants to haggle over the price.</li>
<li>Your character tries to sell goods or services and turn a profit. In this case, your character needs to use Negotiation to raise the price.</li>
<li>Your character attempts to broker a political agreement or treaty between two parties.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character isn’t offering anything in return for what they want. Getting something for nothing is something your character can try to do using other social skills, but Negotiation is predicated on the idea of an exchange.</li>
<li>Your character tells someone what to do. Negotiation has to be a bargain, so at the end of the interactions, the opposing party has agreed to do something, not been ordered to do it.</li>
<li>Your character wants to buy something for a previously established price.</li>
</ul>", characteristic: 'Presence', skill_type: 'Social')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Operating')
skill.update_attributes(description: "", characteristic: 'Intellect', skill_type: 'Social')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Perception')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character wants to search a crime scene for clues.</li>
<li>Your character wants to study the surrounding landscape for possible threats.</li>
<li>Your character conducts surveillance on an unaware target from a distance.</li>
<li>Your character studies an ancient relic, trying to spot any minute details that could reveal its purpose or construction.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character tries to avoid being surprised during an ambush. Constant, unconscious awareness of your character’s surroundings is a function of the Vigilance skill.</li>
<li>Your character is being lied to, and you’re trying to find out if your character noticed or not. Again, Vigilance is the skill for this situation.</li>
<li>Your character tries to follow a trail or track a foe through the wilderness. The Survival skill covers these activities.</li>
</ul>", characteristic: 'Cunning', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Piloting')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character needs to land an airplane with one (or both!) engines out.</li>
<li>Your character tries to outmaneuver opponents in a dogfight.</li>
<li>Your character flies a drop ship through a maze of needle-sharp rock spires to a landing zone.</li>
<li>Your character attempts to land a personal dirigible while under fire from an enemy airship.</li>
</ul><h6>Don't use this skill if...</h6><ul>
Your character tries to repair a damaged aircraft propeller or engine, a task that calls for the Mechanics skill.</li>
<li>Your character drives a car off a precipice and wants to land with as few fatalities as possible; that’s still Driving (and likely with a pretty high difficulty!)</li>
<li>Your character fires a weapon mounted on a vehicle, which requires a Ranged or Gunnery check.</li>
<li>There is no chance of catastrophic failure when operating the vehicle. Checks are not required for simple tasks like flying in open skies with light wind.</li>
</ul>", characteristic: 'Agility', skill_type: 'General')
skill.update_attributes(settings: ['Steampunk', 'Weird war', 'Modern Day', 'Science Fiction', 'Space opera'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Primal')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character wants to heal a friend.</li>
<li>Your character attempts to communicate with plants or animals or gain the cooperation of natural forces.</li>
<li>Your character wishes to manipulate or control the weather, including summoning storms or throwing lighting at foes.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character attempts to fire a magic bolt at an enemy or group of enemies.</li>
<li>Your character tries to raise or reanimate the dead.</li>
</ul>", characteristic: 'Cunning', skill_type: 'Magic')
skill.update_attributes(settings: ['Fantasy', 'Terrinoth'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Ranged')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul><li>Your character fights with a crossbow, longbow, blowgun, pistol, rifle, machine gun, laser pistol, missile launcher…again, you probably get the idea.</li><li>Your character uses the weaponry of a vehicle such as the cannon on a tank, the machine guns on a fighter plane, or the Gauss rifles on a starfighter.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character fights with any kind of close combat weapon. Those are handled by the Melee skill.</li><li>Your character uses a ranged weapon to hit someone within arm’s reach, such as by taking the butt of their rifle and using it like a club. Even though they’re using a ranged weapon, they’re using it as if it were a melee weapon, and the check should be handled by the Melee skill.</li><li>Your character tries to fix or modify a ranged weapon. Repairing or creating weapons is usually handled by the Mechanics or Knowledge skill.</li></ul>", characteristic: 'Agility', skill_type: 'Combat')
skill.update_attributes(settings: ['Fantasy', 'Terrinoth'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Ranged (Light)')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character shoots a pistol, submachine gun, flechette pistol, laser pistol, or a similar weapon.</li>
<li>Your character wants to hurl a throwing knife.</li>
<li>Your character wants to toss a grenade.</li>
</ul>
<h6>Don't use this skill if...</h6><ul>
<li>Your character is firing a shotgun, rifle, laser cannon, machine gun, or other large, heavy weapon.</li>
<li>Your character is firing a weapon mounted on a spaceship or vehicle.</li>
</ul>", characteristic: 'Agility', skill_type: 'Combat')
skill.update_attributes(settings: ['Steampunk', 'Weird war', 'Modern Day', 'Science Fiction', 'Space opera'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Ranged (Heavy)')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
  <li>Your character shoots a shotgun, assault rifle, flamethrower, flechette launcher, sniper rifle, or similar weapon.</li>
</ul>
<h6>Don't use this skill if...</h6><ul>
<li>Your character is firing a pistol, submachine gun, flechette pistol, laser pistol, or a similar weapon.</li>
<li>Your character is firing a weapon mounted on a spaceship or vehicle.</li>
</ul>", characteristic: 'Agility', skill_type: 'Combat')
skill.update_attributes(settings: ['Steampunk', 'Weird war', 'Modern Day', 'Science Fiction', 'Space opera'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Resilience')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character tries to go without sleeping for days on end, and you need to see if they stay awake.</li>
<li>Your character ingests a toxin, and you need to see how bad the effects are.</li>
<li>Your character endures a hostile environment (somewhere too hot, too cold, or even too polluted) for days on end.</li>
<li>Your character attempts to recover from a Critical Injury on their own, without medical attention.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character tries to do something that isn’t beyond the limits of normal endurance. Going for a day-long hike wouldn’t call for a Resilience check unless the hike is through the Rocky Mountains in a snowstorm.</li>
<li>Your character immediately stops and rests to recover fully at the end of the activity. If there’s no need to track lasting consequences, there’s no need to make the check.</li>
</ul>", characteristic: 'Brawn', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Riding')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character flees from pursuers who are also mounted, or fast enough to potentially catch up.</li>
<li>Your character tries to joust at a tournament.</li>
<li>Your character competes in a friendly (or not so friendly) race.</li>
<li>Your character tries to catch up to enemies with a significant head start.</li>
<li>Your character’s mount panics during a storm, and your character needs to get the creature under control.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character travels without any immediate danger.</li>
<li>Your character makes an attack from horseback. The additional difficulty brought about by attacking from a horse should be factored into the combat check’s difficulty, generally in the form of one or more [setback].</li>
<li>Your character tries to tame a wild animal. In this case, your character uses the Survival skill.</li>
</ul>", characteristic: 'Agility', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Skulduggery')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character attempts to pick someone’s pocket or lift their wallet.</li>
<li>Your character tries to pick a lock or disable a trap. Your character would also use Skulduggery to set a trap in the first place.</li>
<li>Your character wants to study a security system.
<li>Your character attempts to distract an opponent through guile or a feint, such as by throwing a handful of dirt in their eyes during a fight.</li>
<li>Your character tries to surreptitiously slip a poison into someone’s food or drink.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character attempts to sneak into a location unnoticed. Your character needs to make a Stealth check instead.</li>
<li>Your character attempts to pick someone’s pocket when that person is helpless or incapacitated. This doesn’t require a check at all.</li>
<li>Your character tries to make a poison. Your character needs Medicine to make poisons or toxins, but they do need Skulduggery to use them.</li>
</ul>", characteristic: 'Cunning', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Stealth')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character attempts to hide from someone.</li>
<li>Your character tries to tail someone through a crowd, and to do it without being noticed.</li>
<li>Your character tries to infiltrate a government installation while avoiding both electronic security and human guards.</li>
<li>Your character tries to move quietly through a house.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character tries to pick someone’s pocket. Your character needs Skulduggery for this activity.</li>
<li>Your character tries to remain hidden when their opponent has no chance of spotting them, such as if they try to avoid being seen by an aircraft during a blizzard at midnight.</li>
<li>Your character has no realistic chance of hiding from an opponent, such as if trying to hide from a nearby person while in the middle of miles of salt flats at noon.</li>
</ul>", characteristic: 'Agility', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Streetwise')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul>
<li>Your character looks for a merchant who sells black-market goods or illegal services.</li>
<li>Your character wants to understand particular references or slang in a conversation.</li>
<li>Your character tries to approach criminals and start up a conversation without appearing like an outsider or a threat.</li>
<li>Your character tries to find their way around an unfamiliar city.</li>
<li>Your character tries to track and hunt someone somewhere in a city.</li>
</ul><h6>Don't use this skill if...</h6><ul>
<li>Your character tries to find their way around a rural or wilderness environment. In this case, your character should be using Survival.</li>
<li>Your character interacts with the upper crust of society. Charm (or possibly Deception or Coercion) may serve the character better here.</li>
<li>Your character has already established themself as a member of the criminal underworld, and is continuing to interact with other criminals. Streetwise lets your character fit in, know how to act, and know what topics to bring up and what to avoid. However, it shouldn’t replace social skills.</li></ul>", characteristic: 'Cunning', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Survival')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul><li>Your character is trapped in the wilderness and needs to find food and potable water.</li><li>Your character needs to notice approaching severe weather and know how to prepare for it.</li><li>Your character needs to follow a crude map or directions through a rural area to find a specific location.</li><li>Your character tries to tame or calm a wild animal, or handle a domesticated animal.</li><li>Your character hunts something (or someone!) through a wilderness setting.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character uses a highly accurate and detailed map to find a location.</li><li>Your character tries to find their way around an urban environment. In this case, your character should be using Streetwise.</li><li>Your character interacts with an animal that already likes or respects your character, or your character asks an animal to do something completely within their nature (they wouldn’t need to make a Survival check to get a dog to play “fetch,” for example).</li></ul>", characteristic: 'Cunning', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Vigilance')
skill.update_attributes(description: "<h6>Use this skill if...</h6><ul><li>Your character just got ambushed, and you are rolling to determine Initiative order. A high Vigilance means your character has a better chance of reacting quickly to the threat.</li><li>Your character is being lied to; the opponent’s Deception check is opposed by your character’s Vigilance skill.</li><li>Your character has a chance to notice important details in their surroundings while not looking for them directly.</li></ul><h6>Don't use this skill if...</h6><ul><li>You are determining Initiative order when your character is not surprised (such as when they are the ambushers, instead of the ambushed). In this case, your character uses Cool instead.</li><li>Your character actively looks for something. This calls for a Perception check.</li></ul>", characteristic: 'Willpower', skill_type: 'General')
skill.update_attributes(settings: ['All'])
puts "Skill: #{skill.name}"

puts '############################'
puts '# Seeding Terrinoth Skills #'
puts '############################'
skill = Skill.find_or_create_by(name: 'Adventuring')
skill.update_attributes(characteristic: 'Intellect', skill_type: 'Knowledge', description: "<h6>Use this skill if...</h6><ul><li>Your character tries to identify a strange, slimy mass hanging from a dungeon ceiling.</li><li>Your character attempts to find the safest course across a treacherously unstable ruin.</li><li>Your character is attempting to solve a complicated puzzle of moving statues and levers in the depths of an ancient tomb.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character wants to determine the provenance of a strange amulet found in the depths of a ruin. That would use Knowledge (Lore).</li><li>Your character tries to use their reflexes and dexterity to avoid falling into a pit trap. Coordination is the appropriate skill for this situation (although Vigilance might help them avoid stepping on the trap in the first place).</li><li>Your character is relying on their alertness to avoid dangers underground, which would use Vigilance.</li></ul>")
skill.update_attributes(settings: ['Terrinoth'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Forbidden')
skill.update_attributes(characteristic: 'Intellect', skill_type: 'Knowledge', description: "<h6>Use this skill if...</h6><ul><li>Your character attempts to decipher the arcane glyphs adorning a Reanimate’s ancient blade.</li><li>Your character tries to uncover the ritual by which to contact a denizen of the Ynfernael.</li><li>Your character wants to identify the magic employed by a Daewyl Elf.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character attempts to cast a spell using energy drawn from the Ynfernael. That would use Divine.</li><li>Your character researches the magic practiced by the wizards at Greyhaven University. Magic of this sort is covered by Knowledge (Lore).</li><li>Your character attempts to identify a magic amulet from the days of the Penacor Kings. That would use Knowledge (Lore).</li></ul>")
skill.update_attributes(settings: ['Terrinoth'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Lore')
skill.update_attributes(characteristic: 'Intellect', skill_type: 'Knowledge', description: "<h6>Use this skill if...</h6><ul><li>Your character tries to identify the origins of an ancient sword recovered from a ruin.</li><li>Your character wants to determine the source of a wizard’s magic after witnessing it in action.</li><li>Your character attempts to recall a terrible legend of the First Darkness.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character tries to cast a spell. This would require the use of a magic skill.</li><li>Your character attempts to read a map. That would use Knowledge (Geography).</li><li>Your character wants to chart a safe course through ancient ruins. That would use Knowledge (Adventuring).</li></ul>")
skill.update_attributes(settings: ['Terrinoth'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Geography')
skill.update_attributes(characteristic: 'Intellect', skill_type: 'Knowledge', description: "<h6>Use this skill if...</h6><ul><li>Your character wants to chart a course through dangerous wilderness to a nearby village.</li><li>Your character wants to indicate the approximate location of a landmark or settlement from memory.</li><li>Your character needs to select appropriate garb so as not to stand out among the locals.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character wants to recall information about the Darklands. This would require the Knowledge (Forbidden) skill.</li><li>Your character needs to set a camp or deal with the other practical matters of traversing the wilderness. That would use Survival.</li><li>Your character tries to recall the history of a ruin. They would use Knowledge (Lore) for that.</li></ul>")
skill.update_attributes(settings: ['Terrinoth'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Runes')
skill.update_attributes(characteristic: 'Intellect', skill_type: 'Magic', description: "<h6>Use this skill if...</h6><ul><li>Your character wants to use an immolation rune to unleash a great gout of flame or simply light a campfire.</li><li>Your character wants to create a protective barrier of ice using an ice storm rune.</li><li>Your character wants to identify the meaning and purpose of a rune inscribed on a runebound shard.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character wants to cast a spell without the aid of a runebound shard. That would require a different magic skill, depending on the spell involved.</li><li>Your character attempts to translate the glyphs in an ancient Dwarven ruin. That would use the Knowledge (Lore) skill.</li><li>Your character wants to use a runebound shard’s activation effect, as this does not require a magic skill check.</li></ul>")
skill.update_attributes(settings: ['Terrinoth'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Verse')
skill.update_attributes(characteristic: 'Presence', skill_type: 'Magic', description: "<h6>Use this skill if...</h6><ul><li>Your character wants to fortify their allies in battle with an inspiring song.</li><li>Your character wants to demoralize their foe with a scathing limerick, to the point that it physically hampers their abilities.</li><li>Your character wants to counteract the baleful spells of a necromancer with a rousing speech.</li></ul><h6>Don't use this skill if...</h6><ul><li>Your character wants to sing an ordinary song. That would use Charm.</li><li>Your character wants to throw a fireball or otherwise physically damage the foe. That would require a different magic skill, such as Arcana.</li></ul>")
skill.update_attributes(settings: ['Terrinoth'])
puts "Skill: #{skill.name}"

puts '############################'
puts '# Seeding Android Skills.  #'
puts '############################'

skill = Skill.find_or_create_by(name: 'Society')
skill.update_attributes(characteristic: 'Intellect', skill_type: 'Knowledge', description: "")
skill.update_attributes(settings: ['Android'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Science')
skill.update_attributes(characteristic: 'Intellect', skill_type: 'Knowledge', description: "")
skill.update_attributes(settings: ['Android'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'The Net')
skill.update_attributes(characteristic: 'Intellect', skill_type: 'Knowledge', description: "")
skill.update_attributes(settings: ['Android'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Computers (Hacking)')
skill.update_attributes(characteristic: 'Intellect', skill_type: 'General', description: "")
skill.update_attributes(settings: ['Android'])
puts "Skill: #{skill.name}"

skill = Skill.find_or_create_by(name: 'Computers (Sysops)')
skill.update_attributes(characteristic: 'Intellect', skill_type: 'General', description: "")
skill.update_attributes(settings: ['Android'])
puts "Skill: #{skill.name}"

ActsAsTaggableOn.remove_unused_tags = true