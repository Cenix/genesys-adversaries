puts '#########################'
puts '# Android Adversaries.  #'
puts '#########################'

adversary = Adversary.find_or_create_by(name: 'Aaron Marron')
adversary.update_attributes(
  brawn: 4,
  agility: 2,
  intellect: 2,
  cunning: 4,
  willpower: 2,
  presence: 4,
  adversary_type: 'Nemesis',
  soak: 4,
  wound_th: 17,
  strain_th: 12,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 7,
  social_rating: 5,
  general_rating: 4,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Coercion').id, rank: 4},
    {id: Skill.find_by_name('Leadership').id, rank: 3},
    {id: Skill.find_by_name('Melee').id, rank: 4},
    {id: Skill.find_by_name('Ranged (Light)').id, rank: 3},
    {id: Skill.find_by_name('Streetwise').id, rank: 4},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Field Commander').id, rank: 0},
    {id: Talent.find_by_name('Quick Draw').id, rank: 0},
  ],
  abilities: [
    {name: 'Bad Rep', description: 'When another character fails a check targeting this character, that character suffers 2 strain.'},
    {name: 'Charismatic Menace', description: 'Allies within short range add [a] to checks they make; opponents within short range add [t] to checks they make.'},
  ],
  weapons: [
    {
      name: 'Custom Six-shooter',
      skill_id: Skill.find_by_name('Ranged (Light)').id,
      damage: 7,
      critical: 2,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
      ]
    },
    {
      name: 'Pocket plasma knife',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 8,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Burn').id, rank: 1},
        {id: ItemQuality.find_by_name('Prepare').id, rank: 1},
        {id: ItemQuality.find_by_name('Sunder').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>Sleeveless shirt, bandannas, silver crossed pistols on chain.</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Android', 'Science Fiction')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Adam Industrial Labor Bioroid')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 2,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 6,
  wound_th: 6,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 0},
    {id: Skill.find_by_name('Mechanics').id, rank: 0},
  ],
  abilities: [
    {name: 'Bioroid', description: 'Does not need to breathe, eat, or drink; can survive in vacuum and underwater; is immune to poisons and toxins; and cannot knowingly endanger humans.'},
  ],
  weapons: [
  ],
  equipment: "<p></p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Android', 'Science Fiction')
adversary.tag_list.add('Bioroid')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Adonis and Eve Pleasure Bioroid')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 2,
  cunning: 1,
  willpower: 1,
  presence: 4,
  adversary_type: 'Rival',
  soak: 5,
  wound_th: 10,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 5,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 4},
    {id: Skill.find_by_name('Cool').id, rank: 4},
    {id: Skill.find_by_name('Coordination').id, rank: 2},
    {id: Skill.find_by_name('Deception').id, rank: 3},
    {id: Skill.find_by_name('Society').id, rank: 3},
    {id: Skill.find_by_name('Perception').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Natural').id, rank: 0},
  ],
  abilities: [
    {name: 'Bioroid', description: 'Does not need to breathe, eat, or drink; can survive in vacuum and underwater; is immune to poisons and toxins; and cannot knowingly endanger humans.'},
  ],
  weapons: [
  ],
  equipment: "<p>Fuzzy restraints.</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Android', 'Science Fiction')
adversary.tag_list.add('Bioroid')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Anarch Troublemaker')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 3,
  cunning: 3,
  willpower: 1,
  presence: 3,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 10,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 2,
  general_rating: 7,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 3},
    {id: Skill.find_by_name('Computers (Hacking)').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 4},
    {id: Skill.find_by_name('The Net').id, rank: 2},
    {id: Skill.find_by_name('Skulduggery').id, rank: 4},
    {id: Skill.find_by_name('Streetwise').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 1},
  ],
  talents: [
    {id: Talent.find_by_name('Distinctive Style').id, rank: 0},
  ],
  abilities: [
    {name: 'Burn It Down', description: 'This character adds [ad][ad] to the results of checks they make targeting authority figures.'},
  ],
  weapons: [
    {
      name: 'Fletcher Pistol',
      skill_id: Skill.find_by_name('Ranged (Light)').id,
      damage: 4,
      critical: 2,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 2},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
    {
      name: 'Molotov Cocktail',
      skill_id: Skill.find_by_name('Ranged (Light)').id,
      damage: 7,
      critical: 4,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Blast').id, rank: 7},
        {id: ItemQuality.find_by_name('Burn').id, rank: 3},
      ]
    },
  ],
  equipment: "<p>Skulljack, Portable Terminal with Crypsis (SotB p.137) - protected by Hadrian's Wall (SotB p. 133).</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Android', 'Science Fiction')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Ares Combat Bioroid')
adversary.update_attributes(
  brawn: 5,
  agility: 4,
  intellect: 3,
  cunning: 3,
  willpower: 1,
  presence: 1,
  adversary_type: 'Nemesis',
  soak: 10,
  wound_th: 25,
  strain_th: 15,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 15,
  social_rating: 3,
  general_rating: 4,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 4},
    {id: Skill.find_by_name('Coercion').id, rank: 5},
    {id: Skill.find_by_name('Coordination').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 3},
    {id: Skill.find_by_name('Gunnery').id, rank: 4},
    {id: Skill.find_by_name('Survival').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
    {id: Talent.find_by_name('Grenadier').id, rank: 0},
  ],
  abilities: [
    {name: 'Combat Bioroid', description: 'Does not need to breathe, eat, or drink; can survive in vacuum and underwater; is immune to poisons and toxins.'},
    {name: 'Enhanced Auto-Bracing', description: 'Using the Auto-fire quality does not increase the difficulty of combat checks; each time the Ares triggers and additional hit, it suffers 1 strain.'},
  ],
  weapons: [
    {
      name: 'Integrated Rotary Cannon',
      skill_id: Skill.find_by_name('Gunnery').id,
      damage: 10,
      critical: 3,
      range: 'Long',
      qualities: [
        {id: ItemQuality.find_by_name('Auto-Fire').id, rank: 0},
      ]
    },
    {
      name: 'Integrated Heavy Gauss Rifle',
      skill_id: Skill.find_by_name('Gunnery').id,
      damage: 10,
      critical: 2,
      range: 'Extreme',
      qualities: [
        {id: ItemQuality.find_by_name('Breach').id, rank: 1},
        {id: ItemQuality.find_by_name('Slow-Firing').id, rank: 1},
      ]
    },
    {
      name: 'Integrated Micro-missile Launcher',
      skill_id: Skill.find_by_name('Gunnery').id,
      damage: 12,
      critical: 4,
      range: 'Long',
      qualities: [
        {id: ItemQuality.find_by_name('Blast').id, rank: 10},
        {id: ItemQuality.find_by_name('Guided').id, rank: 3},
        {id: ItemQuality.find_by_name('Limited Ammo').id, rank: 3},
      ]
    },
    {
      name: 'Armored Fist',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 8,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Concussive').id, rank: 1},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
        {id: ItemQuality.find_by_name('Sunder').id, rank: 0},
      ]
    },
  ],
  equipment: "<p></p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Android', 'Science Fiction')
adversary.tag_list.add('Bioroid')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Assistant District Attorney')
adversary.update_attributes(
  brawn: 2,
  agility: 1,
  intellect: 3,
  cunning: 3,
  willpower: 2,
  presence: 3,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 10,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 1,
  social_rating: 4,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 2},
    {id: Skill.find_by_name('Coercion').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Deception').id, rank: 1},
    {id: Skill.find_by_name('Society').id, rank: 2},
    {id: Skill.find_by_name('Melee').id, rank: 2},
    {id: Skill.find_by_name('Negotiation').id, rank: 3},
    {id: Skill.find_by_name('Perception').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Natural').id, rank: 0},
  ],
  abilities: [
    {name: 'Full Weight of the Law', description: 'When making a Coercion check to inflict strain during a social encounter, inflict 2 additional strain.'},
  ],
  weapons: [
    {
      name: 'Concealed Stun Baton',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 4,
      critical: 5,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 3},
        {id: ItemQuality.find_by_name('Stun Damage').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>Legal PAD, expensive suit.</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Android', 'Science Fiction')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Bernice Mai')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 3,
  cunning: 3,
  willpower: 3,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 2,
  wound_th: 9,
  strain_th: 14,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 1,
  social_rating: 3,
  general_rating: 9,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 4},
    {id: Skill.find_by_name('Computers (Hacking)').id, rank: 2},
    {id: Skill.find_by_name('Computers (Sysops)').id, rank: 5},
    {id: Skill.find_by_name('Society').id, rank: 4},
    {id: Skill.find_by_name('The Net').id, rank: 4},
    {id: Skill.find_by_name('Perception').id, rank: 3},
    {id: Skill.find_by_name('Streetwise').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Nethunter').id, rank: 0},
    {id: Talent.find_by_name('Snare').id, rank: 0},
  ],
  abilities: [
    {name: 'All Seeing Eye', description: 'If this character has access to a server, increase the strength of all sentry ice on that server by 2.'},
    {name: 'Datasniffers', description: 'May spend [t][t][t] or [des] from any character\'s check to have this character learn their present location.'},
  ],
  weapons: [

  ],
  equipment: "<p>High-end encrypted rig with voice and haptic controls, business suit.</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Android', 'Science Fiction')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Camdrone')
adversary.update_attributes(
  brawn: 1,
  agility: 3,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 1,
  wound_th: 2,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 2,
  combat_rating: 1,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Perception').id, rank: 0},
    {id: Skill.find_by_name('Vigilance').id, rank: 0},
  ],
  talents: [
  ],
  abilities: [
    {name: 'Early Warning', description: 'Adds [b] to its operator\'s checks to determine Initiative.'},
    {name: 'Flyer', description: 'Can fly; see the Flying sidebar on page 100 of the Genesys Core Rulebook.'},
    {name: 'Mechanical', description: 'Does not need to breathe, eat, or drink, and can survive in vacuum and underwater; is immune to poisons and toxins.'},
    {name: 'Silhouette', description: '0'},
    {name: 'Telepresence', description: 'Can operate independently, or can be controlled directly by an operator via wireless link; if being controlled, the camdrone counts as having ranks in any skill equal to the controller\'s ranks in that skill.'},
  ],
  weapons: [
  ],
  equipment: "<p></p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Android', 'Science Fiction')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Caprice Nisei')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 4,
  cunning: 2,
  willpower: 5,
  presence: 4,
  adversary_type: 'Nemesis',
  soak: 2,
  wound_th: 14,
  strain_th: 18,
  defense_melee: 5,
  defense_ranged: 5,
  combat_rating: 6,
  social_rating: 8,
  general_rating: 5,
  skills: [
    {id: Skill.find_by_name('Computers (Sysops)').id, rank: 2},
    {id: Skill.find_by_name('Deception').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 4},
    {id: Skill.find_by_name('Science').id, rank: 2},
    {id: Skill.find_by_name('Society').id, rank: 2 },
    {id: Skill.find_by_name('Perception').id, rank: 2},
    {id: Skill.find_by_name('Ranged (Light)').id, rank: 1},
    {id: Skill.find_by_name('Streetwise').id, rank: 1},
    {id: Skill.find_by_name('Vigilance').id, rank: 4},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
    {id: Talent.find_by_name('Probing Question').id, rank: 0},
  ],
  abilities: [
    {name: 'Apparent Precognition', description: "Once per encounter when making a combat check, may use Discipline and Willpower instead of the normal skill and characteristic."},
    {name: 'Psychic', description: "As a maneuver may make and Average ([d][d]) Vigilance check to learn the surface thoughts of all other characters in medium range."},
    {name: 'Nisei Line', description: "As an action, may make a Hard ([d][d][d]) Discipline check targeting one character within medium range. If successful, this character learns the target's Strength, Flaw, Fear, and Desire motivations and all current goals (in addition to other information at the GM's discretion)."},
  ],
  weapons: [
    {
      name: 'Synap Pistol',
      skill_id: Skill.find_by_name('Ranged (Light)').id,
      damage: 5,
      critical: 6,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 4},
        {id: ItemQuality.find_by_name('Stun').id, rank: 3},
        {id: ItemQuality.find_by_name('Stun Damage').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>Trenchcoat, NAPD badge, restraints.</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Android', 'Science Fiction')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Civic Brigadier')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 2,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 4,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 0},
    {id: Skill.find_by_name('Driving').id, rank: 0},
    {id: Skill.find_by_name('Society').id, rank: 0},
    {id: Skill.find_by_name('Mechanics').id, rank: 0},
    {id: Skill.find_by_name('Melee').id, rank: 0},
  ],
  talents: [
  ],
  abilities: [
    {name: 'Community Volunteer', description: 'May use Knowledge (Society) instead of Charm, Coercion, Or negotiation during social interactions with undercity residents.'},
  ],
  weapons: [
    {
      name: 'Titanium Shovel',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 5,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Stun').id, rank: 2},
      ]
    },
  ],
  equipment: "<p>Hard Hat: +1 melee defense, PAD, toolkit</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Android', 'Science Fiction')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Club Owner')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 3,
  cunning: 3,
  willpower: 2,
  presence: 5,
  adversary_type: 'Nemesis',
  soak: 2,
  wound_th: 12,
  strain_th: 16,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 7,
  general_rating: 4,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 3},
    {id: Skill.find_by_name('Deception').id, rank: 2},
    {id: Skill.find_by_name('Society').id, rank: 2},
    {id: Skill.find_by_name('Perception').id, rank: 3},
    {id: Skill.find_by_name('Streetwise').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Clever Retort').id, rank: 0},
    {id: Talent.find_by_name('Customer Service Experience').id, rank: 3},
  ],
  abilities: [
    {name: 'Threat of Ban', description: "While in club owner's establishment, may spend [t] from checks targeting club owner to have character making the check suffer 2 strain instead of 1."},
  ],
  weapons: [
    {
      name: 'Stun Baton',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 4,
      critical: 5,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 3},
        {id: ItemQuality.find_by_name('Stun Damage').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>PAD, bouncers.</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Android', 'Science Fiction')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"