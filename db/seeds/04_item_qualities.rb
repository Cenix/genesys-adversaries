puts '##########################'
puts '# Seeding Item Qualities.#'
puts '##########################'

quality = ItemQuality.find_or_create_by(name: 'Accurate')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>Accurate weapons are easier to aim or wield, whether through design or technology. For each level of this quality, the attacker adds [boost] to their combat checks while using this weapon.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Auto-Fire')
quality.update_attributes(ranked: false, activation: 'Active', description: "<p>A weapon with Auto-fire can be set to shoot in rapid succession and potentially spray an area with bolts, flechettes, slugs, or other types of projectiles. The advantage in using Auto-fire is that it has the chance to hit multiple targets or to hit a single target multiple times.</p>
  <p>As attacking with a weapon on Auto-fire is generally less accurate, the attacker must increase the difficulty of the combat check by [difficulty]. The user may choose not to use the Auto-fire quality on a weapon; in this case, they cannot trigger the quality but also do not suffer the aforementioned penalty.</p>
  <p>If the attack hits, the attacker can trigger Auto-fire by spending [advantage] [advantage]. Auto-fire can be triggered multiple times. Each time the attacker triggers Auto-fire, it deals an additional hit to the target. Each of these counts as an additional hit from that weapon, and each hit deals base damage plus the number of [success] on the check.</p>
  <p>These additional hits can be allocated to the original target, or to other targets within range of the weapon. If the attacker wishes to hit multiple targets, they must decide to do so before making the check. Furthermore, if they wish to hit multiple targets, their initial target must always be the target with the highest difficulty and highest defense (if this is two separate targets, the GM chooses which is the initial target). The initial hit must always be against the initial target. Subsequent hits generated can be allocated to any of the other designated targets.</p>
  <p>Auto-fire weapons can also activate one Critical Injury for each hit generated on the attack, per the normal rules; the Critical Injury must be applied to the target of the specific hit.<p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Blast')
quality.update_attributes(ranked: true, activation: 'Active', description: "<p>The weapon has a large spread, an explosive blast, or a similar area of effect, like a detonated grenade or a warhead fired from a missile launcher. If the attack is successful and Blast activates, each character (friend or foe) engaged with the original target suffers a hit dealing damage equal to the Blast quality’s rating, plus damage equal to the total [success] scored on the check.</p>
<p>In a relatively small and enclosed area, the Game Master might decide that everyone in the room suffers damage.</p>
<p>If the Blast quality doesn’t activate, the ordnance still detonates, but bad luck or poor aim on the part of the firer (or quick reactions on the part of the targets) means the explosion may not catch anyone else in its radius. However, the user may also trigger Blast if the attack misses by spending [advantage] [advantage] [advantage]. In this case, the original target and every target engaged with the original target suffers a hit dealing damage equal to the Blast rating of the weapon.<p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Breach')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>Weapons with Breach burn through the toughest armor; they are often heavy weapons or weapons mounted on some sort of vehicle.</p>
<p>Hits from weapons with the Breach quality ignore one point of vehicle armor for every rating of Breach (meaning they also ignore 10 soak for every rating of Breach).</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Burn')
quality.update_attributes(ranked: true, activation: 'Active', description: "<p>Weapons with Burn inflict damage over time. When Burn is triggered, one target hit by the attack continues to suffer the weapon’s base damage each round for a number of rounds equal to the weapon’s Burn rating. Apply damage at the start of each of the target’s turns. If multiple targets suffer hits from a weapon with Burn, the quality may be triggered multiple times, affecting a different target each time.</p>
<p>A victim might be able to stop the damage by performing an action to roll around and make a Coordination check. The difficulty is <strong>Average ([difficulty] [difficulty])</strong> on hard surfaces such as the floor of a building, or an <strong>Easy ([difficulty])</strong> on grass or soft ground. Jumping into a body of water stops the damage immediately. Both situations assume the flame is from actual combustion rather than a chemical reaction. With the latter, there is usually little the victim can do.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Concussive')
quality.update_attributes(ranked: true, activation: 'Active', description: "<p>The weapon’s attack can leave the target shell-shocked from mighty blows or punishing shock waves, unable to perform any but the most basic actions. When Concussive is triggered, one target hit by the attack is staggered for a number of rounds equal to the weapon’s Concussive rating. A staggered target cannot perform actions. If multiple targets suffer hits from a weapon with Concussive, the quality may be triggered multiple times, affecting a different target each time.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Cumbersome')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>A Cumbersome weapon is large, unwieldy, awkward, or heavy. To wield a Cumbersome weapon properly, the character needs a Brawn characteristic equal to or greater than the weapon’s Cumbersome rating. For each point of Brawn by which the character is deficient, they must increase the difficulty of all checks made while using the weapon by one.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Defensive')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>An item with the Defensive quality increases the user’s melee defense by its Defensive rating.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Deflection')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>An item with the Deflection quality increases the user’s ranged defense by its Deflection rating.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Disorient')
quality.update_attributes(ranked: true, activation: 'Active', description: "<p>A weapon with Disorient can daze an opponent. When Disorient is triggered, one target hit by the attack is disoriented for a number of rounds equal to the weapon’s Disorient rating. A disoriented target adds [setback] to all skill checks they perform. If multiple targets suffer hits from a weapon with Disorient, the quality may be triggered multiple times, affecting a different target each time.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Ensnare')
quality.update_attributes(ranked: true, activation: 'Active', description: "<p>A weapon with Ensnare binds a foe and restricts their movements. When Ensnare is triggered, one target hit by the attack becomes immobilized for a number of rounds equal to the weapon’s Ensnare rating. An immobilized target cannot perform maneuvers. If multiple targets suffer hits from a weapon with Ensnare, the quality may be triggered multiple times, affecting a different target each time.</p>
<p>An Ensnared target may perform an action to attempt a Hard ([difficulty] [difficulty] [difficulty]) Athletics check on their turn to break free from the effect.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Guided')
quality.update_attributes(ranked: true, activation: 'Active', description: "<p>A weapon with the Guided quality can track opponents. The Guided quality can only be triggered if an attack misses. If Guided is triggered, the controlling character may make a combat check at the end of the round as an out-of-turn incidental. The difficulty of this combat check is <strong>Average ([difficulty] [difficulty])</strong>; instead of building the ability of the pool normally, add [ability] equal to the weapon’s Guided rating. If the check is successful, the weapon strikes the target, and the attack is resolved normally.</p>
<p>Guided requires [advantage] [advantage] [advantage] to activate, unless otherwise specified in the weapon’s description. The Guided quality can activate on any subsequent combat check it makes, representing the projectile continuing to track the target.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Inaccurate')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>Inaccurate weapons are less likely to be accurate or precise. When making an attack with an Inaccurate weapon, add [setback] to the check equal to the Inaccurate rating.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Inferior')
quality.update_attributes(ranked: false, activation: 'Passive', description: "<p>An Inferior item is a lackluster example of its kind, representing shoddy and poor craftsmanship. An Inferior item generates automatic [threat] on all checks related to its use.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Knockdown')
quality.update_attributes(ranked: false, activation: 'Active', description: "<p>When Knockdown is triggered, one target hit by the attack is knocked prone. If multiple targets suffer hits from a weapon with Knockdown, the quality may be triggered multiple times, affecting a different target each time.</p>
<p>Unless specified otherwise, Knockdown requires [advantage] [advantage] to trigger, plus one additional [advantage] per silhouette of the target beyond 1.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Limited Ammo')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>Some weapons fire particularly large or complex projectiles that cost lots of money. Other weapons are expendable weapons like grenades that, once used, are destroyed. A weapon with the Limited Ammo quality may be used to make a number of attacks equal to its Limited Ammo rating before it must be reloaded with a maneuver. In addition, each shot expends one of a limited number of rounds of ammo; more ammo must be purchased or obtained before anyone fires the weapon again. This also applies to grenades and other “one-use” weapons that have the Limited Ammo 1 quality (here, your character is not “reloading” the grenade, but drawing another to use—mechanically, they are equivalent).</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Linked')
quality.update_attributes(ranked: true, activation: 'Active', description: "<p>Some weapons are designed to fire together at the same target (turrets housing multiple guns are a good example of this). When a character fires a linked weapon, on a successful attack, the weapon deals one hit. The wielder may spend [advantage] [advantage] to gain an additional hit, and may do so a number of times equal to the weapon’s Linked rating. Additional hits from the Linked weapon may only be applied against the original target. Each hit deals the weapon’s base damage plus damage equal to the [success] scored on the check.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Pierce')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>Any hits from this weapon ignore a number of points point of soak equal to the weapon’s Pierce rating. If the weapon has more ranks of Pierce than the target’s total soak, it completely ignores the target’s soak. For example, Pierce 3 against a soak of 2 ignores two points of soak, but the extra point of Pierce has no further effect.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Prepare')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>Items with this quality require time to set up before being used. The user must perform a number of preparation maneuvers equal to the item’s Prepare rating before using the item (if the item is a weapon, “using” it would be making attacks with the weapon). At your GM’s discretion, moving with the item, being knocked prone with the item, or other disruptions may require the user to perform the preparation maneuvers again before using the item.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Reinforced')
quality.update_attributes(ranked: false, activation: 'Passive', description: "<p>Weapons or items with the Reinforced quality are immune to the Sunder quality. Armor with the Reinforced quality make the wearer’s soak immune to the Pierce and Breach qualities.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Slow-Firing')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>Slow-Firing weapons tend to deal incredible damage, but need time to recharge or cool down between shots. A weapon’s Slow-Firing rating dictates the number of rounds that must pass before the weapon can be fired again after attacking. For example, a heavy laser cannon with Slow-Firing 2 must wait two rounds after being fired before it can be fired again.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Stun')
quality.update_attributes(ranked: true, activation: 'Active', description: "<p>A weapon with Stun can deal strain to the target. When the Stun quality is activated, it inflicts strain equal to the weapon’s Stun rating. Since this is strain, and not strain damage, it is not reduced by the target’s soak.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Stun Damage')
quality.update_attributes(ranked: false, activation: 'Passive', description: "<p>A weapon with this quality can only deal strain damage (damage applied to the target’s strain threshold). Because this is strain damage, not strain, it is still reduced by a target’s soak.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Sunder')
quality.update_attributes(ranked: false, activation: 'Active', description: "<p>When activating Sunder, the attacker chooses one item openly wielded by the target (such as a weapon, shield, or item on a belt). That item is damaged one step: to minor if undamaged, from minor to moderate, or from moderate to major. If an item already suffering major damage is the target of a successful Sunder, it is destroyed.</p>
<p>Sunder requires [advantage] to activate, and may be activated even if the attack is unsuccessful. Sunder may be activated multiple times in the same attack, but each activation must be applied to the same item, potentially taking it from undamaged to destroyed in a single attack.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Superior')
quality.update_attributes(ranked: false, activation: 'Passive', description: "<p>A Superior item is a sterling example of its kind, representing masterful craftsmanship. A Superior item generates automatic [advantage] on all checks related to its use.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Tractor')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>Instead of firing searing beams of laser fire or crackling ion discharges, this weapon fires relatively harmless electromagnetic beams that ensnare ships and hold them fast in space. Tractor beams, like all weapons, are fired at their target using the appropriate skill check with all suitable modifiers. Once the weapon hits its target, the target may not move unless its pilot makes a successful Piloting check with a difficulty equal to the tractor beam’s rating. If the target is an individual character, the character is immobilized while the beam is active.</p>
<p>(Weapons with the Tractor quality are only going to show up in sci-fi settings, and probably only the more fanciful sci-fi settings! However, they are such a staple in those settings that we included the quality here.)</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Unwieldy')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>An Unwieldy weapon is a weapon that can be particularly awkward to use for those without impressive dexterity and hand-eye coordination. To wield an Unwieldy weapon properly, the character needs an Agility characteristic equal to or greater than the weapon’s Unwieldy rating. For each point of Agility by which the character is deficient, they must increase the difficulty of all checks made while using the weapon by one.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Vicious')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>When an attack with this weapon results in a Critical Injury or Hit, the character adds ten times the Vicious rating to the Critical roll. With Vicious 3, for example, you would add +30 to the resulting Critical Injury or Hit result.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Noisy')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>Noisy armor like plate and scale armor makes it harder for the wearer to walk unnoticed. The character adds [setback] per rank of Noisy to all Stealth checks while wearing this armor.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Expand Encumbrance')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>This item increases the character's encumbrance threshold when equipped.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Expand Mount Encumbrance')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>This item increases the encumbrance threshold of the character's mount when equipped.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Encumbrance Equipped')
quality.update_attributes(ranked: true, activation: 'Passive', description: "<p>When this item is equipped, it's encumbrance is adjusted to ranks in Encumbrance Equipped.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Equippable')
quality.update_attributes(ranked: false, activation: 'Passive', description: "<p>This piece of equipment can be equipped on / worn by a character. Used when different rules apply for items either being worn or placed in i.e. a backpack. Equippable items can not be stacked.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Magic')
quality.update_attributes(ranked: false, activation: 'Passive', description: "<p>A magic item is one of those rare objects that possess magical properties, whether as the result of a runebound shard worked into the item, a runic inscription, or some other form of enchantment. Some magic items are the intentional creation of a wizard or other crafter, while others might have developed their power through exposure to arcane energies, proximity to great events, or even stranger ways. No matter their origins, magic items—even those that produce similar effects—are each a unique wonder of Mennara. Magic items are almost never offered for sale and cannot be crafted.</p>")
puts "Quality: #{quality.name}"

quality = ItemQuality.find_or_create_by(name: 'Poison')
quality.update_attributes(ranked: false, activation: 'Passive', description: "<p>a character wounded by this weapon must make a <strong>Hard ([difficulty][difficulty][difficulty])</strong> Resilience check or suffer 3 additional wounds plus 1 strain per [threat], and must check again on their next turn if the check generates [despair]</p>")
puts "Quality: #{quality.name}"
