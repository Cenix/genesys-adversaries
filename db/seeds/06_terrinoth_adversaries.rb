puts '#########################'
puts '# Terrinoth Adversaries.#'
puts '#########################'

adversary = Adversary.find_or_create_by(name: 'Beast of burden')
adversary.update_attributes(
  brawn: 4,
  agility: 2,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 4,
  wound_th: 7,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 0,
  social_rating: 0,
  general_rating: 0,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 0},
    {id: Skill.find_by_name('Resilience').id, rank: 0},
  ],
  abilities: [
    {
      name: 'Encumbrance Capacity',
      description: '18'
    },
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
  ],
  equipment: "<p>Harness.</p>",
  description: "<p>Mules, oxen, draft horses, and other strong, hearty animals are put to many working uses, such as pulling wagons and carts.</p>",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Animal')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Flying mount')
adversary.update_attributes(
  brawn: 3,
  agility: 4,
  intellect: 1,
  cunning: 2,
  willpower: 2,
  presence: 2,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 2,
  combat_rating: 1,
  social_rating: 0,
  general_rating: 0,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 3},
    {id: Skill.find_by_name('Coordination').id, rank: 3},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
    {id: Skill.find_by_name('Survival').id, rank: 2},
  ],
  talents: [{id: Talent.find_by_name('Dodge').id, rank: 2}],
  abilities: [
    {name: 'Encumbrance Capacity', description: '12'},
    {name: 'Flyer', description: 'Can fly; see page 100 of Genesys CRB'},
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
  ],
  weapons: [
    {
      name: 'Hooves or talons',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 4,
      range: 'Engaged',
      qualities: [{id: ItemQuality.find_by_name('Knockdown').id, rank: 0}]
    },
  ],
  description: "<p>Throughout history, a number of flying creatures have been bred and trained as mounts, including the rocs of Baron Hadrian and the Yeron of the Latari Elves. Such animals are always rarer than more typical riding beasts, and those trained for war are rarer still. Due to the obvious dangers involved, only the most skilled and courageous riders seek out flying mounts.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Animal', 'Mount', 'Flying')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Riding beast')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 4,
  wound_th: 5,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 1,
  social_rating: 0,
  general_rating: 0,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 0},
    {id: Skill.find_by_name('Resilience').id, rank: 0},
  ],
  abilities: [
    {
      name: 'Encumbrance Capacity',
      description: '12'
    },
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
  ],
  equipment: "<p>Riding tack.</p>",
  description: "<p>Although they can carry riders over great distances, most horses, ponies, and other such steeds are neither bred nor trained for the violence of battlefield conditions. A Riding check is required to maintain control of a riding beast in combat or a similarly stressful situation.</p>",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Animal', 'Mount')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'War mount')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 3,
  presence: 1,
  adversary_type: 'Rival',
  soak: 4,
  wound_th: 14,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 1,
  social_rating: 0,
  general_rating: 0,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 3},
    {id: Skill.find_by_name('Brawl').id, rank: 1},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Resilience').id, rank: 3},
    {id: Skill.find_by_name('Survival').id, rank: 2},
  ],
  abilities: [
    {
      name: 'Encumbrance Capacity',
      description: '13'
    },
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
  ],
  weapons: [{
      name: 'Hooves or talons',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 4,
      range: 'Engaged',
      qualities: [{id: ItemQuality.find_by_name('Knockdown').id, rank: 0}]
    }],
  equipment: "<p>Riding tack.</p>",
  description: "<p>War mounts are those steeds bred and trained for war, such as the destriers of the Daqan Baronies. Just like their riders, such mounts do not shy away when facing combat and can use their stomping feet, grasping talons, or other natural weapons to aid in battle.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Animal', 'Mount')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Goblin Witcher')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 2,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 16,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 4,
  social_rating: 2,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Deception').id, rank: 1},
    {id: Skill.find_by_name('Arcana').id, rank: 3},
    {id: Skill.find_by_name('Forbidden').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 1},
  ],
  talents: [{id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Dark Insight').id, rank: 0}],
  abilities: [
    {
      name: 'Dark Vision',
      description: 'When making skill checks, goblins remove up to [s][s] imposed due to darkness.'
    },
    {
      name: 'Spiteful Curse',
      description: 'Add [b][b] to magic actions that target a character who has previously inflicted wounds on the goblin witcher during this encounter.'
    },
  ],
  weapons: [
    {
      name: 'Pointy teeth',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 3,
      critical: 3,
      range: 'Engaged',
      qualities: [{id: ItemQuality.find_by_name('Vicious').id, rank: 1}]
    },
  ],
  equipment: "<p>Bone staff (+4 damage to magic attacks; the first Range effect added does not increase difficulty; when used to cast a spell that inflicts 1 or more wounds, caster heals 1 wound), robed bone armor (+1 soak, +1 defense).</p>",
  spells: [
    {name: 'Agonizing Hex', description: 'Choose a target at short or medium range for this attack and make an <b>Average ([d][d]) Arcana check</b>. If successful, this magic attack inflicts 6 damage +1 damage per [su], with the Disorient 2 and Knockdown qualities.'},
    {name: 'Bad Luck', description: 'Choose a target at short or medium range and make a <b>Hard ([d][d][d]) Arcana check</b>. If successful, the target decreases the ability of any skill checks they make by 1 until the end of the goblin witcher’s next turn, and when the target makes a check, may change one [s] to a face displaying [f]. The goblin witcher may maintain these effects by performing the concentrate maneuver.'},
  ],
  description: "<p>Few would consider goblins intelligent enough to utilize magic. However, there are indeed some of exceptional cleverness who have managed to not only discover their sorcerous abilities, but also harness them to a degree. Adventurers who come across witchers often take them for mere goblin leaders, but soon learn better when bolts of energy are hurled their way. Like all goblins, witchers are cowardly and avaricious, but with their access to dark magics they are able to brutally slay foes from which most of their kin would flee in terror.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/4/4c/Goblin_Witcher.png'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Goblin', 'Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Baronial Knight')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 2,
  cunning: 2,
  willpower: 3,
  presence: 3,
  adversary_type: 'Rival',
  soak: 5,
  wound_th: 16,
  strain_th: nil,
  defense_melee: 4,
  defense_ranged: 3,
  combat_rating: 6,
  social_rating: 3,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Leadership').id, rank: 1},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
    {id: Skill.find_by_name('Riding').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  weapons: [
    {
      name: 'Sword',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 2,
      range: 'Engaged',
      qualities: [{id: ItemQuality.find_by_name('Defensive').id, rank: 1}]
    },
    {
      name: 'Lance',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 9,
      critical: 3,
      range: 'Engaged',
      qualities: [{id: ItemQuality.find_by_name('Knockdown').id, rank: 0}]
    },
    {
      name: 'Large Shield',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 4,
      critical: 5,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 2},
        {id: ItemQuality.find_by_name('Deflection').id, rank: 2},
        {id: ItemQuality.find_by_name('Inaccurate').id, rank: 2},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0}
      ]
    },
  ],
  equipment: "<p>Lance: A lance can only be used while mounted and can only be used to attack immediately after performing a maneuver to engage the target. Plate armor (+2 soak, +1 defense). War mount.</p>",
  description: "<p>It is the dream of many a soldier to be elevated to the position of knight. Not only does it bring honor and pride to their family name, but having a baron’s favor and a position in their retinue is most often the key to advancement later in life. Many seek further glory in a knightly order, such as the Knights of the Greatwood, or the legendary Citadel Guard. Tougher than the average swordsman, baronial knights are usually armed with either a long spear or a sword and a shield to ward off blows.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/3/3f/Oathsworn_Cavalry.PNG/revision/latest?cb=20181211025255'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Human')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Dragon Hybrid')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 3,
  presence: 2,
  adversary_type: 'Rival',
  soak: 6,
  wound_th: 15,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 0,
  combat_rating: 7,
  social_rating: 2,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Coercion').id, rank: 2},
    {id: Skill.find_by_name('Melee (Heavy)').id, rank: 1},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
    {id: Skill.find_by_name('Ranged').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 1},
  ],
  talents: [{id: Talent.find_by_name('Adversary').id, rank: 1}],
  abilities: [{
    name: 'Draconic Heritage',
    description: 'Dragon hybrids reduce the damage they take from fire and similar sources by 3.'
  },{
    name: 'Flyer',
    description: 'Can fly; see page 100 of the Genesys Core Rulebook.'
  }],
  weapons: [
    {
      name: 'Greatsword',
      skill_id: Skill.find_by_name('Melee (Heavy)').id,
      damage: 8,
      critical: 2,
      range: 'Engaged',
      qualities: [{id: ItemQuality.find_by_name('Defensive').id, rank: 1},{id: ItemQuality.find_by_name('Pierce').id, rank: 1},{id: ItemQuality.find_by_name('Unwieldy').id, rank: 3}]
    },
    {
      name: 'Fiery breath',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 6,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Blast').id, rank: 6},
        {id: ItemQuality.find_by_name('Burn').id, rank: 1},
        {id: ItemQuality.find_by_name('Prepare').id, rank: 1},
        {id: ItemQuality.find_by_name('Slow-Firing').id, rank: 2},
      ]
    },
    {
      name: 'Talons',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Scale armor (+2 soak).</p>",
  description: "<p>Dragon hybrids are cruel abominations, relics from the age of the Third Darkness. They are the result of depraved experiments of the most inventive draconic invaders, who required loyal servants with both the cunning of Humans and the resiliency and magic of dragon-kind.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/descent2e/images/b/b0/HybridSentinel.jpg'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Dragon')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Feral Dragon')
adversary.update_attributes(
  brawn: 5,
  agility: 4,
  intellect: 2,
  cunning: 4,
  willpower: 4,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 8,
  wound_th: 41,
  strain_th: 20,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 13,
  social_rating: 3,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 4},
    {id: Skill.find_by_name('Coercion').id, rank: 4},
    {id: Skill.find_by_name('Cool').id, rank: 3},
    {id: Skill.find_by_name('Discipline').id, rank: 4},
    {id: Skill.find_by_name('Ranged').id, rank: 4},
    {id: Skill.find_by_name('Resilience').id, rank: 4},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Flyer', description: 'Can fly; see page 100 of Genesys CRB'},
    {name: 'Silhouette 4', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Sweep Attack', description: 'A dragon may spend [ad] from a Brawl check to hit one additional engaged opponent that would be no more difficult to attack than the original target, dealing base damage +1 damage per [su].'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Daunting ([d][d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},
  ],
  weapons: [
    {
      name: 'Fiery breath',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 12,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Blast').id, rank: 12},
        {id: ItemQuality.find_by_name('Burn').id, rank: 3},
        {id: ItemQuality.find_by_name('Slow-Firing').id, rank: 2},
      ]
    },
    {
      name: 'Claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 11,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 3},
      ]
    },
  ],
  description: "<p>Dragons are perhaps most powerful beings in all of Mennara. No one knows where they come from or what their purpose is when they embark on their destructive flights. What is known is that all dragons possess an insatiable appetite for magical objects and can detect magic from hundreds of leagues away. During the Third Darkness, also known as the Dragon Wars, they plundered countless holdings for their magical artifacts, leaving their other treasures untouched or burning them to ash.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/0/03/Dragon_2.JPG'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Dragon')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Goblin')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 4,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Cool').id, rank: 0},
    {id: Skill.find_by_name('Deception').id, rank: 0},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
    {id: Skill.find_by_name('Ranged').id, rank: 0},
    {id: Skill.find_by_name('Stealth').id, rank: 0},
  ],
  abilities: [
    {name: 'Dark Vision', description: 'When making skill checks, goblins remove up to [s][s] imposed due to darkness.'},
    {name: 'Opportunistic', description: 'Goblins inflict 1 additional damage with successful melee attacks on prone or immobilized targets.'}
  ],
  weapons: [
    {
      name: 'Jagged blade',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: []
    },
    {
      name: 'Crude bow',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 6,
      critical: 4,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Unwieldy').id, rank: 2},
      ]
    },
    {
      name: 'Wooden buckler',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 2,
      critical: 6,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
        {id: ItemQuality.find_by_name('Inaccurate').id, rank: 2},
      ]
    },
    {
      name: 'Pointy teeth',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 3,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Grimy patchwork armor (+1 soak).</p>",
  description: "<p>Goblins are smelly, small, and decidedly cunning when they wish to be. On average, goblins in Terrinoth stand shorter than a Dwarf and are usually possessed of a cowardly personality, a valuable instinct in a place where death is usually quick and painful. Lacking any real sense of courage or tactics, goblins usually attack in packs, raiding and stealing what they can before being chased off by the local guard or an angry farmer with a pitchfork. Though it seems they should be easy to exterminate, goblins are quite difficult to completely root out from their caves and homes. Breeding quickly in the dark, they can rebuild their numbers in a matter of months.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/2/2f/Goblins_art.jpg/revision/latest?cb=20181112223720'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Greyhaven Wizard')
adversary.update_attributes(
  brawn: 1,
  agility: 2,
  intellect: 4,
  cunning: 2,
  willpower: 3,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 1,
  wound_th: 12,
  strain_th: 18,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 6,
  social_rating: 3,
  general_rating: 5,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Lore').id, rank: 4},
    {id: Skill.find_by_name('Runes').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  equipment: "<p>Magic staff (add +4 damage to magic attacks; the first Range effect added does not increase difficulty), heavy robes (+1 defense).</p>",
  spells: [
    {name: 'Fireball', description: 'Choose one target at short or medium range and make a <b>Hard ([d][d][d]) Arcana check</b>; if successful, this magic attack inflicts 8 damage +1 damage per [su], with the Blast 4 and Burn 4 qualities.'},
    {name: 'Magic Shield', description: 'Make a <b>Hard ([d][d][d]) Arcana check</b>; if successful, until the end of the wizard’s next turn, reduce the damage of all hits against them by one, plus one for every [su][su], and the wizard gains +3 defense. The wizard can maintain these effects with the concentrate maneuver.'},
  ],
  description: "<p>To the outside world, the wizards of Greyhaven are a reclusive bunch, hiding away in their towers to augur the arcane truths of rune magic away from prying eyes. To those who have fought beside them, they are some of the mightiest allies one could ask for.</p>",
  image_url: 'https://miniaturegamingguide.com/wp-content/uploads/2018/06/gns05_preview1.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Ironbound')
adversary.update_attributes(
  brawn: 4,
  agility: 2,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Rival',
  soak: 6,
  wound_th: 15,
  strain_th: nil,
  defense_melee: 3,
  defense_ranged: 2,
  combat_rating: 7,
  social_rating: 2,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Discipline').id, rank: 3},
    {id: Skill.find_by_name('Melee (Heavy)').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  abilities: [
    {name: 'Graven Wards', description: 'Increase the difficulty of all spells that target an ironbound twice.'},
    {name: 'Strength of Iron', description: 'An ironbound can wield a Melee (Heavy) weapon in one hand.'},
    {name: 'Watchful', description: 'Ironbound add [b][b] to Perception and Vigilance checks to detect the use or effects of Magic skills.'},
  ],
  weapons: [
    {
      name: 'Halberd',
      skill_id: Skill.find_by_name('Melee (Heavy)').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 3},
      ]
    },
    {
      name: 'Large Shield',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 5,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 2},
        {id: ItemQuality.find_by_name('Deflection').id, rank: 2},
        {id: ItemQuality.find_by_name('Inaccurate').id, rank: 2},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  description: "<p>The ironbound are the unfeeling, ever-watching eyes of the sorcerers of Nerekhall. Mechanical soldiers originally created to police against necromantic and other dark acts, they have far surpassed the expectations of their creators, and they now form the majority of Nerekhall’s military forces.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/7/70/Nerekhall_Ironbound_Guard.jpg/revision/latest?cb=20160111001825'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Construct')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Priest of Kellos')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 2,
  cunning: 2,
  willpower: 3,
  presence: 3,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 16,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 3,
  social_rating: 2,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Divine').id, rank: 2},
    {id: Skill.find_by_name('Lore').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 1},
  ],
  talents: [
    {id: Talent.find_by_name('Flames of Kellos').id, rank: 0},
  ],
  weapons: [
    {
      name: 'Mace',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 4,
      range: 'Engaged',
      qualities: []
    },
  ],
  equipment: "<p>Heavy robes (+1 defense).</p>",
  spells: [
    {name: 'Healing Spark', description: 'Choose a target within short range and make an <b>Average ([d][d]) Divine check</b>. If the check succeeds, the target heals 1 wound per [su] and 1 strain per [ad].'},
    {name: 'Righteous Blaze', description: 'Choose one target within short range and make an <b>Average ([d][d]) Divine check</b>. If successful, this magic attack inflicts 3 damage +1 damage per uncanceled [su], with the Burn 3 quality. Against an undead target, the attack inflicts +2 damage per [su] instead.'},
  ],
  description: "<p>Kellos is the Red-Handed God, a fiery deity whose presence is anathema to anything tainted by the foul magic of undeath or other dark sorceries. Though there is much debate on whether Kellos was just a hero or truly a god, none will deny that his priests are worthy allies in the fight to keep Terrinoth safe from the terrors that lurk in the dark.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/b/ba/Brother_Gherinn.jpg/revision/latest?cb=20160113052805'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Rune Golem')
adversary.update_attributes(
  brawn: 5,
  agility: 3,
  intellect: 2,
  cunning: 2,
  willpower: 3,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 7,
  wound_th: 25,
  strain_th: 22,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 11,
  social_rating: 2,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  abilities: [
    {name: 'Four-armed', description: 'A rune golem may make a combined combat check with its massive swords and stone fists without increasing the difficulty.'},
    {name: 'Elemental Runes', description: 'Each rune golem takes on different traits depending on its animating rune. Select one rune for each individual golem. Fire: add [su][su] to checks the rune golem makes to determine initiative. Ice: upgrade the difficulty of social skill checks targeting the golem once. Lightning: the rune golem may perform a second maneuver without suffering strain. Stone: the rune golem\'s soak increases to 8) other runes can be used at the GM\'s discretion'},
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
  ],
  weapons: [
    {
      name: 'Massive swords',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 9,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Linked').id, rank: 1},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 1},
      ]
    },
    {
      name: 'Stone fists',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Concussive').id, rank: 1},
        {id: ItemQuality.find_by_name('Disorient').id, rank: 3},
        {id: ItemQuality.find_by_name('Linked').id, rank: 1},
      ]
    },
  ],
  description: "<p>Considered by many to be Terrinoth’s mightiest living weapons, the rune golems are hulking engines of destruction and the apex of runic magic and wizardry. They are impervious to all but the mightiest blows and highly resistant to the ravages of time.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/7/7d/Rune_Golem.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Splig, King of all Goblins')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 2,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 5,
  wound_th: 20,
  strain_th: 11,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 6,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Coercion').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Melee (Heavy)').id, rank: 3},
    {id: Skill.find_by_name('Leadership').id, rank: 2},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Whirlwind').id, rank: 0},
  ],
  abilities: [
    {name: 'King of the Goblins!', description: 'Upgrade the difficulty of combat checks targeting Splig once for each minion group of goblins within short range of him. If the check fails, one individual minion is defeated.'},
    {name: 'Lucky Escape', description: 'When Splig would be defeated due to exceeding his wound threshold or strain threshold, or for any reason, he may spend 1 Story Point to instead escape in a suitably fortunate and cowardly fashion. All other goblins in the encounter are immediately defeated.'},
  ],
  weapons: [
    {
      name: 'The Club of All Goblins',
      skill_id: Skill.find_by_name('Melee (Heavy)').id,
      damage: 8,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Inaccurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  equipment: "<p>Stolen patchwork armor (+1 soak).</p>",
  description: "<p>Splig, the self-styled “King of All Goblins,” is one such goblin. Taller than his minions by several heads, and several hundred pounds heavier, Splig embodies the two major virtues of goblin-kind: cowardice and avarice. Using his large size and brutal demeanor, he intimidates his goblin lackeys into running headlong into readied enemy weapons, and he has even been known to grab one to use as a living shield when an adventurer’s blade comes too close. Should things even hint at going badly, he quickly retreats—all the while calling for his minions to keep fighting to the end.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/a/a6/Splig.jpg/revision/latest?cb=20181106094921'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Goblins')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Tamalir Guildmaster')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 3,
  cunning: 3,
  willpower: 2,
  presence: 3,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 13,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 1,
  social_rating: 4,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Deception').id, rank: 2},
    {id: Skill.find_by_name('Negotiation').id, rank: 3},
  ],
  abilities: [
    {name: 'Certain You Want To Do This?', description: 'When targeting a Guildmaster with a check, a character suffers 1 strain.'},
  ],
  weapons: [
    {
      name: 'Dagger',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 4,
      critical: 3,
      range: 'Engaged',
      qualities: []
    },
  ],
  equipment: "<p>Fine clothing.</p>",
  description: "<p>Drawing the ire of any of Tamalir’s Guildmasters can make one’s life suddenly difficult, often without the source being apparent. All a traveler might know is that prices are raised unexpectedly, porters become unruly, or meals acquire an upsetting flavor. Bloodshed is bad for business, so nonviolent methods are the first course of action when Guildmasters wish to remove a troublesome opponent. This can be anything from a complete denial of a guild’s services to a decree of outright banishment beyond the city limits.</p>",
  image_url: 'https://i.pinimg.com/originals/13/8c/39/138c39f8d56047daf6d387cc7a91f086.png'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Barghest')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 3,
  presence: 1,
  adversary_type: 'Rival',
  soak: 5,
  wound_th: 13,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 4,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Perception').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Maul', description: 'Barghest adds [b][b] to Brawl checks against prone or immobilized targets.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all of its opponents must make a <b>Hard ([d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents only make one fear check against the most terrifying enemy.'},
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
  ],
  weapons: [
    {
      name: 'Claws and fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  description: "<p>Some necromancers specialize in commanding barghests, ghastly canines of immense size and strength, rather than raising simple Reanimates. The origins of these bestial creatures is the subject of dark myths, and most firmly hold that they have existed in Mennara long before Waiqar was born. All tales agree, though, that their vicious, undead form has no resemblance to any living creature, and that they appear more the work of some twisted mentality than any natural evolution.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/b/bb/Barghest.jpg/revision/latest?cb=20181030123628'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Undead', 'The Haunted City')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Death Knight')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 3,
  presence: 1,
  adversary_type: 'Nemesis',
  soak: 5,
  wound_th: 20,
  strain_th: 14,
  defense_melee: 1,
  defense_ranged: 0,
  combat_rating: 9,
  social_rating: 2,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 3},
    {id: Skill.find_by_name('Melee (Heavy)').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
    {id: Skill.find_by_name('Riding').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Parry').id, rank: 3},
    {id: Talent.find_by_name('Parry (Improved)').id, rank: 0},
  ],
  abilities: [
    {name: 'Mounted Charge', description: 'a mounted death knight adds [b][b] to its first melee attack after performing a maneuver to engage the target in the same turn.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all of its opponents must make a <b>Hard ([d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents only make one fear check against the most terrifying enemy.'},
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
  ],
  weapons: [
    {
      name: 'Sword',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 8,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
      ]
    },
    {
      name: 'Flail',
      skill_id: Skill.find_by_name('Melee (Heavy)').id,
      damage: 9,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Cumbersome').id, rank: 3},
        {id: ItemQuality.find_by_name('Linked').id, rank: 1},
        {id: ItemQuality.find_by_name('Unwieldy').id, rank: 3},
      ]
    },
  ],
  equipment: "<p>Battered armor (+2 soak, +1 defense), war mount.</p>",
  description: "<p>Leagues above mere Reanimates, death knights are the very hammer of Waiqar, his hatred and will to dominate all life made manifest. Made from the dust of the most infamous and loyal of his original soldiers, they possess a degree of autonomy unknown in lesser undead, even retaining and developing personalities to a degree.</p>",
  image_url: 'https://miniaturegamingguide.com/wp-content/uploads/2018/06/g18r2_preview1.png'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Undead')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Ferrox')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 13,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 3,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Coordination').id, rank: 1},
    {id: Skill.find_by_name('Resilience').id, rank: 1},
  ],
  abilities: [
    {name: 'Bloodthirst', description: 'When a ferrox inflicts wounds with its fangs, it heals an equal number of wounds.'},
    {name: 'Glider', description: 'Ferrox can fly, but cannot increase their altitude while doing so; see page 100 of the Genesys Core Rulebook.'},
    {name: 'Savage', description: 'A ferrox increases the damage of its attacks by 2 when targeting an immobilized foe.'},
  ],
  weapons: [
    {
      name: 'Hooked claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 1},
      ]
    },
    {
      name: 'Fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  description: "<p>Ferrox are humanoid creatures with pale green skin over powerful muscles. Membranous skin stretched between their arms and torsos allows ferrox to glide, and gives a very bat-like appearance when extended. Their arms terminate in wicked, hook-like claws, and their mouths are filled with razor-like teeth. Vicious and savage, these creatures are only dimly intelligent, and easily dominated by certain dark magics.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/f/fe/Ferrox.jpg'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Lord of Bilehall')
adversary.update_attributes(
  brawn: 4,
  agility: 5,
  intellect: 4,
  cunning: 4,
  willpower: 4,
  presence: 4,
  adversary_type: 'Nemesis',
  soak: 6,
  wound_th: 18,
  strain_th: 20,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 11,
  social_rating: 5,
  general_rating: 7,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 3},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Charm').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 3},
    {id: Skill.find_by_name('Discipline').id, rank: 3},
    {id: Skill.find_by_name('Forbidden').id, rank: 5},
    {id: Skill.find_by_name('Negotiation').id, rank: 3},
    {id: Skill.find_by_name('Ranged').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
    {id: Talent.find_by_name('Dark Insight').id, rank: 0},
  ],
  abilities: [
    {name: 'Blood Call', description: 'When a Lord of Bilehall damages a target using their fangs or a magic attack, they heal wounds equal to the wounds inflicted.'},
    {name: 'Sunlight Sensitivity', description: 'While exposed to sunlight, a Lord of Bilehall reduces all their characteristics by 2 and halves their Wound Threshold and Strain Threshold.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all of its opponents must make a <b>Hard ([d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents only make one fear check against the most terrifying enemy.'},
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
    {name: 'Vampiric Magic', description: 'A Lord of Bilehall reduces the difficulty of all magic skill checks one step.'},
  ],
  weapons: [
    {
      name: 'Bloodstained fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 1},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  equipment: "<p>Magic staff (add +4 damage to magic attacks; the first Range effect added does not increase difficulty).</p>",
  spells: [
    {name: 'Blood Funnel', description: 'Choose one target at short or medium range for this attack and make a <b>Hard ([d][d][d]) Arcana check</b>. If successful, this magic attack inflicts 8 damage + 1 additional damage per uncanceled [su], with Critical Rating 2 and the Blast 5 and Vicious 5 qualities.'},
    {name: 'Curse of the Night', description: 'Choose one target within short range and make a <b>Hard ([d][d][d]) Arcana check</b>. If successful, the target decreases the ability of any skill checks they make by one and reduces their strain and wound thresholds by 4 until the end of the Lord of Bilehall’s next turn. The Lord of Bilehall may maintain these effects by performing the concentrate maneuver'},
  ],
  description: "<p>When the Lords of Bilehall discarded their humanity to join Waiqar, they tied their fates to that of his. They became immensely more powerful as the corrupting mists covered their lands and they rose in undeath. Today, they are an arrogant line, besotted from centuries of indulging in their every desire, exulting in the pleasures of immortality. Each has become a ruler in their own right, and even the lowest vampire of the line of Bilehall can claim a crypt-fortress as their own.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/a/a1/VampireRunewars.png'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Vampire', 'Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Necromancer')
adversary.update_attributes(
  brawn: 1,
  agility: 2,
  intellect: 3,
  cunning: 2,
  willpower: 3,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 1,
  wound_th: 12,
  strain_th: 18,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 6,
  social_rating: 3,
  general_rating: 6,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Forbidden').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 1},
    {id: Skill.find_by_name('Vigilance').id, rank: 1},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Chill of Nordros').id, rank: 0},
    {id: Talent.find_by_name('Dark Insight').id, rank: 0},
  ],
  weapons: [
    {
      name: 'Ceremonial dagger',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 4,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Bone staff (+4 damage to magic attacks; the first Range effect added does not increase difficulty; when used to cast a spell that inflicts 1 or more wounds, caster heals 1 wound), heavy robes (+1 defense), forbidden grimoire (a user with the Dark Insight talent can add the Additional Target and Enervate effects to Curse spells with no increase in difficulty).</p>",
  spells: [
    {name: 'Death Knell', description: 'Choose one target at short or medium range and make an <b>Average ([d][d]) Arcana check</b>. If successful, this magic attack inflicts 8 damage +1 damage per uncanceled [su], with Critical Rating 2 and the Ensnare 3 and Vicious 3 qualities.'},
    {name: 'Wilt', description: 'Choose two targets within short range and make an <b>Average ([d][d]) Arcana check</b>. If successful, the Necromancer can choose one additional target for each [ad] and all targets reduce the ability of any skill checks they make by one until the end of the necromancer’s next turn. If an affected character suffers strain for any reason, they suffer 1 additional strain. The necromancer can perform the concentrate maneuver to maintain all effects of this curse.'},
    {name: 'Wall of Bones', description: 'Make a <b>Hard ([d][d][d]) Arcana check</b>. If successful, the necromancer reduces the damage of all hits they suffer by 1 plus 1 for every [su][su] beyond the first until the end of his next turn; in addition, if an attack targeting the necromancer generates [t][t][t] or [des], the attacker suffers a hit inflicting damage equal to the total damage of the attack; the necromancer may perform the concentrate maneuver to maintain the effects of this barrier.'},
  ],
  description: "<p>Necromancy is forbidden as the darkest of all possible magical arts, one that corrupts the wielder and brings ruin to those who allow it to fester. Though they may try to hide their acts, necromancers are usually found out as they are spurred on to greater acts of desecration or as their bodies become more noticeably twisted by the demands necromancy places upon living flesh. Those who survive discovery invariably find themselves in the Mistlands, wandering from crypt to crypt in search of either a patron or enough untouched dead to raise into protective retinues.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/7/7e/Necromancer.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Reanimate')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 4,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 3,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
    {id: Skill.find_by_name('Perception').id, rank: 0},
    {id: Skill.find_by_name('Ranged').id, rank: 0},
    {id: Skill.find_by_name('Resilience').id, rank: 0},
    {id: Skill.find_by_name('Vigilance').id, rank: 0},
  ],
  abilities: [
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
    {name: 'Undying', description: 'may spend [t][t][t] from any check made by a PC to return one previously defeated Reanimate to an existing minion group, removing damage from the group accordingly. Spend [des] to return two Reanimates to a minion group).'},
  ],
  weapons: [
    {
      name: 'Rusted blade',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: []
    },
    {
      name: 'Worn bow',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 6,
      critical: 3,
      range: 'Medium',
      qualities: []
    },
  ],
  equipment: "<p>Antique mail (+1 soak).</p>",
  description: "<p>Reanimates form the backbone of most undead forces, from the smallest raiding warband of one of the lesser vampires of Bilehall to Waiqar’s Deathborn Legions themselves. Reanimates feel neither pain nor fear, and when struck down, they simply pick themselves back up again and continue fighting. Perhaps most chilling is their utter silence. Though they open their mouths as if to utter a war cry, the only sound from their ranks is the rattling of bone against rusted armor. Despite their terrifying presence, though, they are not invincible. Separated from their master, for example, they lose much of their will to fight.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/8/8f/Reanimate.jpg/revision/latest?cb=20160111082148'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Undead', 'The Haunted City')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Wraith')
adversary.update_attributes(
  brawn: 1,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 3,
  presence: 1,
  adversary_type: 'Rival',
  soak: 0,
  wound_th: 19,
  strain_th: nil,
  defense_melee: 3,
  defense_ranged: 3,
  combat_rating: 3,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Ranged').id, rank: 1},
  ],
  abilities: [
    {name: 'Ghostly', description: 'May move over or through terrain (including doors and walls) without penalty. Halve the damage dealt to the wraith before applying soak, unless the attack came from a magical source such as a spell or magical weapon.'},
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all of its opponents must make a <b>Hard ([d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents only make one fear check against the most terrifying enemy.'},
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
  ],
  weapons: [
    {
      name: 'Spectral claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 2,
      critical: 1,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Breach').id, rank: 1},
        {id: ItemQuality.find_by_name('Stun Damage').id, rank: 0},
      ]
    },
    {
      name: 'Wailing cry',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 2,
      critical: 5,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Breach').id, rank: 1},
        {id: ItemQuality.find_by_name('Stun Damage').id, rank: 0},
      ]
    },
  ],
  description: "<p>These horrific spectral creatures conjure fear in even the most stouthearted of Daqan knights as they eerily glide over marshes and through bulwarks, bringing the icy cold touch of the grave to whomever they encounter. Often just the sight of a single wraith is enough to send disciplined units screaming in retreat, and many an adventuring band has been reduced to a single gibbering survivor who barely returns to tell their tale.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/a/a4/Wraith.jpg'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Undead')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Aymhelin Scion')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 2,
  presence: 3,
  adversary_type: 'Rival',
  soak: 5,
  wound_th: 18,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 6,
  social_rating: 2,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 4},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Cage of Roots', description: 'Once per round after a character moves within medium range of the Aymhelin Scion, the Aymhelin Scion may perform an out-of-turn incidental to immobilize that character for the remainder of the encounter. As an action an immobilized character can attempt a <b>Hard ([d][d][d]) Athletics check</b> on their turn to no longer be immobilized'},
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
  ],
  weapons: [
    {
      name: 'Limbs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 8,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  description: "<p>Some say the Elves use arcane, ritualistic magic to animate huge Deepwood trees. Others claim that the trees achieved mobility on their own, a byproduct from millennia of magic being cast across the Aymhelin. No matter their origin, the trees of the Aymhelin are potent allies. Scions are young, only a few centuries old perhaps, and often accompany the Elves when battle must be done outside the great forest. Though not as powerful as Forest Guardians, these trees can ensnare foes to make then easy targets for Elven archers.</p>",
  image_url: 'https://www.goodlookgamer.com/wp-content/uploads/2017/11/Runewars-Fantasy-Board-Game-Aymhelin-Scion-Concept-Art.jpg'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elemental', 'Elves')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Deepwood Archer')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 2,
  presence: 2,
  adversary_type: 'Minion',
  soak: 4,
  wound_th: 4,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 4,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 0},
    {id: Skill.find_by_name('Ranged').id, rank: 0},
    {id: Skill.find_by_name('Perception').id, rank: 0},
    {id: Skill.find_by_name('Vigilance').id, rank: 0},
  ],
  abilities: [
    {name: 'Point Blank Shot', description: 'When making a Ranged combat check targeting an opponent the Deepwood Archer is engaged with, increase the difficulty once (instead of twice).'},
  ],
  weapons: [
    {
      name: 'Deepwood Longbow',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 8,
      critical: 2,
      range: 'Extreme',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Cumbersome').id, rank: 3},
        {id: ItemQuality.find_by_name('Superior').id, rank: 0},
        {id: ItemQuality.find_by_name('Unwieldy').id, rank: 3},
      ]
    },
  ],
  equipment: "<p>Leather armor (+1 soak).</p>",
  description: "<p>The Deepwood Archers take exceptional pride in their abilities, a confidence that often drifts into high-handed arrogance — especially when dealing with bowmen in other lands. When they are not serving as guards within the ranks of the Starplain Archers, the Blythwth Arethyl, or the Deepwood Rangers, they seek to participate in bowyer contests, instruct other Elven archers, and locate more impressive game than can be found in the Thalian Glades.</p>",
  image_url: 'https://www.goodlookgamer.com/wp-content/uploads/2017/11/Runewars-Fantasy-Board-Game-Deepwood-Archers-Concept-Art.jpg'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elves')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Forest Guardian')
adversary.update_attributes(
  brawn: 5,
  agility: 2,
  intellect: 2,
  cunning: 2,
  willpower: 3,
  presence: 3,
  adversary_type: 'Nemesis',
  soak: 6,
  wound_th: 25,
  strain_th: 18,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 8,
  social_rating: 3,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Resilience').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
  ],
  abilities: [
    {name: 'Sweep Attack', description: 'A Forest Guardian may spend [ad] from a Brawl check to hit an additional engaged opponent that would be no more difficult to attack than the original target, dealing base damage +1 damage per [su]'},
    {name: 'Silhouette 3', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
  ],
  weapons: [
    {
      name: 'Huge limbs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 10,
      critical: 4,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
        {id: ItemQuality.find_by_name('Prepare').id, rank: 1},
      ]
    },
  ],
  description: "<p>Forest Guardians are unnerving to behold at any time, but especially in combat. The largest of the animated trees of the Aymhel, they march forward with a fearless abandon, lashing out with their “legs” and using the dense gnarl of their branches and trunks to smash and crush anything that stands before them. Forest Guardians are preternaturally strong, have the inherent resilience of a dense hardwood tree, and act with an efficient brutality borne of beings who may be utterly divorced from the constraints of traditional mortality.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/b/b5/Forest_Guardian_War_Party.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elemental', 'Elves')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Leonx Rider')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 3,
  presence: 3,
  adversary_type: 'Rival',
  soak: 4,
  wound_th: 15,
  strain_th: nil,
  defense_melee: 2,
  defense_ranged: 1,
  combat_rating: 4,
  social_rating: 3,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Melee (Heavy)').id, rank: 2},
    {id: Skill.find_by_name('Riding').id, rank: 2},
    {id: Skill.find_by_name('Survival').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  abilities: [
    {name: 'Leonx Bond', description: 'When a Leonx Rider directs its mount, the mount may perform an action and a maneuver and is not limited to using its maneuvers to move.'},
  ],
  weapons: [
    {
      name: 'Spear',
      skill_id: Skill.find_by_name('Melee (Heavy)').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Leather armor (+1 soak), Leonx</p>",
  description: "<p>None exemplify the primal, wild nature of the Verdelam Elves more than the Leonx Riders. Charging into battle atop the ferocious Leonx, large sentient felines native to the Aymhelin, these Elven warriors pierce through enemy lines with the force of a storm, using their lightning speed to evade counterblows and change their angles of attack with the swiftness of a gale.</p>",
  image_url: 'https://bowlivestorage.blob.core.windows.net/beastsofwarlivesite/2017/04/Aliana-of-Summersong1.png'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elves')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Leonx')
adversary.update_attributes(
  brawn: 4,
  agility: 4,
  intellect: 1,
  cunning: 3,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 4,
  wound_th: 14,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 4,
  social_rating: 1,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Survival').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Maul', description: 'A Leonx adds [b][b] to Brawl checks against prone or immobilized targets'},
  ],
  weapons: [
    {
      name: 'Claws and fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 2},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  description: "<p>Each half of a Leonx-Verdelam pair trusts the other with its life, and the bond a Leonx shares with its rider is akin to that of family. In battle, the Leonx and rider coordinate their movements as one, fusing Elven skill with the deadly instincts of one of Mennara’s most fearsome predators.</p>",
  image_url: 'https://spikeybits.com/wp-content/uploads/2017/04/Latari-Elves.jpg'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elves')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Storm Sorceress')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 4,
  cunning: 3,
  willpower: 4,
  presence: 3,
  adversary_type: 'Nemesis',
  soak: 2,
  wound_th: 15,
  strain_th: 18,
  defense_melee: 2,
  defense_ranged: 2,
  combat_rating: 8,
  social_rating: 4,
  general_rating: 5,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Lore').id, rank: 4},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  abilities: [
    {name: 'Elemental Mastery', description: 'The first effect that a storm sorceress adds to a spell does not increase the difficulty of the Arcana check.'},
  ],
  equipment: "<p>Magic staff (the first Range effect added to a spell does not increase its difficulty; increase damage of Attack spells by +4), robes (+1 defense).</p>",
  spells: [
    {name: 'Blizzard', description: 'The storm sorceress chooses a target within long range and makes a <b>Daunting ([d][d][d][d]) Arcana check</b>. If the check is successful, this magic attack inflicts 8 damage +1 damage per [su] with the Blast 4, Disorient 4, Ensnare 4, and Knockdown qualities.'},
    {name: 'Stormbolt', description: 'The storm sorceress chooses a target within medium range and makes an <b>Easy ([d]) Arcana check</b>. If the check is successful, this magic attack inflicts 8 damage +1 damage per [su], with the Auto-fire and Stun 4 qualities.'},
    {name: 'Squall', description: 'The storm sorceress chooses a target within medium range and makes an <b>Easy ([d]) Arcana check</b>. If the check is successful, this magic attack inflicts 8 damage +1 damage per [su]. The storm sorceress may spend [ad] on the check to move the target or a character engaged with the target up to one range band in any direction.'},
  ],
  description: "<p>Drawn from Elves with centuries of training in elemental magic, the storm sorceresses have an aweinspiring ability to take control of the air around them. They use that dominance to call blinding blizzards, scatter their foes with gale-force winds, and strike their enemies with brutally precise lightning.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/e/ee/Storm_Sorceress.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elves', 'Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'True Fae')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 4,
  cunning: 4,
  willpower: 3,
  presence: 4,
  adversary_type: 'Nemesis',
  soak: 3,
  wound_th: 17,
  strain_th: 19,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 9,
  social_rating: 4,
  general_rating: 6,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 3},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Lore').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Quick Strike').id, rank: 2},
  ],
  abilities: [
    {name: 'Creature of the Aenlong', description: 'A Fae reduces the difficulty of Arcana checks by one, to a minimum of Easy ([d]), and the base damage of their Attack spells is increased by three.'},
    {name: 'Flyer', description: 'Can fly; see page 100 of Genesys CRB'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Daunting ([d][d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},
  ],
  weapons: [
    {
      name: 'Eldritch blade',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 4,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 2},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 4},
      ]
    },
    {
      name: 'Claws and teeth',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 4,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  spells: [
    {name: 'Blinding Curse', description: 'The Fae chooses one target within short range and makes an <b>Average ([d][d]) Arcana check</b>. If the check is successful, the target decreases the ability of all checks they make and adds [b][b] to all checks involving sight until the end of the Fae’s next turn. The Fae can sustain these effects by performing the concentrate maneuver.'},
    {name: 'Shuddering Paralysis', description: 'The Fae chooses one target at short ange and makes an <b>Easy ([d]) Arcana check</b>. If the check is uccessful, this magic attack inflicts 7 damage, +1 damage per s], with the Ensnare 3 quality.'},
  ],
  description: "<p>The Fae are utterly terrifying. Their appearance varies wildly, but tales are told of distorted nostrils that lead to wide maws filled with razor-sharp teeth. Their horns and antlers are said to jut at unpredictable angles from their skull in ways none of the many monsters that wander the countryside can match. Most disturbing are the sightless visages some are claimed to posses; how they see without eyes is a mystery.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/8/80/Zyla.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Deep Elf')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 3,
  presence: 2,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 3,
  social_rating: 2,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Forbidden').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Skulduggery').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Dual Wielder').id, rank: 0},
  ],
  abilities: [
    {name: 'Shadow Training', description: 'Enemies add [s] to magic checks targeting a Deep Elf.'},
  ],
  weapons: [
    {
      name: 'Two freezing blades',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Sunder').id, rank: 0},
      ]
    },
  ],
  description: "<p>Though most Elves live in relative harmony with the world and the races around them, some members of the Elven race have turned their back on their cousins. The goals of these Elves are to more fully embrace Emorial’s mission against the forces of the Ynfernael and to redeem their name from its sullying by Malcorne, the Corrupted One.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elves')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Dimora')
adversary.update_attributes(
  brawn: 4,
  agility: 1,
  intellect: 3,
  cunning: 2,
  willpower: 4,
  presence: 1,
  adversary_type: 'Nemesis',
  soak: 6,
  wound_th: 18,
  strain_th: 18,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 7,
  social_rating: 3,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 3},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 3},
    {id: Skill.find_by_name('Lore').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Durable').id, rank: 2},
  ],
  abilities: [
    {name: 'Creature of the Aenlong', description: 'A Dimora reduces the difficulty of Arcana checks by one, to a minimum of Easy ([d]), and the base damage of their Attack spells is increased by three.'},
    {name: 'Stonewalker', description: 'A Dimora moves normally through difficult or impassable terrain of stone, soil, or similar materials.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Daunting ([d][d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},
  ],
  weapons: [
    {
      name: 'Stone blades',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
    {
      name: 'Stone fists',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 2},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  spells: [
    {name: 'Tremor', description: 'The Dimora chooses one target at short range and makes an Average ([d][d]) Arcana check. If the check is successful, this magic attack inflicts 7 damage, +1 damage per [su] and the target and all characters engaged with the target are knocked prone. The Dimora can spend [ad] on the check to move the target up to one range band in any direction.'},
    {name: 'Prison of Stone', description: 'The Dimora chooses one target at short range and makes an Average ([d][d]) Arcana check. If the check is successful, this magic attack inflicts 7 damage, +1 damage per [su], with the Blast 3 and Ensnare 3 qualities.'},
  ],
  description: "<p>Creatures born of the Aelong, the Dimora are believed to cross into the world as formless elementals. They then build themselves out of the earth, taking on the form of stony, vaguely mole-like creatures. They are constantly shifting in density and composition, perpetually shedding the soil of their bodies and replacing what’s lost with fresh loam from the ground beneath them.</p>",
  image_url: 'https://myth-wiki-thumb.s3.amazonaws.com/a/a0/Earth_Elemental.jpg/339px-Earth_Elemental.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elemental')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Dwarf Guilder')
adversary.update_attributes(
  brawn: 2,
  agility: 1,
  intellect: 3,
  cunning: 2,
  willpower: 3,
  presence: 2,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 14,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 1,
  social_rating: 2,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Melee (Light)').id, rank: 2},
    {id: Skill.find_by_name('Ranged').id, rank: 2},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
  ],
  abilities: [
    {name: 'Dark Vision', description: 'When making skill checks, Dwarves remove up to [s][s] imposed due to darkness.'},
    {name: 'Guild Member', description: 'When performing a check related to their specific guild, Dwarf Guilders upgrade the ability of the check twice.'},
  ],
  equipment: "<p>Guild tools and scrolls.</p>",
  description: "<p>Dwarf guilds dominate society in the Dunwarr Mountains, and guild members are everywhere in the tunnels under the mountains here. The ten guilds cover a wide range of knowledge, abilities, and traditions, and so guild members are ideal sources of information for visitors. Depending on the guild, they can be excellent weapon forgers and repairers, decipherers of mysterious runes, and even scouts and fighters. For tired adventurers, though, members of the Brewers' Guild may be the most welcome to locate.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/e/ed/Dwarf.png/revision/latest?cb=20181117011629'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Dwarves')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Dwarven Dragon Hunter')
adversary.update_attributes(
  brawn: 4,
  agility: 2,
  intellect: 2,
  cunning: 3,
  willpower: 4,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 6,
  wound_th: 17,
  strain_th: 15,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 9,
  social_rating: 3,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Discipline').id, rank: 4},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Ranged').id, rank: 4},
    {id: Skill.find_by_name('Resilience').id, rank: 3},
    {id: Skill.find_by_name('Survival').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  abilities: [
    {name: 'Dauntless', description: 'Dwarven Dragon Hunters upgrade the ability of Discipline checks they make to resist fear or intimidation once.'},
    {name: 'Dark Vision', description: 'When making skill checks, Dwarves remove up to [s][s] imposed due to darkness.'},
  ],
  weapons: [
    {
      name: 'Axe',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 8,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
    {
      name: 'Portable bolt thrower',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 9,
      critical: 2,
      range: 'Extreme',
      qualities: [
        {id: ItemQuality.find_by_name('Cumbersome').id, rank: 3},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 2},
        {id: ItemQuality.find_by_name('Prepare').id, rank: 1},
      ]
    },
    {
      name: 'Dwarven firebombs',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 8,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Blast').id, rank: 6},
        {id: ItemQuality.find_by_name('Burn').id, rank: 2},
        {id: ItemQuality.find_by_name('Limited Ammo').id, rank: 1},
      ]
    },
  ],
  description: "<p>The Dwarven Dragon Hunters are hostile, rage-filled, proselytizing zealots who give every aspect of their lives to the hunt of their ancestral foes. They train endlessly in a specialized style of combat most effectively suited to ending the lives of dragons wherever they can be found, then make pilgrimages to places that report sightings of the beasts. Typically, when Dragon Hunters leave the Dunwarr Mountains, they aggressively try to enlist anyone whom they see as having the makings of an adventurer to join their quest. Refusal results in a barrage of harsh and belittling insults at best, for Dragon Hunters tend to take a dim view of anyone who doesn’t hate dragons as enthusiastically as they do. In some cases, they have even taken their blade to anyone seen as “Dargeth Moni,” or “friend of filth-lizards.”</p>",
  image_url: 'https://cdna.artstation.com/p/assets/images/images/000/950/240/large/adrian-wilkins-dragon-hunter-final-by-adrian-wilkins.jpg?1443931502'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Dwarves')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Kennsir Dwarf')
adversary.update_attributes(
  brawn: 2,
  agility: 1,
  intellect: 2,
  cunning: 3,
  willpower: 3,
  presence: 2,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 15,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 0,
  combat_rating: 3,
  social_rating: 1,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 3},
    {id: Skill.find_by_name('Skulduggery').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 2},
    {id: Skill.find_by_name('Survival').id, rank: 2},
  ],
  abilities: [
    {name: 'Dark Vision', description: 'When making skill checks, Dwarves remove up to [s][s] imposed due to darkness.'},
    {name: 'Tunnel Fighter', description: 'Add [b][b] to combat checks Kennsir Dwarves make in enclosed spaces'},
  ],
  weapons: [
    {
      name: 'Brace of Dwarven pickaxes',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 4,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 2},
      ]
    },
  ],
  equipment: "<p>Smokebombs, vials of poison gas.</p>",
  description: "<p>Though Dunwarr Dwarves live, fight, and often die in the tunnels beneath the mountains, the Kennsir Dwarves take fighting to horrifying levels of strategic brutality. They live for battle in the claustrophobic, fortified tunnels under the mountains—employing the unconventional, unpredictable strategies of guerrilla warriors. Whether they are tunneling deep behind invaders’ flanks, collapsing mine shafts, or choking their foes in noxious gasses from the Alchemists’ League, there are none better at tunnel fighting in all the world.</p>",
  image_url: 'https://i.pinimg.com/736x/9e/df/63/9edf631be1a8de3bed202f6530287d9f--engineers-character-inspiration.jpg'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Dwarves')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Kobold')
adversary.update_attributes(
  brawn: 1,
  agility: 2,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 1,
  wound_th: 3,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 1,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
    {id: Skill.find_by_name('Skulduggery').id, rank: 0},
  ],
  abilities: [
    {name: 'Not Another!', description: 'In an encounter with any number of kobolds, the GM may spend [t][t] from any check made by a PC to add one kobold to an existing minion group, or spend [des] to add a new minion group of three kobolds to the encounter.'},
    {name: 'Silhouette 0', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
  ],
  weapons: [
    {
      name: 'Claws and teeth',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 2,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
    {
      name: 'Crude blade',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 4,
      critical: 3,
      range: 'Engaged',
      qualities: [
      ]
    },
  ],
  description: "<p>Centuries ago, a sorcerer desperately in need of minions used magic to twist a race of beastmen to suit his own depraved purposes. He made them smaller so they could better work in tight spaces, slashed their intelligence so they would never revolt against him, made them genderless so they wouldn’t be distracted by mating instincts, and gave them the ability to reproduce asexually so they would always have large enough numbers for their tasks. He was an exceptionally powerful magician whose intellect was matched only by his lack of foresight.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/0/08/Kobolds.png'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Ancient Dragon')
adversary.update_attributes(
  brawn: 5,
  agility: 4,
  intellect: 5,
  cunning: 4,
  willpower: 5,
  presence: 4,
  adversary_type: 'Nemesis',
  soak: 8,
  wound_th: 45,
  strain_th: 34,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 17,
  social_rating: 10,
  general_rating: 10,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 4},
    {id: Skill.find_by_name('Brawl').id, rank: 4},
    {id: Skill.find_by_name('Charm').id, rank: 3},
    {id: Skill.find_by_name('Coercion').id, rank: 4},
    {id: Skill.find_by_name('Cool').id, rank: 3},
    {id: Skill.find_by_name('Discipline').id, rank: 4},
    {id: Skill.find_by_name('Ranged').id, rank: 4},
    {id: Skill.find_by_name('Resilience').id, rank: 4},
    {id: Skill.find_by_name('Runes').id, rank: 4},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Claw Sweep', description: 'A dragon may spend [ad] from a Brawl check to hit one additional engaged opponent that would be no more difficult to attack than the original target, dealing base damage +1 damage per [su].'},
    {name: 'Flyer', description: 'Can fly; see page 100 of Genesys CRB'},
    {name: 'Silhouette 4', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Daunting ([d][d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},
  ],
  weapons: [
    {
      name: 'Fiery breath',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 16,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Blast').id, rank: 16},
        {id: ItemQuality.find_by_name('Burn').id, rank: 3},
        {id: ItemQuality.find_by_name('Prepare').id, rank: 1},
      ]
    },
    {
      name: 'Claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 17,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
        {id: ItemQuality.find_by_name('Sunder').id, rank: 0},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 5},
      ]
    },
  ],
  spells: [
    {name: 'Unbind Spell', description: 'The dragon chooses a target within short range that is under the effects of a spell and makes a <b>Hard ([d][d][d][d]) Arcana check</b>. If the check is successful, the spell effects on the target end.'},
    {name: 'Words of Unmaking', description: "The dragon chooses one magic item (including a magic weapon and armor) or runebound shard within medium range and make an <b>Daunting ([d][d][d][d]) Arcana check</b>. If successful, until the beginning of the dragon's next turn that item loses its magical ability and becomes a mundane gem, suit of armor, sword, rock, etc. The dragon may maintain this spell by performing the concentrate maneuver."},
  ],
  description: "<p>The feral beasts that terrorize the people of Terrinoth are only pale echoes of the dragons to be found in the Molten Heath. Here, the oldest, wisest, and most powerful dragons make their kingdom, under the guidance of the Dragon Rex. Ancient dragons have all the physical power of their southern kin, matched to an intellect and mastery of magic beyond any mortal. Seldom have the dragons seen fit to meddle in the affairs of mortals, and since the time of the Dragon Wars, these godlike beings have largely passed into legend.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/b/bb/Dragon.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Dragon')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Dwarf Ancestral Specter')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 4,
  presence: 2,
  adversary_type: 'Rival',
  soak: 4,
  wound_th: 14,
  strain_th: nil,
  defense_melee: 3,
  defense_ranged: 2,
  combat_rating: 6,
  social_rating: 2,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Discipline').id, rank: 0},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
  ],
  abilities: [
    {name: 'Ancestral Gaze', description: 'While within short range of a Dwarf Ancestral Specter, living Dwarves add [su] to their combat checks and [tri] if the opponent is a dragon.'},
    {name: 'Ghostly', description: 'Dwarf Ancestral Specters ignore the effects of terrain and darkness'},
    {name: 'Terrifying', description: 'At the start of the encounter, all of its opponents must make a <b>Hard ([d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents only make one fear check against the most terrifying enemy.'},
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
  ],
  weapons: [
    {
      name: 'Ethereal hammer',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 7,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Stun Damage').id, rank: 0},
      ]
    },
  ],
  description: "<p>The Dwarves of Dunwarr hold that the valiant Dwarves who bravely gave their lives defending their ancestral homes agains the dragons have never ended their battle, even in death Many ballads tell of the brave spirits who still roam the Molte Heath, seeking vengeance against the dragons and other who stole their lands. One day, the Dwarves know, they wil reclaim their ancestral home, and perhaps only then will thes spirits allow themselves to rest.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Dwarves')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Lava Elemental')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 3,
  presence: 1,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 13,
  strain_th: nil,
  defense_melee: 4,
  defense_ranged: 4,
  combat_rating: 6,
  social_rating: 2,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  abilities: [
    {name: 'Amorphous', description: 'As an incidental, a lava elemental may increase or decrease its silhouette by 1.'},
    {name: 'Inconspicuous', description: 'A lava elemental in a natural pool of molten rock appears indistinguishable from ordinary lava; a character can make a Hard ([d][d][d]) Perception or Vigilance check to identify a lava elemental.'},
  ],
  weapons: [
    {
      name: 'Pseudopods',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Burn').id, rank: 2},
        {id: ItemQuality.find_by_name('Sunder').id, rank: 0},
      ]
    },
    {
      name: 'Lava projectile',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 9,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Blast').id, rank: 4},
        {id: ItemQuality.find_by_name('Burn').id, rank: 2},
        {id: ItemQuality.find_by_name('Concussive').id, rank: 1},
      ]
    },
  ],
  description: "<p>While most elementals reside in the distant lands of Al-Kalim, a few also live in the even hotter lairs within the Heart. The volcanoes in this land are an ideal environment for these magical creatures, though it is uncertain if the elementals are the cause of the deadly heat or if the volcanoes attracted their attention.</p>",
  image_url: 'http://gowdb.com/assets/troops/Troop_K00_06.png'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elemental')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Salamander')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 1,
  cunning: 3,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 5,
  wound_th: 18,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 4,
  social_rating: 1,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 3},
    {id: Skill.find_by_name('Survival').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 1},
  ],
  abilities: [
    {name: 'Flameborn', description: 'A salamander does not suffer damage from natural heat or flame, and doubles its soak for purposes of reducing damage from magical fire.'},
  ],
  weapons: [
    {
      name: 'Burning fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Burn').id, rank: 2},
        {id: ItemQuality.find_by_name('Sunder').id, rank: 0},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  description: "<p>The Dragonkin known as salamanders lack the intelligence and arcane power of true dragons, but are nonetheless fearsome creatures and the foundation for countless legends. Salamanders are creatures of magic, although they lack the requisite intelligence to consciously master this power. Heat radiates from their smooth reptilian bodies and the creatures seem particularly suited for life in the Heath—few other environments could tolerate their inflammatory presence.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Young Dragon')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 3,
  cunning: 2,
  willpower: 2,
  presence: 2,
  adversary_type: 'Rival',
  soak: 6,
  wound_th: 23,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 8,
  social_rating: 2,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 1},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 3},
    {id: Skill.find_by_name('Ranged').id, rank: 2},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
    {id: Skill.find_by_name('Runes').id, rank: 1},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Flyer', description: 'Can fly; see page 100 of Genesys CRB'},
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Daunting ([d][d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},
  ],
  weapons: [
    {
      name: 'Fiery breath',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 8,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Blast').id, rank: 8},
        {id: ItemQuality.find_by_name('Burn').id, rank: 2},
        {id: ItemQuality.find_by_name('Prepare').id, rank: 1},
      ]
    },
    {
      name: 'Claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 8,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  spells: [
    {name: '', description: 'A young dragon can use any magic actions allowed for the Arcana and Runes skills, and may select additional effects as normal. A young dragon is still learning the magical crafts of its race, and might experiment with any manner of spell.'},
  ],
  description: "<p>Although weak and fragile compared to fully grown dragons, the youngest wyrmling is far from defenseless. A newly hatched dragon might be the size of a small stag, with talons strong and sharp enough to split a warrior open. Within a few years at most, the dragonling stands larger and stronger than an ox. Similarly, although they lack the intense depth of insight and centuries of wisdom of their elders, even the youngest dragons are at least the intellectual equals of the finest minds of Human civilization.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Dragon')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Gurak Tol')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 3,
  presence: 1,
  adversary_type: 'Rival',
  soak: 1,
  wound_th: 10,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 5,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 3},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Tail catapult', description: 'When engaged with a Gurak Tol that the Orc outider is bonded with, the Orc outrider may use the steed to perform a Ranged attack.'},
  ],
  weapons: [
    {
      name: 'Claws and fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
    {
      name: 'Tail',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 3},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
    {
      name: 'Tail catapult',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 8,
      critical: 4,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Concussive').id, rank: 1},
        {id: ItemQuality.find_by_name('Inaccurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>When bonded with an Orc outrider, a Gurak Tol gains armor that adds +2 to its soak.</p>",
  description: "<p>Gurak Tols are territorial, six-legged lizards that jealously guard their homes; whether that be a warren, isolated cave, or valley. That instinctive behavior has made the creatures invaluable allies for the Orcs.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/5/58/Gurak_Tol_Riders.PNG'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Orcs')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Orc Spiritspeaker')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 2,
  cunning: 3,
  willpower: 3,
  presence: 3,
  adversary_type: 'Nemesis',
  soak: 3,
  wound_th: 15,
  strain_th: 17,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 8,
  social_rating: 5,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Lore').id, rank: 3},
    {id: Skill.find_by_name('Primal').id, rank: 4},
  ],
  talents: [
    {id: Talent.find_by_name('Second Wind').id, rank: 0},
  ],
  abilities: [
    {name: 'Spiritual Focus', description: 'A spiritspeaker adds [b] to Primal checks they make for each Orc ally within short range.'},
  ],
  equipment: "<p>Magic staff (add +4 damage to magic attacks; the first Range effect added to a spell does not increase its difficulty), beast-hide robes (+1 defense).</p>",
  spells: [
    {name: 'Fury of the Spirits', description: 'The spiritspeaker chooses one target at short or medium range and makes a <b>Hard ([d][d][d]) Primal check</b>. If the check is successful, this magic attack inflicts 7 damage, +1 damage per [su], with the Pierce 3 and Sunder qualities.'},
    {name: 'Ancestral Strength', description: 'The spiritspeaker chooses two allies within short range and makes a <b>Hard ([d][d][d]) Primal check</b>. If the check is successful, the spiritspeaker may affect additional targets in range by spending [ad] for each additional target. Targets affected by this spell increase the ability of skill checks they make until the end of the spiritspeaker’s next turn by one. The spiritspeaker may sustain the effects of this spell by performing the concentrate maneuver.'},
  ],
  description: "<p>Magic users unique to the Orcish race, spiritspeakers use a shamanistic connection through the earth to communicate with the hidden, ethereal creatures of Mennara. This not only allows them to speak as mediums for The Unseen, but also to act as conduits for the spirits to act on the mundane world.</p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Orcs', 'Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Orc Outrider')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 5,
  wound_th: 14,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 5,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Melee (Light)').id, rank: 2},
    {id: Skill.find_by_name('Ranged').id, rank: 2},
    {id: Skill.find_by_name('Riding').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Quick Draw').id, rank: 0},
  ],
  abilities: [
    {name: 'Brutal Training', description: 'When an Orc outrider directs its mount, the mount may perform an action and a maneuver and is not limited to using its maneuvers to move.'},
  ],
  weapons: [
    {
      name: 'Orc throwing spear',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 7,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Limited Ammo').id, rank: 1},
      ]
    },
    {
      name: 'Axe',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Extra spears, leather armor (+1 soak), armored Gurak Tol mount.</p>",
  description: "<p>Lizard riders from the Great Beast Clan, the Mag Ugluk Urak are mounted warriors who are as dangerous as any cavalry in Mennara. Nomadic Orcs who follow elk and other wild game across the Broken Plains, they tend to lead fairly simple lives as hunters and gatherers. Following their chieftain's noble example, the Mag Ugluk Urak are brutally honest and always good to their word. They will never cheat during a trade, stab an ally in the back, or abandon a pledge. If that pledge is to kill an adversary or slaughter an enemy, then one of the two are absolutely going to die.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/5/58/Gurak_Tol_Riders.PNG'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Berserker')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 5,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
    {id: Skill.find_by_name('Resilience').id, rank: 0},
    {id: Skill.find_by_name('Survival').id, rank: 0},
  ],
  abilities: [
    {name: 'Bone Spurs', description: 'A berserker who is targeted by a melee combat check may spend [t][t][t] or [des] to cause the attacker to suffer 3 wounds.'},
    {name: 'A Good Death', description: 'As an incidental, at the start of its turn a minion group of berserkers that is engaged with an enemy may remove a member of its group as a casualty to add +6 damage to the minion group\'s attack in that turn'},
  ],
  weapons: [
    {
      name: 'Two bone blades',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 4,
      range: 'Engaged',
      qualities: [
      ]
    },
  ],
  description: "<p>Frothing with bloody frenzy, Uthuk berserkers hurl themselves into the press of combat, craving only to hack apart their foes. Touched by the demonic darkness of the Ru, these Uthuk warriors have given themselves over completely to mindless savagery. Forsaking the discipline of drill and formation, as well as armor and shield, scorning anything that might slow their headlong charge into the fray, they become depraved murderers in combat.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/9/94/Frenzy.jpg'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Bloodsister')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 4,
  cunning: 3,
  willpower: 4,
  presence: 4,
  adversary_type: 'Nemesis',
  soak: 3,
  wound_th: 17,
  strain_th: 17,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 8,
  social_rating: 5,
  general_rating: 4,
  skills: [
    {id: Skill.find_by_name('Divine').id, rank: 3},
    {id: Skill.find_by_name('Coercion').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Forbidden').id, rank: 4},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 1},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Dark Insight').id, rank: 0},
  ],
  abilities: [
    {name: 'Telepathic Bond', description: 'A bloodsister can perform an incidental once per round to add [b] to the next check made by all other Uthuk Y’llan in the encounter.'},
    {name: 'Telepathic Domination', description: 'A bloodsister may attempt to telepathically dominate a foe once per encounter as an action, making an opposed Coercion vs. Discipline check targeting one character in short range; if successful, the target is immobilized for 1 round per [su], and the bloodsister may spend [tri] to stagger the target for 1 round.'},
  ],
  equipment: "<p>Bone staff (+4 damage to magic attacks; the first Range effect added does not increase difficulty; when used to cast a spell that inflicts 1 or more wounds, caster heals 1 wound), leather armor (+1 soak)</p>",
  spells: [
    {name: 'Bone Eruption', description: 'Select one target at short or medium range make a <b>Hard ([d][d][d]) Divine check</b>. If the check is successful, this magic attack inflicts 8 damage, +1 damage per [su], with the Pierce 4, Sunder, and Vicious 4 qualities, and with a Critical Rating of 2).'},
    {name: 'Exsanguinate', description: 'Select one target at short or medium range for this curse and make a <b>Hard ([d][d][d]) Divine check</b>. If the check is successful, until the end of the witch’s next turn, the target deceases the ability of any skill checks they make by one and suffers one additional wound each time they suffer wounds. The witch can maintain these effects with the concentrate maneuver).'},
  ],
  description: "<p>The most powerful of the Uthuk witches and warlocks are known as bloodsisters and nightseers, respectively. Though not part of the Blood Coven, these immensely skilled sorcerers are the true leaders of the Uthuk tribes. Their influence stretches out across the Ru Darklands, reaching wherever the blades of the Uthuk draw blood. Nightseers in particular are skilled in dreamwalking, a discipline of the original Loth K’har peoples, and have preserved this ancient tradition across the centuries. Adept in battle, gifted with supernatural longevity and resilience, and able to control the minds of those around them, few dare stand against them.</p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Nightseer')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 4,
  cunning: 3,
  willpower: 4,
  presence: 4,
  adversary_type: 'Nemesis',
  soak: 3,
  wound_th: 17,
  strain_th: 17,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 8,
  social_rating: 5,
  general_rating: 4,
  skills: [
    {id: Skill.find_by_name('Divine').id, rank: 3},
    {id: Skill.find_by_name('Coercion').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Forbidden').id, rank: 4},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 1},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Dark Insight').id, rank: 0},
  ],
  abilities: [
    {name: 'Telepathic Domination', description: 'A nightseer may attempt to telepathically dominate a foe once per encounter as an action, making an opposed Coercion vs. Discipline check targeting one character in short range; if successful, the target is immobilized for 1 round per [su], and the bloodsister may spend [tri] to stagger the target for 1 round.'},
    {name: 'Dreamwalker', description: 'Once per session, a nightseer can spend a Story Point to either re-roll the dice pool after making a check, or to force a PC to re-roll the dice pool after the PC makes a check.'},
  ],
  equipment: "<p>Bone staff (+4 damage to magic attacks; the first Range effect added does not increase difficulty; when used to cast a spell that inflicts 1 or more wounds, caster heals 1 wound), leather armor (+1 soak)</p>",
  spells: [
    {name: 'Bone Eruption', description: 'Select one target at short or medium range make a <b>Hard ([d][d][d]) Divine check</b>. If the check is successful, this magic attack inflicts 8 damage, +1 damage per [su], with the Pierce 4, Sunder, and Vicious 4 qualities, and with a Critical Rating of 2).'},
    {name: 'Exsanguinate', description: 'Select one target at short or medium range for this curse and make a <b>Hard ([d][d][d]) Divine check</b>. If the check is successful, until the end of the witch’s next turn, the target deceases the ability of any skill checks they make by one and suffers one additional wound each time they suffer wounds. The witch can maintain these effects with the concentrate maneuver).'},
  ],
  description: "<p>The most powerful of the Uthuk witches and warlocks are known as bloodsisters and nightseers, respectively. Though not part of the Blood Coven, these immensely skilled sorcerers are the true leaders of the Uthuk tribes. Their influence stretches out across the Ru Darklands, reaching wherever the blades of the Uthuk draw blood. Nightseers in particular are skilled in dreamwalking, a discipline of the original Loth K’har peoples, and have preserved this ancient tradition across the centuries. Adept in battle, gifted with supernatural longevity and resilience, and able to control the minds of those around them, few dare stand against them.</p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Flesh ripper')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 6,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 3,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Cool').id, rank: 0},
    {id: Skill.find_by_name('Resilience').id, rank: 0},
    {id: Skill.find_by_name('Vigilance').id, rank: 0},
  ],
  talents: [
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Daunting ([d][d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},
  ],
  weapons: [
    {
      name: 'Claws and fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
    {
      name: 'Spiked tail',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 1},
      ]
    },
  ],
  description: "<p>Flesh rippers are the heralds of the Uthuk armies: packs of these slavering beasts lead the charge into battle with savage roars and screams. There can be little doubt as to the demonic ancestry of these twisted creatures given their blood-red sinew, bulging muscles, and bony, armored hides. As large as fullgrown lions, flesh rippers tower over most prey. Their size, however, belies their speed. Racing across the ground on all fours, these vicious beasts can easily catch a knight on horseback, while soldiers on foot have no hope of outrunning them. When flesh rippers catch their foes, they truly live up to their name; even a single such monster will paint the earth red as it rends apart its prize. Of course, when one sees a single flesh ripper, others are never far behind.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/6/67/Flesh_ripper.jpg'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Grotesque')
adversary.update_attributes(
  brawn: 4,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 5,
  wound_th: 14,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 5,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Ranged').id, rank: 2},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  abilities: [
    {name: 'Bone Spurs', description: 'A grotesque may spend [t][t][t] or [des] on a melee combat check targeting them to inflict 6 damage on the attacker.'},
    {name: 'Killing Frenzy', description: 'A grotesque adds [b][b] to all melee combat checks, but attackers add [b] to all combat checks targeting the grotesque.'},
  ],
  weapons: [
    {
      name: 'Massive claw',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 1},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
    {
      name: 'Hurled bone shards',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 5,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  description: "<p>In battle, the grotesques are the lumbering elite of the Uthuk tribes, many rising to become warlords of their own raiding armies or wasteland fortresses. Horrendously strong in combat, grotesques can rip a person apart with their bare hands, carve them up with their bone spines, and even fling bone shards from their fists to fell enemies at a distance. Few bother to carry weapons or wear armor, as their demonically transformed bodies are able to take and dish out enormous amounts of punishment.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/5/58/Grotesque_2.JPG'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Spined thresher')
adversary.update_attributes(
  brawn: 5,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 3,
  presence: 3,
  adversary_type: 'Nemesis',
  soak: 7,
  wound_th: 25,
  strain_th: 18,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 9,
  social_rating: 3,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 4},
    {id: Skill.find_by_name('Brawl').id, rank: 4},
    {id: Skill.find_by_name('Resilience').id, rank: 4},
  ],
  talents: [
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Silhouette 3', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Spine-tow', description: 'A spined thresher can use a maneuver to move a target affected by its tail’s Ensnare quality up to one range band.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Daunting ([d][d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},
    {name: 'Too Many Mouths', description: 'Spined thresher can make a combined check to attack with any number of its weapons, and only needs to spend [ad] to hit with each additional weapon.'},
  ],
  weapons: [
    {
      name: 'Two fanged claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 9,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
    {
      name: 'Slavering maw',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 2},
        {id: ItemQuality.find_by_name('Sunder').id, rank: 0},
      ]
    },
    {
      name: 'Spiny tail',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 4,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 2},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  description: "<p>Known to the Uthuk as the Gw’reth Chuik, or “that which feeds”, these mighty demons are simply “spined threshers” to the defenders of the Land of Steel. When the enemies of the Uthuk cower behind the stone walls of their cities and keeps, the witches call out into the Ynfernael for the Gw’reth Chuik. If their sacrifices are sufficient, a huge demon hauls its chitinous form from the beyond to do battle for the Locust Swarm.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/9/9f/Spined_Thresher.PNG'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Viper legion archer')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 1,
  cunning: 3,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 3,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Ranged').id, rank: 3},
    {id: Skill.find_by_name('Stealth').id, rank: 2},
  ],
  abilities: [
    {name: 'Tailored Poison', description: 'Once per encounter before making a Ranged check, as a maneuver a viper legion archer can apply a tailored poison to an arrow; if the check succeeds, the attack inflicts additional damage equal to the target’s highest characteristic and gains the Disorient 3 quality.'},
  ],
  weapons: [
    {
      name: 'Viper Bow',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 8,
      critical: 3,
      range: 'Long',
      qualities: [
        {id: ItemQuality.find_by_name('Unwieldy').id, rank: 3},
      ]
    },
  ],
  equipment: "<p>A character wounded by the viper bow must make a <b>Hard ([d][d][d]) Resilience check</b> or suffer 3 additional wounds plus 1 strain per [t], and must check again on their next turn if the check generates [des]. Leather armor (+1 soak).</p>",
  description: "<p>Uthuk warriors are natural raiders, and none more so than the viper legion archers. As befits the vile ways in which the Uthuk wage war, members of the viper legion make extensive use of sorcerous poisons to tip their arrows. Brewed by the viper war leaders and blessed by the Blood Coven, these toxic elixirs are often tailored to the enemy’s weaknesses. Heroes’ Bane works against a foe’s courage so as to kill the brave faster than the cowardly, while Nightshood is made from the powdered remains of powerful undead and can steal an enemy’s sight. Perhaps the deadliest of all is the Kiss of Set, crafted from the distilled blood of a naga, which turns the target's heart into a bomb, bursting from their body and leaving a toxic cloud in their wake.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/d/d9/Viper_Legion.png/revision/latest?cb=20190616045044'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Witch')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 3,
  cunning: 3,
  willpower: 3,
  presence: 3,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 18,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 5,
  social_rating: 3,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Divine').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 1},
    {id: Skill.find_by_name('Forbidden').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 1},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Dark Insight').id, rank: 0},
  ],
  abilities: [
    {name: 'Telepathic Coordination', description: 'A witch can perform a maneuver to add [b] to the next check made by all other Uthuk Y’llan in the encounter.'},
    {name: 'Sacrifice', description: 'After inflicting wounds on a living creature (whether an enemy or ally), a witch adds [b] to her next magic skill check; after killing a living creature, a witch instead upgrades the ability of her next magic skill check once.'},
  ],
  weapons: [
    {
      name: 'Sacrificial blade',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 4,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Bone armor (+1 soak).</p>",
  spells: [
    {name: 'Boneshatter', description: 'Select one target at short range for this magic attack and make a <b>Hard ([d][d][d]) Divine check</b>. If the check is successful, the magic attack inflicts 5 damage, +1 damage per [su], with the Pierce 3, Sunder, and Vicious 3 qualities, and with a Critical Rating of 2).'},
    {name: 'Boil Blood', description: 'Select one target at short range for this curse and make an <b>Average ([d][d]) Divine check</b>. If the check is successful, until the end of the witch\'s next turn, the target decreases the ability of any skill checks they make by one and suffers one additional strain each time they suffer strain for any reason. The witch can maintain these effects with the concentrate maneuver).'},
  ],
  description: "<p>Witches and warlocks are the spiritual slave masters of the Uthuk tribes and the keepers of their dark Ynfernael magic. Only a handful of Uthuk are born with such gifts. These children often arrive into the world amid a portentous rain of blood or storm of crimson lightning. A long and torturous road follows; not all of them live to master their magic, as the brutality of Uthuk society ruthlessly weeds out the weak. Those who survive are cruel and skilled sorcerers and priestesses, utterly dedicated to the Uthuk and without a shred of mercy for their enemies.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Warlock')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 3,
  cunning: 3,
  willpower: 3,
  presence: 3,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 18,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 5,
  social_rating: 3,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Divine').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 1},
    {id: Skill.find_by_name('Forbidden').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 1},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Dark Insight').id, rank: 0},
  ],
  abilities: [
    {name: 'Telepathic Coordination', description: 'A warlock can perform a maneuver to add [b] to the next check made by all other Uthuk Y’llan in the encounter.'},
    {name: 'Sacrifice', description: 'After inflicting wounds on a living creature (whether an enemy or ally), a warlock adds [b] to his next magic skill check; after killing a living creature, a warlock instead upgrades the ability of his next magic skill check once.'},
  ],
  weapons: [
    {
      name: 'Sacrificial blade',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 4,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Bone armor (+1 soak).</p>",
  spells: [
    {name: 'Boneshatter', description: 'Select one target at short range for this magic attack and make a <b>Hard ([d][d][d]) Divine check</b>. If the check is successful, the magic attack inflicts 5 damage, +1 damage per [su], with the Pierce 3, Sunder, and Vicious 3 qualities, and with a Critical Rating of 2).'},
    {name: 'Boil Blood', description: 'Select one target at short range for this curse and make an <b>Average ([d][d]) Divine check</b>. If the check is successful, until the end of the warlock\'s next turn, the target decreases the ability of any skill checks they make by one and suffers one additional strain each time they suffer strain for any reason. The warlock can maintain these effects with the concentrate maneuver).'},
  ],
  description: "<p>Witches and warlocks are the spiritual slave masters of the Uthuk tribes and the keepers of their dark Ynfernael magic. Only a handful of Uthuk are born with such gifts. These children often arrive into the world amid a portentous rain of blood or storm of crimson lightning. A long and torturous road follows; not all of them live to master their magic, as the brutality of Uthuk society ruthlessly weeds out the weak. Those who survive are cruel and skilled sorcerers and priestesses, utterly dedicated to the Uthuk and without a shred of mercy for their enemies.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Giant')
adversary.update_attributes(
  brawn: 6,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 2,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 8,
  wound_th: 33,
  strain_th: 29,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 12,
  social_rating: 2,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  abilities: [
    {name: 'Giant Stomp!', description: 'Brawl attacks that giants make have a Critical rating of 3, and if they inflict Critical Injuries +40 is added to the resulting Critical Injury result.'},
    {name: 'Silhouette 3', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Daunting ([d][d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},

  ],
  weapons: [
    {
      name: 'Fists and feet',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 4},
      ]
    },
    {
      name: 'Huge club',
      skill_id: Skill.find_by_name('Melee').id,
      damage: 12,
      critical: 2,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Inaccurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
        {id: ItemQuality.find_by_name('Prepare').id, rank: 1},
      ]
    },
  ],
  description: "<p>Few have seen these huge, lumbering creatures and lived to speak of it, for their raw strength is enough to crush entire bands of soldiers like lesser beings would swat away flies. Many are as old as the hills, with reclusive natures, and immediately see outsiders as enemies. Despite the relatively small sizes of its islands, much of the Torue Albes is still unexplored, and giants can still be discovered within enormous underground caverns or wading through deep marshes and lakes in search of prey.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/0/0c/Giant.png'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Gnome Minstrel')
adversary.update_attributes(
  brawn: 1,
  agility: 2,
  intellect: 2,
  cunning: 4,
  willpower: 2,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 2,
  wound_th: 7,
  strain_th: 11,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 1,
  social_rating: 2,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 3},
    {id: Skill.find_by_name('Coordination').id, rank: 1},
    {id: Skill.find_by_name('Geography').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 1},
    {id: Skill.find_by_name('Verse').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Encouraging Song').id, rank: 0},
  ],
  abilities: [
    {name: 'Haunting melodies', description: 'Gnome minstrels may spend [ad] in a successful Charm check to inflict 1 strain on their target, and may do this multiple times.'},
    {name: 'Silhouette 0', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
  ],
  equipment: "<p>Lute, pipes, and other musical instruments, colorful garb.</p>",
  spells: [
    {name: 'Addling Tune', description: 'Make an <b>Average ([d][d]) Verse check</b>. If the check is successful, until the end of the minstrel\'s next turn, all other characters within medium range must first suffer one strain before using the concentrate maneuver. The minstrel can sustain this effect with the concentrate maneuver.'},
    {name: 'Demoralizing Stanza', description: 'Select one target within short range and make an <b>Average ([d][d]) Verse check</b>. If the check is successful, until the end of the minstrel\'s next turn, the target decreases the ability of any skill checks they make by one. The minstrel can sustain this effect with the concentrate maneuver.'},
  ],
  description: "<p>For reasons still unknown, a large number of Gnomes from across Mennara make their way to the Torue Albes each year, often in the early days of summer. They spend months traveling from island to island surviving on their wits and their skills with the lute, pipes, small drums, and their own musical voices to pay for lodgings and meals. Some of these bards display arcane abilities, leading scholars to believe their could be something in the Albes that calls to their magical nature.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/2/2e/Gnome_Minstrel.PNG/revision/latest?cb=20181212175640'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Gnomes', 'Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Lorimor Marine')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 2,
  adversary_type: 'Minion',
  soak: 4,
  wound_th: 5,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 0},
    {id: Skill.find_by_name('Cool').id, rank: 0},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
  ],
  abilities: [
    {name: 'Sea Legs', description: 'A Lorimor marine does not add [s] to checks due to unstable footing on a ship or boat, or in a similar environment.'},
  ],
  weapons: [
    {
      name: 'Saber',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Scale breastplate (+1 soak).</p>",
  description: "<p>Lorimor is known for its fleets, but while its ships are among the finest on the seas, it is the quality of the crews that really make them shine. As not all seafaring ventures are peaceful ones, the crews always include a contingent of marines ready to both repel boarders and lead assaults against enemy vessels.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/d/d2/Lorimor_Marine-0.PNG'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Lost Knight')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 2,
  cunning: 2,
  willpower: 4,
  presence: 3,
  adversary_type: 'Rival',
  soak: 5,
  wound_th: 15,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 4,
  social_rating: 3,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
    {id: Skill.find_by_name('Survival').id, rank: 1},
  ],
  abilities: [
    {name: 'Fatalistic Ferocity', description: 'A lost knight adds [su][t] to any combat checks they make.'},
  ],
  weapons: [
    {
      name: 'Ancient sword',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Heraldic plate armor (+2 soak, +1 defense).</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Manticore')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Nemesis',
  soak: 5,
  wound_th: 21,
  strain_th: 19,
  defense_melee: 1,
  defense_ranged: 0,
  combat_rating: 8,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Ranged').id, rank: 2},
  ],
  abilities: [
    {name: 'Poisonous stinger', description: 'A character wounded by a manticore\'s tail stinger must make a <b>Hard ([d][d][d]) Resilience check</b> as an out-of-turn incidental or suffer 4 additional wounds, and must check again on their next turn if the check generates [des].'},
    {name: 'Flyer', description: 'Can fly; see page 100 of Genesys CRB'},
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
  ],
  weapons: [
    {
      name: 'Claws and fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: []
    },
    {
      name: 'Tail stinger',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 7,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Slow-Firing').id, rank: 1},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 2},
      ]
    },
  ],
  description: "<p>There are many theories about how manticores came to be most assuming an artificial origin, as no natural process ca readily be imagined for how lion bodies, bat-like wings, an scorpion tails could come to exist in a single entity. Such theorie tend to involve drunken mages and spells gone horribl wrong. Unfortunately, the fierce nature of these creature means there are very few opportunities to study them.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/e/ea/Manticore.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Merriod')
adversary.update_attributes(
  brawn: 4,
  agility: 4,
  intellect: 1,
  cunning: 3,
  willpower: 1,
  presence: 1,
  adversary_type: 'Nemesis',
  soak: 5,
  wound_th: 22,
  strain_th: 18,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 9,
  social_rating: 1,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 4},
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Stealth').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  abilities: [
    {name: 'Aquatic', description: 'A merriod never treats water as difficult terrain and can breathe underwater.'},
    {name: 'Monstrous Limbs', description: 'A merriod does not increase  the difficulty of combined checks to attack with its claws, tentacles, and jaws, and may spend [ad][ad] or [tri] to hit with each additional weapon.'},
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Daunting ([d][d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},

  ],
  weapons: [
    {
      name: 'Claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
    {
      name: 'Mawed tentacles',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 3},
        {id: ItemQuality.find_by_name('Linked').id, rank: 1},
      ]
    },
    {
      name: 'Jaws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 2},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  description: "<p>Among Mennara’s aquatic beasts, few elicit as much terror and dread as the merriod. Sailors and pirates in Shellport taverns swear it is the last vengeance of a dead sea god, one who cursed the world with an avatar borne of its own unfathomable malice. Combining the jaws of the shark, the grasping tentacles of the kraken, and the cleverness of the octopus, the god unleashed the merriod into the waters.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/2/24/Merriod.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Pirate')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 2,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 5,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Cool').id, rank: 0},
    {id: Skill.find_by_name('Coordination').id, rank: 0},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
  ],
  talents: [
    {id: Talent.find_by_name('Quick Draw').id, rank: 0},
  ],
  weapons: [
    {
      name: 'Cutlass',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
      ]
    },
  ],
  description: "<p>Pirates have a dual identity. On one hand, they are reviled as despoilers and scourges who prowl the waterways, little better than thieves and murderers. On the other hand, on their voyages, they often discover new lands to be exploited and rising threats to be repelled. Despite their wicked deeds, most also operate in an honorable fashion and offer better payments and a more democratic operation than official navies. Many pirates are consummate businesspeople, inking deals to privateer or act as smugglers in exchange for amnesty. It’s a difficult balancing act, and those who cannot master it rarely remain alive for long.</p>",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Shade')
adversary.update_attributes(
  brawn: 1,
  agility: 2,
  intellect: 2,
  cunning: 2,
  willpower: 3,
  presence: 2,
  adversary_type: 'Minion',
  soak: 0,
  wound_th: 6,
  strain_th: nil,
  defense_melee: 2,
  defense_ranged: 2,
  combat_rating: 3,
  social_rating: 2,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Cool').id, rank: 0},
    {id: Skill.find_by_name('Discipline').id, rank: 0},
  ],
  abilities: [
    {name: 'Ghostly', description: 'May move over or through terrain (including doors and walls) without penalty. Halve the damage dealt to the wraith before applying soak, unless the attack came from a magical source such as a spell or magical weapon.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all of its opponents must make a <b>Hard ([d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents only make one fear check against the most terrifying enemy.'},
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
  ],
  weapons: [
    {
      name: 'Spectral hand',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 1,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Breach').id, rank: 1},
        {id: ItemQuality.find_by_name('Stun Damage').id, rank: 0},
      ]
    },
  ],
  description: "<p>None are sure why these spirits are so common in Tigh Higard. They can certainly be found in other lands, but on this island there are few cemeteries or other resting places from which scholars of such things would expect shades to rise. To walk about on certain nights is to watch echoes of the dead float across the grasses, ghastly blue-white forms that undulate despite the stillness of the air.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/d/dc/Shade.png'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Undead')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Siren')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 2,
  presence: 4,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 3,
  social_rating: 3,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Perception').id, rank: 3},
  ],
  abilities: [
    {name: 'Deadly Distraction', description: 'A siren decreases the difficulty of combat checks it makes that target a character immobilized or staggered by its Irresistible Song by one.'},
    {name: 'Flyer', description: 'Can fly; see page 100 of the Genesys Core Rulebook.'},
    {name: 'Irresistible Song', description: 'As an action, a siren may make an <b>Average ([d][d]) Charm check</b>. If the check is successful, all characters within long range who can hear the siren’s song suffer 1 strain per [su]. The siren may spend [ad][ad] to immobilize one affected character until the end of the following round, and spend [tri] to stagger one affected character until the end of the following round. While a character is staggered by Irresistible Song must spend all of their maneuvers to move closer to the siren.'},
  ],
  weapons: [
    {
      name: 'Talons',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  description: "<p>Sirens are intelligent, but are completely without mercy or culture. These cannibalistic monsters see humans and other intelligent races as nothing more than food, yet they prefer to inflict as much misery as possible on their prey. Sirens are as spiteful as they are craven, quick to flee should their victims fight back, but equally quick to return when they believe their prey to be vulnerable once more. In some rare cases, sirens have been known to pursue victims who escape back to outposts of civilization.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/0/0a/Siren.png'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Sword Poet')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 3,
  adversary_type: 'Nemesis',
  soak: 1,
  wound_th: 10,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 5,
  social_rating: 3,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Deception').id, rank: 2},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Duelist').id, rank: 0},
    {id: Talent.find_by_name('Parry').id, rank: 3},
    {id: Talent.find_by_name('Parry (Improved)').id, rank: 0},
  ],
  abilities: [
    {name: 'Show Off', description: 'A sword poet may choose to inflict stun damage when resolving a successful melee combat check or using the Improved Parry ability'},
  ],
  weapons: [
    {
      name: 'Dueling rapier',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 3},
      ]
    },
  ],
  equipment: "<p>Fancy clothes.</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Wyrm of the Deep')
adversary.update_attributes(
  brawn: 5,
  agility: 4,
  intellect: 2,
  cunning: 3,
  willpower: 2,
  presence: 4,
  adversary_type: 'Nemesis',
  soak: 9,
  wound_th: 28,
  strain_th: 24,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 12,
  social_rating: 3,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 4},
    {id: Skill.find_by_name('Discipline').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 4},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  abilities: [
    {name: 'Aquatic', description: 'A wyrm of the deep never treats water as difficult terrain and can breathe underwater.'},
    {name: 'Silhouette 4', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Daunting ([d][d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},
  ],
  weapons: [
    {
      name: 'Massive jaws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 12,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 2},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 4},
      ]
    },
    {
      name: 'Tail and fins',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 10,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  description: "<p>Mennara is known for its many dragonkin that roam over its varied terrain. While most possess deep and ancient wisdoms, some have seemingly devolved into little more than beasts of hunger and fury. The fathomless seas that lay beyond the shores of Lorimor and the Torue Albes are home to one such strain, gigantic creatures that prey on seagoing vessels all along the western coast. Many sightings are reported even in the well-traveled southern waters separating Al-Kalim and Zanaga, a truly frightening occurrence for passengers. Experienced sailors, though, know that the true terrors are the gods that also live in the seas, and would rather face a wyrm any day.</p>",
  image_url: 'https://imgv2-1-f.scribdassets.com/img/document/183717583/original/873b4aa954/1556946910?v=1'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Dragonkin')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Ice Wyrm')
adversary.update_attributes(
  brawn: 5,
  agility: 4,
  intellect: 3,
  cunning: 3,
  willpower: 3,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 8,
  wound_th: 31,
  strain_th: 20,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 13,
  social_rating: 5,
  general_rating: 6,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 4},
    {id: Skill.find_by_name('Coercion').id, rank: 4},
    {id: Skill.find_by_name('Cool').id, rank: 3},
    {id: Skill.find_by_name('Discipline').id, rank: 3},
    {id: Skill.find_by_name('Ranged').id, rank: 4},
    {id: Skill.find_by_name('Resilience').id, rank: 4},
    {id: Skill.find_by_name('Stealth').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Flyer', description: 'Can fly; see page 100 of Genesys CRB'},
    {name: 'Silhouette 4', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Camouflaged Scales', description: 'An ice wyrm adds [b][b] to Stealth checks in icy or snowy surroundings.'},
  ],
  weapons: [
    {
      name: 'Freezing blood torrent',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 10,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Blast').id, rank: 8},
        {id: ItemQuality.find_by_name('Concussive').id, rank: 1},
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 4},
      ]
    },
    {
      name: 'Claws and fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 8,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 3},
      ]
    },
  ],
  description: "<p>Ice wyrms, like their dragon ancestors, are deeply dangerous foes. Conversant in many dark tongues, these beings have enslaved or bargained with most of the monstrous races of the north, enlisting them in their unending war against the remnants of the Salishwyrd. Many grow great wings of cold leather, and silently glide over the glaciers in search of prey. Some ice wyrms even practice sorcery and are able to spit spells as well as streams of freezing blood. Scarsha the Vile, Queen of the ice wyrms, is perhaps the most deadly of her kind. Heroes who wander into the far north are likely to find themselves drawing her gaze, whether as potential adversaries, prey, or tools in her hunt for the Green Vale.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/0/0a/Ice_Wyrm.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Dragonkin')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Ice-Blood Warrior')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 4,
  wound_th: 5,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 0},
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
    {id: Skill.find_by_name('Resilience').id, rank: 0},
  ],
  abilities: [
    {name: 'Ice Madness', description: 'Upgrade the difficulty of social skill checks targeting an ice-blood warrior once.'},
  ],
  weapons: [
    {
      name: 'Brittle axe',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
    {
      name: 'Freezing Touch',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Stun').id, rank: 3},
      ]
    },
  ],
  description: "<p>The White Death is an insidious effect of the Rime Storm that can claim those unfortunates who traverse Isheim’s northern wilds. For some, the White Death merely kills, leaving their frozen corpses contorted in their final moments of death. For others, it can be even worse, transmuting their once-warm blood into icy fluid. Their veins filled with sorcerous cold, they become sadistic killers, preying upon any being or creature that crosses their path. Raging and cursing, these frozen beings are drawn to the warmth of hot-blooded creatures so they might quench the heat with their eternal cold. Fighting off such adversaries is a terrible struggle; even their touch can blacken flesh with frostbite.</p>",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Wendigo')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 1,
  cunning: 3,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 6,
  wound_th: 18,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 7,
  social_rating: 2,
  general_rating: 4,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 4},
    {id: Skill.find_by_name('Brawl').id, rank: 4},
    {id: Skill.find_by_name('Coercion').id, rank: 4},
    {id: Skill.find_by_name('Resilience').id, rank: 4},
    {id: Skill.find_by_name('Stealth').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  abilities: [
    {name: 'Blood Frenzy', description: 'While at least one opponent in an encounter is suffering a Critical Injury, a Wendigo increases the base damage of its attacks by 2.'},
    {name: 'Eyeless Sight', description: 'Wendigo do not add [s] to checks due to darkness or concealment, and are immune to abilities or Critical Injuries that affect a creature’s sight.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Daunting ([d][d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},
  ],
  weapons: [
    {
      name: 'Fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
      ]
    },
    {
      name: 'Claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
      ]
    },
  ],
  description: "<p>Wendigo are also creations of the Rime Storm. According to the tales, they were once the feline mounts of the Salishwyrd, back when the north encompassed green forests and open plains. Then, the Rime Storm infected them with icy magic. Their eyes shrank away to nothing, and a hunger as keen as the howling tundra wind filled their bellies. It was not long before they became a terror of the north, their chilling cries sending sensible creatures scurrying for their dens and wise individuals reaching for their weapons. Eyeless, these creatures bite the air to gain a sense of their prey; they can pinpoint a target’s location far swifter than the keenest of hunters.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/2/27/Wendigo.PNG'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Onoit Shaman')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 3,
  presence: 3,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 8,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 5,
  social_rating: 3,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Leadership').id, rank: 2},
    {id: Skill.find_by_name('Melee').id, rank: 2},
    {id: Skill.find_by_name('Lore').id, rank: 3},
    {id: Skill.find_by_name('Primal').id, rank: 3},
    {id: Skill.find_by_name('Survival').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  abilities: [
    {name: 'Silhouette 0', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
  ],
  weapons: [
    {
      name: 'Hatchet',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Oaken staff (add +4 damage to magic attacks; the first Range effect added to a spell does not increase its difficulty), bark cloak (+1 defense).</p>",
  spells: [
    {name: 'Winter Gale', description: 'Choose one target at short or medium range and make a <b>Hard ([d][d][d]) Primal check</b>. If successful, this magic attack inflicts 7 damage +1 damage per [su], with the Blast 3 and Ensnare 3 item qualities.'},
    {name: 'Healing Gust', description: 'Choose two targets at up to short range and make an <b>Average ([[d][d]) Primal check</b>. If successful, the targets heal 1 wound per [su] and 1 strain per [ad]; the shaman may affect additional targets by spending [ad] per target.'},
  ],
  description: "<p>The diminutive Onoit shamans are one of Isheim’s many mysteries. Ostensibly, they are part of the nomadic Gnomish tribes. Clad in cloaks of bark and moss, they are frequently seen moving among their people, healing and dispensing wisdom. Yet, they often travel alone in their sled-tents, drawn across the tundra by packs of trusty Ulfen. A society unto themselves, the Onoit shamans serve as guardians of the lost Salishwyrd empire. When the Elves retreated to the Green Vale, they entrusted the keeping of their abandoned cities and tombs to the Onoit shamans, granting each one a spark of elemental magic to light their way. Since that time, those sparks have been passed from shaman to shaman.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Spellcaster', 'Gnomes')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Weik Warrior')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 3,
  presence: 2,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 6,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 0,
  combat_rating: 3,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 0},
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
    {id: Skill.find_by_name('Resilience').id, rank: 0},
  ],
  abilities: [
    {name: 'Aggressive Fighter', description: 'Weik warriors add [ad] to combined checks they make to attack with two melee weapons.'},
  ],
  weapons: [
    {
      name: 'Axe',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
    {
      name: 'Sword',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
      ]
    },
    {
      name: 'Bow',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 7,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Unwieldy').id, rank: 2},
      ]
    },
  ],
  description: "<p>The rugged peoples of the north are as fearsome as the land they call home. Known as the Weik, they have carved out a civilization on the shores of Isheim and won a reputation as daring seafarers, skilled game hunters, and deadly raiders. Their warriors stride into battle with a blade or axe in each hand, wild grins upon their faces—for in the culture of the Weik, there can be no greater honor than to die by the sword. Cold and hardship mean little to them, and they look upon the Human knights and soldiers of Terrinoth as weakling southerners. Some Weik even travel to these warmer lands in search of greater adventure or to hunt a foe who dares think they can escape the wrath of a Weik who has been wronged.</p>",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Assassin')
adversary.update_attributes(
  brawn: 2,
  agility: 4,
  intellect: 3,
  cunning: 4,
  willpower: 2,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 2,
  wound_th: 12,
  strain_th: 14,
  defense_melee: 4,
  defense_ranged: 3,
  combat_rating: 9,
  social_rating: 3,
  general_rating: 6,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Coordination').id, rank: 3},
    {id: Skill.find_by_name('Deception').id, rank: 3},
    {id: Skill.find_by_name('Medicine').id, rank: 2},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 2},
    {id: Skill.find_by_name('Perception').id, rank: 2},
    {id: Skill.find_by_name('Ranged').id, rank: 2},
    {id: Skill.find_by_name('Skulduggery').id, rank: 3},
    {id: Skill.find_by_name('Stealth').id, rank: 4},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Backstab').id, rank: 0},
    {id: Talent.find_by_name('Precision').id, rank: 0},
  ],
  abilities: [
    {name: 'Lightning Draw', description: 'May draw or sheathe a dagger or similar small weapon as an incidental; there is no limit to how many weapons an assassin can ready this way per turn.'},
    {name: 'Poisoner', description: 'As an incidental, may apply poison to all weapons. A character wounded by a poisoned weapon must make a <b>Hard ([d][d][d]) Resilience check</b> as an out-of-turn incidental or suffer 4 additional wounds, and must check again on their next turn if the check generates [des].'},
    {name: 'Vanish', description: 'After performing a check, may spend [ad][ad] or [tri] to hide from all other characters in the encounter.'},
  ],
  weapons: [
    {
      name: 'Scimitar',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
      ]
    },
    {
      name: 'Two Katars',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
      ]
    },
    {
      name: 'Three Daggers',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 5,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Limited Ammo').id, rank: 3},
      ]
    },
    {
      name: 'Bow',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 7,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Unwieldy').id, rank: 2},
      ]
    },
  ],
  equipment: "<p>A variety of concealing cloaks.</p>",
  description: "<p>The assassins of the Hysshari Brotherhoods are as deadly shadows that stalk the courts of Al-Kalim. These cults of murder serve the Caliph and their viziers and train their assassins in hidden mountain temples and oasis fortresses. At the Tower of Knives, which hangs like a dagger from the cavern roof of Haruun, acrobatic killers master the arts of wall walking. In the Valley of Ash, Brotherhood members train with mysterious bladewomen to learn the secrets of silent killing and night fighting.</p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Djinn')
adversary.update_attributes(
  brawn: 4,
  agility: 3,
  intellect: 4,
  cunning: 5,
  willpower: 5,
  presence: 5,
  adversary_type: 'Nemesis',
  soak: 6,
  wound_th: 25,
  strain_th: 25,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: 12,
  social_rating: 12,
  general_rating: 10,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 5},
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Charm').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 3},
    {id: Skill.find_by_name('Deception').id, rank: 4},
    {id: Skill.find_by_name('Discipline').id, rank: 4},
    {id: Skill.find_by_name('Lore').id, rank: 4},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Negotiation').id, rank: 4},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 3},
    {id: Talent.find_by_name('Ruinous Repartee').id, rank: 0},
  ],
  abilities: [
    {name: 'Airborne', description: 'Can fly; see page 100 of the Genesys Core Rulebook.'},
    {name: 'Shapeshifter', description: 'Once per round as an incidental, a Djinn may increase or decrease its silhouette by 1 and change its appearance to that of any character or creature of its current silhouette.'},
  ],
  weapons: [
    {
      name: 'Conjured scimitar',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 9,
      critical: 2,
      range: 'Engaged',
      qualities: [
      ]
    },
    {
      name: 'Illusionary claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  equipment: "<p>Djinni love to meddle in the affairs of mortals, promising riche to those who do their bidding and pretending to be an agen of the Caliph or other lord to sow discord from afar. It is no without cause that many tales of Al-Kalim warn against treatin with them. All are skilled sorcerers, but even should their magi fail them, Djinni can take on gigantic forms to dispatch foe with great falchions or daggerlike claws.</p>",
  spells: [
    {name: 'Cyclone', description: 'The Djinn chooses one target at up to medium range and make a <b>Hard ([d][d][d]) Arcana check</b>. If successful, this magic attack inflicts 4 damage +1 damage per [su], with the Disorient 4 and Knockdown qualities. The Djinn can also spend [ad] to move the target up to one range band in any direction.'},
    {name: 'Twisted Wish', description: 'The Djinn chooses one target within short range and make a <b>Daunting ([d][d][d][d]) Arcana check</b>. If successful, until the end of the Djinn’s next turn, each time the target makes a skill check, decrease the ability by one and the Djinn may choose to change any die in the pool not displaying [tri] or [des] to any other face. The Djinn may sustain these effects with the concentrate maneuver.'},
  ],
  description: "<p></p>",
  image_url: 'https://i.pinimg.com/originals/35/c5/fd/35c5fd10d7b676dd7edcaf8e454cabd1.png'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Minor Flame Elemental')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 3,
  presence: 1,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 13,
  strain_th: nil,
  defense_melee: 4,
  defense_ranged: 4,
  combat_rating: 5,
  social_rating: 2,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  abilities: [
    {name: 'Amorphous', description: 'As an incidental, a minor elemental may increase or decrease its silhouette by 1.'},
    {name: 'Inconspicuous', description: 'a minor elemental in a natural environment appears indistinguishable from an ordinary example of its element; a character can make a Hard ([d][d][d]) Perception or Vigilance check to identify a minor elemental.'},
  ],
  weapons: [
    {
      name: 'Pseudopods',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Linked').id, rank: 1},
        {id: ItemQuality.find_by_name('Burn').id, rank: 1},
      ]
    },
  ],
  description: "<p>Elemental magic, a legacy of the Djinni, runs through the bedrock of Al-Kalim. These vestiges of trapped sorcery tend to draw together to manifest as living things, gathering together like droplets of water might form a shimmering pool or lake. The elemental beings thus created may roam as lone predators, but they are just as likely to group together to form entire regions where the laws of nature no longer apply. Either way, they exist to feed upon mortals, whether drinking the water from their flesh, burning their blood for fuel, or devouring the breath from their lungs.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elemental')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Minor Quicksand Elemental')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 3,
  presence: 1,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 13,
  strain_th: nil,
  defense_melee: 4,
  defense_ranged: 4,
  combat_rating: 5,
  social_rating: 2,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  abilities: [
    {name: 'Amorphous', description: 'As an incidental, a minor elemental may increase or decrease its silhouette by 1.'},
    {name: 'Inconspicuous', description: 'a minor elemental in a natural environment appears indistinguishable from an ordinary example of its element; a character can make a Hard ([d][d][d]) Perception or Vigilance check to identify a minor elemental.'},
  ],
  weapons: [
    {
      name: 'Pseudopods',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Linked').id, rank: 1},
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 1},
      ]
    },
  ],
  description: "<p>Elemental magic, a legacy of the Djinni, runs through the bedrock of Al-Kalim. These vestiges of trapped sorcery tend to draw together to manifest as living things, gathering together like droplets of water might form a shimmering pool or lake. The elemental beings thus created may roam as lone predators, but they are just as likely to group together to form entire regions where the laws of nature no longer apply. Either way, they exist to feed upon mortals, whether drinking the water from their flesh, burning their blood for fuel, or devouring the breath from their lungs.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elemental')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Minor Spring Elemental')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 3,
  presence: 1,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 13,
  strain_th: nil,
  defense_melee: 4,
  defense_ranged: 4,
  combat_rating: 5,
  social_rating: 2,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  abilities: [
    {name: 'Amorphous', description: 'As an incidental, a minor elemental may increase or decrease its silhouette by 1.'},
    {name: 'Inconspicuous', description: 'a minor elemental in a natural environment appears indistinguishable from an ordinary example of its element; a character can make a Hard ([d][d][d]) Perception or Vigilance check to identify a minor elemental.'},
  ],
  weapons: [
    {
      name: 'Pseudopods',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Linked').id, rank: 1},
        {id: ItemQuality.find_by_name('Stun').id, rank: 5},
      ]
    },
  ],
  description: "<p>Elemental magic, a legacy of the Djinni, runs through the bedrock of Al-Kalim. These vestiges of trapped sorcery tend to draw together to manifest as living things, gathering together like droplets of water might form a shimmering pool or lake. The elemental beings thus created may roam as lone predators, but they are just as likely to group together to form entire regions where the laws of nature no longer apply. Either way, they exist to feed upon mortals, whether drinking the water from their flesh, burning their blood for fuel, or devouring the breath from their lungs.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Elemental')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Scorpion Swarm')
adversary.update_attributes(
  brawn: 1,
  agility: 3,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Rival',
  soak: 1,
  wound_th: 36,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Coordination').id, rank: 0},
  ],
  abilities: [
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Swarm', description: 'Halve the damage dealt to the swarm before applying soak, unless the weapon has the Blast or Burn quality (regardless of whether the quality is activated).'},
    {name: 'Venomous', description: 'Enemies who are wounded by a scorpion swarm must make a <b>Hard ([d][d][d]) Resilience check</b> as an out-of-turn incidental or suffer 4 additional wounds, and must check again on their next turn if the check generates [des].'},
  ],
  weapons: [
    {
      name: 'Claws and stingers',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 1,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 3},
        {text: 'May spend [ad][ad] to inflict 3 strain.'},
      ]
    },
  ],
  description: "<p>There are many dangers in the desert, but few will drive traveler to turn and run like the sight of a hundreds of scorpion rising out of a dune. The entire swarm acts as a single being each creature attacking in perfect concert like a terrible wav of death. Those who unknowingly disturb a swarm all to often end up yet another pile of bones, buried in the blowin sands and forgotten by all.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Thieves\' Guild Cutpurse')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 2,
  presence: 2,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 2,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Coordination').id, rank: 2},
    {id: Skill.find_by_name('Deception').id, rank: 2},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 2},
    {id: Skill.find_by_name('Skulduggery').id, rank: 3},
    {id: Skill.find_by_name('Stealth').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Finesse').id, rank: 0},
  ],
  weapons: [
    {
      name: 'Dagger',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 3,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
      ]
    },
    {
      name: 'Blackjack',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 5,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 2},
        {id: ItemQuality.find_by_name('Stun Damage').id, rank: 0},
      ]
    },
  ],
  description: "<p>The thieves’ guild holds great power in Al-Kalim, and its cutpurses are everywhere within the Caliphate. They come in many forms, from filthy street urchins and beggars feigning debilitating injuries to well-dressed merchants and offspring of the nobility, as the strength of a good thief is often the ability to get close to a target without raising suspicions. These nefarious robbers are most often found in taverns and markets where crowds are at their thickest, although daring individuals can operate wherever there is wealth for the taking. The guild takes a dim view of “freelancers” in its territory, so most cutpurses are accounted for. They are marked with the guild’s tattoo and must tithe a measure of their take to the local den bosses.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Carnivorous Flora')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 4},
  ],
  abilities: [
    {name: 'Drag', description: 'A carnivorous flora can use a maneuver to move a target affected by its thorny vines’ Ensnare quality to engaged range.'},
    {name: 'Inconspicuous', description: 'A carnivorous flora in a natural environment appears indistinguishable from an ordinary plant; a character can make a <b>Hard ([d][d][d]) Perception</b> or <b>Vigilance check</b> to identify a carnivorous flora'},
    {name: 'Rooted', description: 'A carnivorous flora cannot perform maneuvers to move.'},
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Unexpected Attack', description: 'A carnivorous flora that has not been yet been identified as a threat uses Stealth to determine initiative.'},
  ],
  weapons: [
    {
      name: 'Thorny vines',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 4,
      critical: 4,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Auto-Fire').id, rank: 0},
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 3},
      ]
    },
    {
      name: 'Snapping jaws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 8,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  description: "<p>There are many kinds of carnivorous flora in Zanaga, like the violet talon, which glows in the dark to lure its prey into its clutches; the traveler’s folly, which mimics cries for help; and the karko noose, which hangs down from trees to snatch up victims as they walk below. Those caught in the grip of these plants have but a short time to free themselves as paralytic toxins assail them.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Giant Snake')
adversary.update_attributes(
  brawn: 4,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 4,
  wound_th: 14,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 0,
  combat_rating: 4,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 2},
  ],
  abilities: [
    {name: 'Silhouette 3', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Swallow', description: 'May make an opposed Brawl check to swallow an engaged target. If the check is successful, swallowed characters are immobilized and suffer 6 damage at the start of each round but can escape if the snake is killed or by making a successful <b>Hard ([d][d][d]) Athletics check</b>.'},
  ],
  weapons: [
    {
      name: 'Jaws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
    {
      name: 'Constricting coils',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 10,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 4},
        {id: ItemQuality.find_by_name('Stun').id, rank: 4},
      ]
    },
  ],
  description: "<p>The jungles of Zanaga host a great many types of snakes, and the larger they are the more deadly they become. Some seem to have no upper limit on their size, and tales speak of entire groups of adventurers swallowed up in fanged, cavernous maws.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Makhim')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Minion',
  soak: 4,
  wound_th: 6,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 3,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Ranged').id, rank: 0},
    {id: Skill.find_by_name('Survival').id, rank: 0},
    {id: Skill.find_by_name('Stealth').id, rank: 0},
    {id: Skill.find_by_name('Vigilance').id, rank: 0},
  ],
  abilities: [
    {name: 'Four-armed', description: 'A Makhim does not increase the difficulty of combined checks to attack with both its dolochs and fists.'},
  ],
  weapons: [
    {
      name: 'Dolochs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Linked').id, rank: 1},
      ]
    },
    {
      name: 'Fists',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 4,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Linked').id, rank: 1},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
    {
      name: 'Sling',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 4,
      critical: 4,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 2},
        {id: ItemQuality.find_by_name('Prepare').id, rank: 1},
      ]
    },
  ],
  description: "<p>Makhim warriors are as fierce as any in Zanaga, and they have adapted to the environments they fight in. Most employ weapons that only a four-armed fighter could wield, like the dagger-like dolochs they wear a gauntlets. They also employ leather slings which they twist and swing to send a missile flying with tremendous momentum. Makhim are seldom encountered alone; they usually travel in great war parties, some scouting ahead, others staying to the shadows. At first glance, their opponents might believe they face but a few of the reptilian creatures, at least until the jungle comes alive with a cacophony of hissing war cries.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/b/b4/Mahkim.PNG'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Naga Priestess')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 3,
  presence: 4,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 16,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 4,
  social_rating: 5,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Charm').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 3},
    {id: Skill.find_by_name('Coordination').id, rank: 4},
    {id: Skill.find_by_name('Leadership').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
  ],
  abilities: [
    {name: 'Hypnotic Gaze', description: 'A naga priestess may make an opposed Charm vs. Discipline check as an action targeting one character within short range. If successful, the target is immobilized for 1 round per [su], and the naga may spend [tri] to stagger the target for 1 round.'},
    {name: 'Opportunistic Predator', description: 'When making a combat check targeting an immobilized character, the naga deals +2 damage per [su] instead of +1.'},
  ],
  weapons: [
    {
      name: 'Fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 1},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
        {text: 'A character wounded by this  weapon must make a <b>Hard ([d][d][d]) Resilience check</b> or suffer 4 additional wounds, and must check again on their next turn if the check generates [des].'}
      ]
    },
  ],
  description: "<p>Naga priestesses are the leaders of the naga serpent cults and blessed with the power of their Primal God, Set. The coils of their long, snakelike lower bodies constantly shift in pattern and color, while their slit eyes bore into the minds of any being foolish enough to meet their gaze. Even the sternest warrior might become hypnotized by a priestess and powerless as she moves to sink her venomous fangs into their throat. If the priestess chooses, she can even turn her enemies against each other, the weak-minded so enraptured by the naga that they throw themselves at anyone who might bring her harm.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/3/35/Naga.png'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Singhara Hunter')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 1,
  cunning: 3,
  willpower: 2,
  presence: 2,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 5,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 3,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 0},
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
    {id: Skill.find_by_name('Ranged').id, rank: 0},
    {id: Skill.find_by_name('Survival').id, rank: 0},
    {id: Skill.find_by_name('Stealth').id, rank: 0},
  ],
  talents: [
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Pounce', description: 'May spend [ad][ad] from an initiative check to perform a free maneuver before the first round of combat.'},
  ],
  weapons: [
    {
      name: 'Spear',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
      ]
    },
    {
      name: 'Spear',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 6,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Limited Ammo').id, rank: 1},
      ]
    },
    {
      name: 'Claws and teeth',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 5,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  description: "<p>The feline ancestry of the Singhara makes them excellent hunters. In the darkness of night, when their night sight gives them an edge over their prey, they move swiftly and silently across the plains and through the jungles. The favored weapon of these hunters is the lion spear, a shaft as long as a warrior is tall, tipped with a sharpened sabrecat tooth. In the hands of a Singhara hunter, such a spear can punch through toughened hide or shell and be launched into the air like a bolt of bone lightning to strike down targets hundreds of paces distant.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/f/fe/Singhara.jpg'
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Catfolk')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Singhara Pridelord')
adversary.update_attributes(
  brawn: 5,
  agility: 4,
  intellect: 2,
  cunning: 3,
  willpower: 3,
  presence: 4,
  adversary_type: 'Nemesis',
  soak: 5,
  wound_th: 20,
  strain_th: 16,
  defense_melee: 3,
  defense_ranged: 3,
  combat_rating: 11,
  social_rating: 3,
  general_rating: 3,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 4},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Ranged').id, rank: 3},
    {id: Skill.find_by_name('Survival').id, rank: 3},
    {id: Skill.find_by_name('Stealth').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
    {id: Talent.find_by_name('Swift').id, rank: 0},
  ],
  abilities: [
    {name: 'Prideful Roar', description: 'As a maneuver, a Pridelord can unleash a prideful roar. All other Singhara in the encounter remove [s] from checks until the end of the Pridelord’s next turn. In addition, enemies within medium range of the Pridelord must make an <b>Average ([d][d]) fear check</b> as per page 243 of the Genesys Core Rulebook.'},
  ],
  weapons: [
    {
      name: 'Spear',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 8,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
      ]
    },
    {
      name: 'Spear',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 8,
      critical: 3,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Limited Ammo').id, rank: 1},
      ]
    },
    {
      name: 'Claws and teeth',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  description: "<p>Pridelords are the largest and fiercest of the Singhara warriors. Powerfully muscled, they stand head and shoulders above even the tallest of Humans. Adding to their grandeur are their great manes, which hang like cloaks down their backs, the size and thickness a mark of a war chief ’s status among their people. Armed with spear, club, tooth, and claw, the Pridelord is always at the forefront of the hunt or where battle is at its thickest, for it is the Singhara way to lead by example. As a mark of this bravery, a Pridelord’s body is often covered in scars, each one worn proudly to tell a tale of the chief ’s life as a warrior and veteran of war.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth-world/images/f/fe/Singhara.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Catfolk')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Brigand Leader')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 2,
  adversary_type: 'Rival',
  soak: 4,
  wound_th: 14,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Leadership').id, rank: 1},
    {id: Skill.find_by_name('Ranged').id, rank: 2},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 2},
  ],
  weapons: [
    {
      name: 'Axe',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
    {
      name: 'Bow',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 7,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Unwieldy').id, rank: 2},
      ]
    },
    {
      name: 'Shield',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 3,
      critical: 6,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
        {id: ItemQuality.find_by_name('Deflection').id, rank: 1},
        {id: ItemQuality.find_by_name('Inaccurate').id, rank: 2},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>Padded Armor (+1 soak).</p>",
  description: "<p>The most charismatic or ruthless brigands often naturally assume a leadership role. Although no more eager to die than any other bandit, some fear losing face more than an enemey’s blade.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('The Haunted City')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Brigand')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 4,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: 2,
  social_rating: 1,
  general_rating: 1,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 0},
    {id: Skill.find_by_name('Ranged').id, rank: 0},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
  ],
  weapons: [
    {
      name: 'Mace',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 4,
      range: 'Engaged',
      qualities: [
      ]
    },
  ],
  equipment: "<p></p>",
  description: "<p>Most brigands are common folk driven to banditry through desperation.</p>",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('The Haunted City')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Eliza Farrow')
adversary.update_attributes(
  brawn: 4,
  agility: 4,
  intellect: 4,
  cunning: 4,
  willpower: 4,
  presence: 5,
  adversary_type: 'Nemesis',
  soak: 6,
  wound_th: 18,
  strain_th: 20,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 3},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Charm').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 3},
    {id: Skill.find_by_name('Discipline').id, rank: 3},
    {id: Skill.find_by_name('Forbidden').id, rank: 4},
    {id: Skill.find_by_name('Negotiation').id, rank: 3},
    {id: Skill.find_by_name('Ranged').id, rank: 3},
    {id: Skill.find_by_name('Riding').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
    {id: Talent.find_by_name('Dark Insight').id, rank: 0},
  ],
  abilities: [
    {name: 'Blood Call', description: 'When Eliza Farrow damages a target using her fangs or a magic attack, she heals wounds equal to the wounds inflicted.'},
    {name: 'Blood Mist', description: 'If Eliza Farrow suffers damage in excess of her Wound Threshold, she is not incapacitated, but takes the form of a cloud of blood mist. While in this form, she can fly (see page 100 of the Genesys Core Rulebook) and does not suffer damage from physical attacks. If she suffers additional damage from a magical attack, she becomes incapacitated as normal and resumes corporeal form.'},
    {name: 'Dominate', description: 'May use the Dominate action once per encounter, making an opposed Charm vs. Discipline check targeting one character in short range; if successful, the target is immobilized for 1 round per uncanceled [su], and Eliza may spend [tri] to stagger the target for 1 round.'},
    {name: 'Sunlight Sensitivity', description: 'While exposed to sunlight, Eliza Farrow reduces all her characteristics by 2 and halves her Wound Threshold and Strain Threshold.'},
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink (except blood), and can survive underwater; immune to poisons and toxins.'},
    {name: 'Vampiric Magic', description: 'Eliza Farrow reduces the difficulty of all magic skill checks one step.'},
  ],
  weapons: [
    {
      name: 'Fangs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 6,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Ensnare').id, rank: 1},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 2},
      ]
    },
  ],
  equipment: "<p></p>",
  spells: [
    {name: 'Blood Funnel', description: 'Choose one target at short range for the attack and make a <b>Hard ([d][d][d]) Arcana check</b>; if the magic combat check succeeds, the target suffers 4 damage + 1 damage per uncanceled [su], with Critical Rating 2 and the Blast 4 and Vicious 4 qualities.'},
    {name: 'Curse of the Night', description: 'Choose one target within short range and make a <b>Hard ([d][d][d]) Arcana check</b>; if the check succeeds, the target decreases the ability of any skill checks they make by one and reduce their strain and wound thresholds by 4 until the end of Eliza Farrow’s next turn; she may maintain these effects by performing the Concentrate maneuver.'},
  ],
  description: "<p>The impossibly beautiful Eliza Farrow has been known to those of noble circles for far longer than her youthful complexion would indicate. Despite various dark rumors surrounding her life, few realize that Eliza Farrow truly is a vampire, a monstrous undead creature that feeds on the blood of living humans and the other intelligent races. Farrow is charming, calm, and collected, but her vicious side can reveal itself in a moment.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/9/99/ElizaFarrow.jpg'
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('The Haunted City', 'Undead', 'Vampire')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Farrow\'s Guard')
adversary.update_attributes(
  brawn: 3,
  agility: 3,
  intellect: 2,
  cunning: 2,
  willpower: 3,
  presence: 2,
  adversary_type: 'Rival',
  soak: 4,
  wound_th: 13,
  strain_th: nil,
  defense_melee: 2,
  defense_ranged: 1,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 1},
    {id: Skill.find_by_name('Ranged').id, rank: 2},
    {id: Skill.find_by_name('Riding').id, rank: 1},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 1},
  ],
  talents: [
    {id: Talent.find_by_name('Quick Draw').id, rank: 0},
  ],
  abilities: [
    {name: 'Grapple', description: 'May take the Grapple incidental; until the start of the guard’s next turn, enemies must spend two maneuvers to disengage.'},
    {name: 'Stay Back!', description: 'When a character within short range attempts to move past the guard, the guard may, as an out-of-turn incidental, spend a Story Point from the GM pool and knock the character prone.'},
  ],
  weapons: [
    {
      name: 'Sword',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 6,
      critical: 2,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 0},
      ]
    },
    {
      name: 'Crossbow',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 7,
      critical: 2,
      range: 'Ranged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 2},
        {id: ItemQuality.find_by_name('Prepare').id, rank: 1},
      ]
    },
    {
      name: 'Shield',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 3,
      critical: 6,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
        {id: ItemQuality.find_by_name('Deflection').id, rank: 1},
        {id: ItemQuality.find_by_name('Inaccurate').id, rank: 2},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>Leather armor (+1 soak).</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('The Haunted City')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Coachman')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 10,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 1},
    {id: Skill.find_by_name('Ranged').id, rank: 1},
    {id: Skill.find_by_name('Riding').id, rank: 2},
    {id: Skill.find_by_name('Vigilance').id, rank: 2},
  ],
  weapons: [
    {
      name: 'Dagger',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 3,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Padded armor (+1 soak).</p>",
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('The Haunted City')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'City Guard')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 2,
  cunning: 2,
  willpower: 2,
  presence: 2,
  adversary_type: 'Rival',
  soak: 4,
  wound_th: 13,
  strain_th: nil,
  defense_melee: 2,
  defense_ranged: 1,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Discipline').id, rank: 1},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 2},
    {id: Skill.find_by_name('Perception').id, rank: 1},
    {id: Skill.find_by_name('Vigilance').id, rank: 1},
  ],
  weapons: [
    {
      name: 'Militia Spear',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
      ]
    },
    {
      name: 'Shield',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 3,
      critical: 6,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
        {id: ItemQuality.find_by_name('Deflection').id, rank: 1},
        {id: ItemQuality.find_by_name('Inaccurate').id, rank: 2},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  equipment: "<p>Leather armor (+1 soak).</p>",
  description: "<p>Nerekhall’s City Watch maintains strict discipline, equal to that of any Free City. Each guard knows of Nerekhall’s dark past, and that they must be ready to guard against the danger within as readily as that without.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('The Haunted City')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Ironbound')
adversary.update_attributes(
  brawn: 4,
  agility: 2,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Rival',
  soak: 6,
  wound_th: 15,
  strain_th: nil,
  defense_melee: 3,
  defense_ranged: 2,
  combat_rating: 7,
  social_rating: 2,
  general_rating: 2,
  skills: [
    {id: Skill.find_by_name('Discipline').id, rank: 3},
    {id: Skill.find_by_name('Melee (Heavy)').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 3},
    {id: Skill.find_by_name('Vigilance').id, rank: 3},
  ],
  abilities: [
    {name: 'Graven wards', description: 'Increase the difficulty of all spells that target an ironbound twice.'},
    {name: 'Strength of Iron', description: 'An ironbound can wield a Melee (Heavy) weapon in one hand.'},
    {name: 'Watchful', description: 'Ironbound add [b][b] to Perception and Vigilance checks to detect the use or effects of Magic skills.'},
  ],
  weapons: [
    {
      name: 'Halberd',
      skill_id: Skill.find_by_name('Melee (Heavy)').id,
      damage: 7,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 1},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 3},
      ]
    },
    {
      name: 'Large shield',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 5,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Defensive').id, rank: 2},
        {id: ItemQuality.find_by_name('Deflection').id, rank: 2},
        {id: ItemQuality.find_by_name('Inaccurate').id, rank: 2},
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
      ]
    },
  ],
  equipment: "<p></p>",
  description: "<p>The magically animated constructs called Ironbound are a pretty common sight in Nerekhall, standing still as statues at street corners or sometimes even patrolling the city. Ironbound can detect the presence of magical energies, and can usually distinguish the forbidden magic that is their charge. Still, Ironbound tend to err on the side of arrest where unfamiliar magic and unfamiliar practitioners are concerned.</p>",
  image_url: 'https://vignette.wikia.nocookie.net/terrinoth/images/7/70/Nerekhall_Ironbound_Guard.jpg'
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('The Haunted City', 'Construct')
adversary.save

adversary = Adversary.find_or_create_by(name: 'Danne Bulvert')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 2,
  cunning: 3,
  willpower: 1,
  presence: 2,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Coordination').id, rank: 2},
    {id: Skill.find_by_name('Deception').id, rank: 2},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 2},
    {id: Skill.find_by_name('Skulduggery').id, rank: 3},
    {id: Skill.find_by_name('Stealth').id, rank: 3},
  ],
  weapons: [
    {
      name: 'Dagger',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 3,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
      ]
    },
    {
      name: 'Blackjack',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 5,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Disorient').id, rank: 2},
        {id: ItemQuality.find_by_name('Stun Damage').id, rank: 0},
      ]
    },
  ],
  equipment: "<p></p>",
  description: "<p>Danne Bulvert is a small-time thief who’s relatively well known among Nerekhall’s seedier elements. Word has gotten around that Bulvert had a strange experience recently, and he’s now laying lower than usual. According to some rumors, before his sudden change in behavior, Bulvert had his eye on a lucrative prospect—the purse of a noblewoman who recently arrived in town.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('The Haunted City', 'Construct', 'Thief')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Magistrate Edmin Cawl')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 3,
  cunning: 3,
  willpower: 3,
  presence: 3,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Charm').id, rank: 1},
    {id: Skill.find_by_name('Coercion').id, rank: 2},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Deception').id, rank: 1},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Negotiation').id, rank: 2},
  ],
  weapons: [
  ],
  equipment: "<p>Fine clothing, sash of office.</p>",
  description: "<p>A thin-faced man of noble bearing, Cawl is sincere in his conviction to protect Nerekhall’s interests and his loathing for dark magic and those who would practice such. However, his dislike for those who violate the law or act as vigilantes is only slightly less.</p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('The Haunted City', 'Government')
adversary.save
puts "Adversary: #{adversary.name}"


adversary = Adversary.find_or_create_by(name: 'Mavaris Skain, Necromancer')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 4,
  cunning: 3,
  willpower: 3,
  presence: 2,
  adversary_type: 'Nemesis',
  soak: 2,
  wound_th: 13,
  strain_th: 17,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Arcana').id, rank: 3},
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Forbidden').id, rank: 3},
    {id: Skill.find_by_name('Melee (Light)').id, rank: 1},
    {id: Skill.find_by_name('Vigilance').id, rank: 1},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 1},
    {id: Talent.find_by_name('Chill of Nordros').id, rank: 0},
    {id: Talent.find_by_name('Dark Insight').id, rank: 0},
    {id: Talent.find_by_name('Necromancy').id, rank: 0},
  ],
  weapons: [
    {
      name: 'Sacrificial dagger',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 4,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Accurate').id, rank: 1},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  equipment: "<p>Bone staff (Add +4 damage to magic attacks and first Range effect does not increase difficulty; heal 1 wound after damaging a target with a spell.), heavy robes (+1 defense), forbidden grimoire (A user with the Dark Insight talent can add the Additional Target and Enervate effects to curse spells with no increase in difficulty).</p>",
  spells: [
    {name: 'Death Knell', description: 'Choose one target at short or medium range and make an <b>Average ([d][d]) Arcana check</b>. If successful, this magic attack inflicts 8 damage +1 damage per uncanceled [su], with Critical Rating 2 and the Ensnare 3 and Vicious 3 qualities.'},
    {name: 'Wilt', description: 'Choose two targets within short range and make an <b>Average ([d][d]) Arcana check</b>. If successful, the Necromancer can choose one additional target for each [ad] and all targets reduce the ability of any skill checks they make by one until the end of the necromancer’s next turn. If an affected character suffers strain for any reason, they suffer 1 additional strain. The necromancer can perform the concentrate maneuver to maintain all effects of this curse.'},
    {name: 'Wall of Bones', description: 'Make a <b>Hard ([d][d][d]) Arcana check</b>. If successful, the necromancer reduces the damage of all hits they suffer by 1 plus 1 for every [su][su] beyond the first until the end of his next turn; in addition, if an attack targeting the necromancer generates [t][t][t] or [des], the attacker suffers a hit inflicting damage equal to the total damage of the attack; the necromancer may perform the concentrate maneuver to maintain the effects of this barrier.'},
  ],
  description: "<p>Mavaris Skain bears an unsettling resemblance to the corpses he works with. In the privacy of his lab deep in the catacombs, Skain wears tattered black robes and ornaments himself in corpse talismans and profane symbols. Skain’s embrace of the forbidden art of necromancy is decided overcompensation for his own fear of death. To this end, he has sworn his service and lasting life to Lord Vorakesh in exchange for tutelage in the blackest of arts.</p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('The Haunted City', 'Spellcaster')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Blighted Shrub')
adversary.update_attributes(
  brawn: 2,
  agility: 1,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 3,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 0},
    {id: Skill.find_by_name('Skulduggery').id, rank: 0},
    {id: Skill.find_by_name('Stealth').id, rank: 0},
  ],
  abilities: [
    {name: 'Bring me a shrubbery!', description: 'In an encounter with any number of Blighted Shrubs, the GM may spend [t][t] from any check made by a PC to add one Blighted Shrub to an existing minion group, or spend [des] to add a new minion group of three Blighted Shrubs to the encounter.'},
    {name: 'Inconspicuous', description: 'A Blighted Shrub in a natural environment appears indistinguishable from an ordinary plant; a character can make a <b>Hard ([d][d][d]) Perception or Vigilance check</b> to identify a Blighted Shrub.'},
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
    {name: 'Unexpected Attack', description: 'A Blighted Shrub that has not yet been identified as a threat uses Stealth to determine initiative'},
    {name: 'Silhouette 0', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Axes, Fire, and Holy Weakness', description: 'Axes, Fire, blessed, or holy weapons and ammunition count their Critical rating as 1 when used against this adversary.'},
  ],
  talents: [
  ],
  weapons: [
    {
      name: 'Claws',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 4,
      critical: 3,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Pierce').id, rank: 1},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  equipment: "",
  spells: [
  ],
  description: "",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Haedra\'s Shard', 'Undead', 'Plant')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Blighted Forest Guardian')
adversary.update_attributes(
  brawn: 5,
  agility: 2,
  intellect: 2,
  cunning: 2,
  willpower: 3,
  presence: 3,
  adversary_type: 'Nemesis',
  soak: 6,
  wound_th: 25,
  strain_th: 18,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Cool').id, rank: 2},
    {id: Skill.find_by_name('Discipline').id, rank: 2},
    {id: Skill.find_by_name('Brawl').id, rank: 2},
    {id: Skill.find_by_name('Resilience').id, rank: 3},
  ],
  talents: [
    {id: Talent.find_by_name('Adversary').id, rank: 2},
  ],
  abilities: [
    {name: 'Sweep Attack', description: 'A Blighted Forest Guardian may spend [advantage] from a Brawl check to hit an additional engaged opponent that would be no more difficult to attack than the original target, dealing base damage +1 damage per [success].'},
    {name: 'Silhouette 3', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Terrifying', description: 'At the start of the encounter, all opponents must make a <b>Hard ([d][d][d]) fear check</b> as an out-of-turn incidental, as per page 243 of the Genesys Core Rulebook. If there are multiple sources of fear in the encounter, the opponents make one fear check against the most terrifying enemy.'},
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
    {name: 'Uproot', description: 'For the first turn a Blighted Forest Guardian wishes to move, it must spend three maneuvers'},
    {name: 'Axes, Fire, and Holy Weakness', description: 'Axes, Fire, blessed, or holy weapons and ammunition count their Critical rating as 1 when used against this adversary.'},
  ],
  weapons: [
    {
      name: 'Huge Limbs',
      skill_id: Skill.find_by_name('Brawl').id,
      damage: 10,
      critical: 4,
      range: 'Short',
      qualities: [
        {id: ItemQuality.find_by_name('Knockdown').id, rank: 0},
        {id: ItemQuality.find_by_name('Prepare').id, rank: 1},
      ]
    },
  ],
  equipment: "<p></p>",
  spells: [
    {name: '', description: ''},
  ],
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Nemesis')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Haedra\'s Shard', 'Undead', 'Plant')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Blighted Bramble Darter')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 1,
  cunning: 3,
  willpower: 2,
  presence: 1,
  adversary_type: 'Rival',
  soak: 3,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 2},
    {id: Skill.find_by_name('Ranged').id, rank: 3},
    {id: Skill.find_by_name('Stealth').id, rank: 2},
  ],
  talents: [
  ],
  abilities: [
    {name: 'Inconspicuous', description: 'A Blighted Bramble Darter in a natural environment appears indistinguishable from an ordinary plant; a character can make a <b>Hard ([d][d][d]) Perception or Vigilance check</b> to identify a Blighted Bramble Darter.'},
    {name: 'Tailored Poison', description: 'Once per encounter before making a Ranged check, as a maneuver a Bramble Darter can apply a tailored poison to a bramble dart; if the check succeeds, the attack inflicts additional damage equal to the target’s highest characteristic and gains the Disorient 3 quality.'},
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
    {name: 'Unexpected Attack', description: 'A Blighted Bramble Darter that has not yet been identified as a threat uses Stealth to determine initiative'},
    {name: 'Axes, Fire, and Holy Weakness', description: 'Axes, Fire, blessed, or holy weapons and ammunition count their Critical rating as 1 when used against this adversary.'},
  ],
  weapons: [
    {
      name: 'Poisoned bramble darts',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 5,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Auto-Fire').id, rank: 0},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 1},
        {id: ItemQuality.find_by_name('Poison').id, rank: 0},
      ]
    },
  ],
  equipment: "<p></p>",
  spells: [
    {name: '', description: ''},
  ],
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Haedra\'s Shard', 'Undead', 'Plant')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Blighted Carnivorous Flora')
adversary.update_attributes(
  brawn: 2,
  agility: 2,
  intellect: 1,
  cunning: 1,
  willpower: 1,
  presence: 1,
  adversary_type: 'Rival',
  soak: 2,
  wound_th: 12,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Brawl').id, rank: 3},
    {id: Skill.find_by_name('Resilience').id, rank: 2},
    {id: Skill.find_by_name('Stealth').id, rank: 4},
  ],
  talents: [
  ],
  abilities: [
    {name: 'Drag', description: 'A Carnivorous Flora can use a maneuver to move a target affected by its thorny vines\' Ensnare quality to engaged range.'},
    {name: 'Inconspicuous', description: 'A Carnivorous Flora in a natural environment appears indistinguishable from an ordinary plant; a character can make a <b>Hard ([d][d][d]) Perception or Vigilance check</b> to identify a Blighted Bramble Darter.'},
    {name: 'Rooted', description: 'A Carnivorous Flora cannot perform maneuvers to move.'},
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
    {name: 'Unexpected Attack', description: 'A Carnivorous Flora that has not yet been identified as a threat uses Stealth to determine initiative'},
    {name: 'Silhouette 2', description: 'When making an attack against a target with a silhouette two or more points larger than they are, they decrease the difficulty of the check by one. When making an attack against a target with a silhouette two or more points smaller than they are, they increase the difficulty of the check by one.'},
    {name: 'Axes, Fire, and Holy Weakness', description: 'Axes, Fire, blessed, or holy weapons and ammunition count their Critical rating as 1 when used against this adversary.'},
  ],
  weapons: [
{
  name: 'Thorny Vines',
  skill_id: Skill.find_by_name('Brawl').id,
  damage: 4,
  critical: 4,
  range: 'Short',
  qualities: [
    {id: ItemQuality.find_by_name('Auto-Fire').id, rank: 0},
    {id: ItemQuality.find_by_name('Ensnare').id, rank: 3},
  ]
},
  ],
  equipment: "<p></p>",
  spells: [
    {name: '', description: ''},
  ],
  description: "<p></p>",
  image_url: ''
)
adversary.type_list.add('Rival')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Haedra\'s Shard', 'Undead', 'Plant')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Reanimate Lumberjack')
adversary.update_attributes(
  brawn: 3,
  agility: 2,
  intellect: 1,
  cunning: 2,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 4,
  wound_th: 4,
  strain_th: nil,
  defense_melee: 0,
  defense_ranged: 0,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Athletics').id, rank: 0},
    {id: Skill.find_by_name('Melee (Heavy)').id, rank: 0},
    {id: Skill.find_by_name('Perception').id, rank: 0},
    {id: Skill.find_by_name('Resilience').id, rank: 0},
    {id: Skill.find_by_name('Vigilance').id, rank: 0},
  ],
  abilities: [
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
    {name: 'Undying', description: 'may spend [t][t][t] from any check made by a PC to return one previously defeated Reanimate to an existing minion group, removing damage from the group accordingly. Spend [des] to return two Reanimates to a minion group).'},
  ],
  talents: [
  ],
  weapons: [
    {
      name: 'Rusted Greataxe',
      skill_id: Skill.find_by_name('Melee (Heavy)').id,
      damage: 7,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Cumbersome').id, rank: 3},
        {id: ItemQuality.find_by_name('Pierce').id, rank: 1},
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
  ],
  equipment: "Heavy clothing (+1 soak).",
  spells: [
  ],
  description: "Strangehaven’s loggers recently slaughtered by blighted bramble darters have become reanimated from the necro-alchemical toxin. They roam the Hanging Woods, eager to murder the living.",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Haedra\'s Shard', 'Undead')
adversary.save
puts "Adversary: #{adversary.name}"

adversary = Adversary.find_or_create_by(name: 'Skeleton Woodsman')
adversary.update_attributes(
  brawn: 2,
  agility: 3,
  intellect: 1,
  cunning: 2,
  willpower: 1,
  presence: 1,
  adversary_type: 'Minion',
  soak: 3,
  wound_th: 3,
  strain_th: nil,
  defense_melee: 1,
  defense_ranged: 1,
  combat_rating: nil,
  social_rating: nil,
  general_rating: nil,
  skills: [
    {id: Skill.find_by_name('Melee (Light)').id, rank: 0},
    {id: Skill.find_by_name('Perception').id, rank: 0},
    {id: Skill.find_by_name('Ranged').id, rank: 0},
    {id: Skill.find_by_name('Resilience').id, rank: 0},
    {id: Skill.find_by_name('Vigilance').id, rank: 0},
  ],
  abilities: [
    {name: 'Undead', description: 'Does not need to breathe, eat, or drink, and can survive underwater; immune to poisons and toxins.'},
  ],
  talents: [
  ],
  weapons: [
    {
      name: 'Rusted Axe',
      skill_id: Skill.find_by_name('Melee (Light)').id,
      damage: 5,
      critical: 4,
      range: 'Engaged',
      qualities: [
        {id: ItemQuality.find_by_name('Vicious').id, rank: 1},
      ]
    },
    {
      name: 'Worn Longbow',
      skill_id: Skill.find_by_name('Ranged').id,
      damage: 7,
      critical: 3,
      range: 'Medium',
      qualities: [
        {id: ItemQuality.find_by_name('Unwieldy').id, rank: 3},
      ]
    },
  ],
  equipment: "Worn leather (+1 soak).",
  spells: [
  ],
  description: "These unfortunate and restless souls have been dead for some time, now reanimated undead from the necro-alchemical toxin, who stalk the woods, eagerly hunting the living.",
  image_url: ''
)
adversary.type_list.add('Minion')
adversary.setting_list.add('Terrinoth', 'Fantasy')
adversary.tag_list.add('Haedra\'s Shard', 'Undead')
adversary.save
puts "Adversary: #{adversary.name}"
