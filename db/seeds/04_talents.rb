puts '#########################'
puts '# Seeding Talents       #'
puts '#########################'
talent = Talent.find_or_create_by(name: 'Adversary')
talent.update_attributes(tier: 0, activation: 'Passive', ranked: true, description: "<p>Upgrade the difficulty of all combat checks targeting this character once per rank of Adversary.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Bought Info')
talent.update_attributes(tier: 1, activation: 'Active (Action)', ranked: false, description: "<p>When making any knowledge skill check, your character can instead use this talent to spend an amount of currency equal to fifty times the difficulty of the check and automatically succeed on the knowledge check with one uncanceled [success] (instead of rolling). At your GM’s discretion, your character may not be able to use Bought Info if the information is particularly sensitive or difficult to find, or buying it doesn’t make narrative sense.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Clever Retort')
talent.update_attributes(tier: 1, activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p>Once per encounter, your character may use this talent to add automatic [threat] [threat] to another character’s social skill check.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Defensive Sysops', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Add [s][s] to opponent's checks when defending a computer system against intrusion.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Desperate Recovery')
talent.update_attributes(tier: 1, activation: 'Passive', ranked: false, description: "<p>Before your character heals strain at the end of an encounter, if their strain is more than half of their strain threshold, they heal two additional strain.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Duelist')
talent.update_attributes(tier: 1, activation: 'Passive', ranked: false, description: "<p>Your character adds [boost] to their melee combat checks while engaged with a single opponent. Your character adds [setback] to their melee combat checks while engaged with three or more opponents.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Durable')
talent.update_attributes(tier: 1, activation: 'Passive', ranked: true, description: "<p>Your character reduces any Critical Injury result they suffer by 10 per rank of Durable, to a minimum of 01.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Forager')
talent.update_attributes(tier: 1, activation: 'Passive', ranked: false, description: "<p>Your character removes up to [setback] [setback] from any skill checks they make to find food, water, or shelter. Checks to forage or search the area that your character makes take half the time they would normally.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Grit')
talent.update_attributes(tier: 1, activation: 'Passive', ranked: true, description: "<p>Each rank of Grit increases your character’s strain threshold by one.</p>")
modifications = {
  strain: 1
}
talent.update_attributes(modifications: modifications)
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Hamstring Shot')
talent.update_attributes(tier: 1, activation: 'Active (Action)', ranked: false, description: "<p>Once per round, your character may use this talent to perform a ranged combat check against one non-vehicle target within range of the weapon used. If the check is successful, halve the damage inflicted by the attack (before reducing damage by the target’s soak). The target is immobilized until the end of its next turn.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Jump Up')
talent.update_attributes(tier: 1, activation: 'Active (Incidental)', ranked: false, description: "<p>Once per round during your character’s turn, your character may use this talent to stand from a prone or seated position as an incidental.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Knack for It')
talent.update_attributes(tier: 1, activation: 'Passive', ranked: true, description: "<p>When you purchase this talent for your character, select one skill. Your character removes [setback] [setback] from any checks they make using this skill.</p><p>Each additional time you purchase this talent for your character, select two additional skills. Your character also removes [setback] [setback] from any checks they make using these skills. You cannot select combat or magic skills when choosing skills for this talent.</p>")
modifications = {
  skill_select: []
}
talent.update_attributes(modifications: modifications)
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Know Somebody')
talent.update_attributes(tier: 1, activation: 'Active (Incidental)', ranked: true, description: "<p>Once per session, when attempting to purchase a legally available item, your character may use this talent to reduce its rarity by one per rank of Know Somebody.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Let\'s Ride')
talent.update_attributes(tier: 1, activation: 'Active (Incidental)', ranked: false, description: "<p>Once per round during your character's turn, your character can use this talent to mount or dismount from a vehicle or animal, or move from one position in a vehicle to another (such as from the cockpit to a gun turret) as an incidental. In addition, if your character suffers a short-range fall (see page 112 of the Genesys Core Rulebook) from a vehicle or animal, they suffer no damage and land on their feet.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'One with Nature')
talent.update_attributes(tier: 1, activation: 'Active (Incidental)', ranked: false, description: "<p>When in the wilderness, your character may make a Simple (–) Survival check, instead of Discipline or Cool, to recover strain at the end of an encounter (see page 117 of the Genesys Core Rulebook).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Parry')
talent.update_attributes(tier: 1, activation: 'Active (Incidental, Out of Turn)', ranked: true, description: "<p>When your character suffers a hit from a melee combat check, after damage is calculated but before soak is applied (so immediately after Step 3 of Perform a Combat check, page 102), your character may suffer 3 strain to use this talent to reduce the damage of the hit by two plus their ranks in Parry. This talent can only be used once per hit, and your character needs to be wielding a Melee weapon.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Proper Upbringing')
talent.update_attributes(tier: 1, activation: 'Active (Incidental)', ranked: true, description: "<p>When your character makes a social skill check in polite company (as determined by your GM), they may suffer a number of strain to use this talent to add an equal number of [advantage] to the check. The number may not exceed your character’s ranks in Proper Upbringing.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Quick Draw')
talent.update_attributes(tier: 1, activation: 'Active (Incidental)', ranked: false, description: "<p>Once per round on your character’s turn, they may use this talent to draw or holster an easily accessible weapon or item as an incidental. Quick Draw also reduces a weapon’s Prepare rating by one, to a minimum of one.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Quick Strike')
talent.update_attributes(tier: 1, activation: 'Passive', ranked: true, description: "<p>Your character adds [boost] for each rank of Quick Strike to any combat checks they make against any targets that have not yet taken their turn in the current encounter.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Rapid Reaction')
talent.update_attributes(tier: 1, activation: 'Active (Incidental, Out of Turn)', ranked: true, description: "<p>Your character may suffer a number of strain to use this talent to add an equal number of [success] to a Vigilance or Cool check they make to determine Initiative order. The number may not exceed your character’s ranks in Rapid Reaction.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Second Wind')
talent.update_attributes(tier: 1, activation: 'Active (Incidental)', ranked: true, description: "<p>Once per encounter, your character may use this talent to heal an amount of strain equal to their ranks in Second Wind.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Surgeon')
talent.update_attributes(tier: 1, activation: 'Passive', ranked: true, description: "<p>When your character makes a Medicine check to heal wounds, the target heals one additional wound per rank of Surgeon.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Swift')
talent.update_attributes(tier: 1, activation: 'Passive', ranked: false, description: "<p>Your character does not suffer the penalties for moving through difficult terrain (they move through difficult terrain at normal speed without spending additional maneuvers).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Toughened')
talent.update_attributes(tier: 1, activation: 'Passive', ranked: true, description: "<p>Each rank of Toughened increases your character’s wound threshold by two.</p>")
modifications = {
  wounds: 2
}
talent.update_attributes(modifications: modifications)
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Unremarkable')
talent.update_attributes(tier: 1, activation: 'Passive', ranked: false, description: "<p>Other characters add [failure] to any checks made to find or identify your character in a crowd.</p>")
puts "Talent: #{talent.name}"

puts '# Tier 2 #'

talent = Talent.find_or_create_by(name: 'Basic Military Training', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Athletics, Ranged (Heavy), and Resilience are now career skills for your character.</p>")
skill_1 = Skill.find_by(name: 'Athletics')
skill_2 = Skill.find_by(name: 'Ranged (Heavy)')
skill_3 = Skill.find_by(name: 'Resilience')
modifications = {
  career_skill: [skill_1.id, skill_2.id, skill_3.id]
}
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Berserk')
talent.update_attributes(tier: 2, activation: 'Active (Maneuver)', ranked: false, description: "<p>Once per encounter, your character may use this talent. Until the end of the encounter or until they are incapacitated, your character adds [success] [advantage] [advantage] to all melee combat checks they make. However, opponents add [success] to all combat checks targeting your character. While berserk, your character cannot make ranged combat checks.</p><p>At the end of the encounter (or when they are incapacitated), your character suffers 6 strain.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Coordinated Assault')
talent.update_attributes(tier: 2, activation: 'Active (Maneuver)', ranked: true, description: "<p>Once per turn, your character may use this talent to have a number of allies engaged with your character equal to your ranks in Leadership add [advantage] to all combat checks they make until the end of your character’s next turn. The range of this talent increases by one band per rank of Coordinated Assault beyond the first.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Counteroffer')
talent.update_attributes(tier: 2, activation: 'Active (Action)', ranked: false, description: "<p>Once per session, your character may use this talent to choose one non-nemesis adversary within medium range and make an opposed Negotiation versus Discipline check. If successful, the target becomes staggered until the end of their next turn.</p><p>At your GM’s discretion, you may spend [triumph] on this check to have the adversary become an ally until the end of the encounter. However, the duration of this may be shortened or extended depending on whether your GM feels your offer is appealing to the adversary and whether your character follows through on their offer!</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Daring Aviator', tier: 2)
talent.update_attributes(activation: 'Active (Maneuver)', ranked: true, description: "<p>Before your character makes a Driving or Piloting check, they may add a number of [t] to the results to use this talent to add an equal number of [su]. The number may not exceed your character’s ranks in Daring Aviator.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Defensive Stance')
talent.update_attributes(tier: 2, activation: 'Active (Maneuver)', ranked: true, description: "<p>Once per round, your character may suffer a number of strain no greater than their ranks in Defensive Stance to use this talent. Then, until the end of your character’s next turn, upgrade the difficulty of all melee combat checks targeting your character a number of times equal to the strain suffered.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Defensive Sysops (Improved)', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Your character must have purchased the Defensive Sysops talent to benefit from this talent. Before adding [s][s] from Defensive Sysops to a check, use this talent to add [f][t] to the results of the check instead.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Dual Wielder')
talent.update_attributes(tier: 2, activation: 'Active (Maneuver)', ranked: false, description: "<p>Once per round, your character may use this talent to decrease the difficulty of the next combined combat check (see Two-Weapon Combat, on page 108 of the Genesys Core Rulebook) they make during the same turn by one.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Fan the Hammer', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Once per encounter before making a combat check with a pistol, your character may use this talent to add the Auto-fire quality to the pistol when resolving the check. If your character does, the weapon runs out of ammo exactly as with an Out of Ammo result.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Heightened Awareness')
talent.update_attributes(tier: 2, activation: 'Passive', ranked: false, description: "<p>Allies within short range of your character add [boost] to their Perception and Vigilance checks. Allies engaged with your character add [boost] [boost] instead.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Inspiring Rhetoric')
talent.update_attributes(tier: 2, activation: 'Active (Action)', ranked: false, description: "<p>Your character may use this talent to make an Average ([difficulty] [difficulty]) Leadership check. For each [success] the check generates, one ally within short range heals one strain. For each [advantage], one ally benefiting from Inspiring Rhetoric heals one additional strain.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Inventor')
talent.update_attributes(tier: 2, activation: 'Active (Incidental)', ranked: true, description: "<p>When your character makes a check to construct new items or modify existing ones, use this talent to add a number of [boost] to the check equal to ranks of Inventor. In addition, your character may attempt to reconstruct devices that they have heard described but have not seen and do not have any kinds of plans or schematics for.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Lucky Strike')
talent.update_attributes(tier: 2, activation: 'Active (Incidental)', ranked: false, description: "<p>When your character purchases this talent, choose one characteristic. After your character makes a successful combat check, you may spend one Story Point to use this talent to add damage equal to your character’s ranks in that characteristic to one hit of the combat check.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Scathing Tirade')
talent.update_attributes(tier: 2, activation: 'Active (Action)', ranked: false, description: "<p>Your character may use this talent to make an Average ([difficulty][difficulty]) Coercion check. For each [success] the check generates, one enemy within short range suffers 1 strain. For each [advantage], one enemy affected by Scathing Tirade suffers 1 additional strain.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Side Step')
talent.update_attributes(tier: 2, activation: 'Active (Maneuver)', ranked: true, description: "<p>Once per round, your character may suffer a number of strain no greater than their ranks in Side Step to use this talent. Until the end of your character’s next turn, upgrade the difficulty of all ranged combat checks targeting your character a number of times equal to the strain suffered.</p>")
puts "Talent: #{talent.name}"

puts '# Tier 3 #'

talent = Talent.find_or_create_by(name: 'Animal Companion')
talent.update_attributes(tier: 3, activation: 'Passive', ranked: true, description: "<p>Your character creates a bond with a single animal approved by your GM. This animal must be silhouette 0 (no larger than a mid-sized dog). The bond persists as long as your character chooses, although at your GM’s discretion, the bond may also be broken due to abusive treatment or other extenuating circumstances.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Barrel Roll', tier: 3)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p>Your character can only use this talent while piloting a starfighter or airplane of Silhouette 3 or less. When your vehicle suffers a hit from a ranged combat check, after damage is calculated but before armor is applied, your character may have their vehicle suffer 3 system strain to use this talent. Then, reduce the damage suffered by a number equal to their ranks in Piloting.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Distinctive Style', tier: 3)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When making a Computers check to hack a system or break into a secured network, before rolling, your character may use this talent to add [s][s][t][t] to the results.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Dodge')
talent.update_attributes(tier: 3, activation: 'Active (Incidental, Out of Turn)', ranked: true, description: "<p>When your character is targeted by a combat check (ranged or melee), they may suffer a number of strain no greater than their ranks in Dodge to use this talent. Then, upgrade the difficulty of the combat check targeting your character a number of times equal to the strain suffered.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Eagle Eyes')
talent.update_attributes(tier: 3, activation: 'Active (Incidental)', ranked: false, description: "<p>Once per encounter before making a ranged combat check, you may use this talent to increase your weapon’s range by one range band (to a maximum of extreme range). This lasts for the duration of the combat check.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Field Commander')
talent.update_attributes(tier: 3, activation: 'Active (Action)', ranked: false, description: "<p>Your character may use this talent to make an Average ([difficulty] [difficulty]) Leadership check. If successful, a number of allies equal to your character’s Presence may immediately suffer 1 strain to perform one maneuver (out of turn). If there are any questions as to which allies take their maneuvers first, your character is the final arbiter.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Forgot to Count?', tier: 3)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p>When an opponent makes a ranged combat check, you can spend [t][t] from that check to use this talent to cause their weapon to run out of ammo, as long as the weapon can normally run out of ammunition.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Full Throttle', tier: 3)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>While driving or flying, your character may use this talent to make a Hard ([d][d][d]) Piloting or Driving check. If successful, the top speed of the vehicle increases by one (to a maximum of 5) for a number of rounds equal to your character’s Cunning</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Grenadier', tier: 3)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When your character makes a ranged combat check with a weapon that has the Blast item quality, you may spend one Story Point to use this talent to trigger the weapon’s Blast quality, instead of spending a (even if the attack misses). In addition, your character treats grenades as having a range of medium.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Inspiring Rhetoric (Improved)')
talent.update_attributes(tier: 3, activation: 'Passive', ranked: false, description: "<p><em>Your character must have purchased the Inspiring Rhetoric talent to benefit from this talent.</em> Allies affected by your character’s Inspiring Rhetoric add [boost] to all skill checks they make for a number of rounds equal to your character’s ranks in Leadership</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Painkiller Specialization')
talent.update_attributes(tier: 3, activation: 'Passive', ranked: true, description: "<p>When your character uses painkillers (or their equivalent, depending on the setting), the target heals one additional wound per rank of Painkiller Specialization. The sixth painkiller and beyond each day still has no effect.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Scathing Tirade (Improved)')
talent.update_attributes(tier: 3, activation: 'Passive', ranked: false, description: "<p><em>Your character must have purchased the Scathing Tirade talent to benefit from this talent.</em> Enemies affected by your character’s Scathing Tirade add [setback] to all skill checks they make for a number of rounds equal to your character’s ranks in Coercion.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Heroic Will')
talent.update_attributes(tier: 3, activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p>When you purchase this talent for your character, choose two characteristics. You may spend a Story Point to use this talent to have your character ignore the effects of all Critical Injuries on any skill checks using those two characteristics until the end of the current encounter. (Your character still suffers the Critical Injuries; they just ignore the effects. See page 114.)</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Natural')
talent.update_attributes(tier: 3, activation: 'Active (Incidental)', ranked: false, description: "<p>When your character purchases this talent, choose two skills. Once per session, your character may use this talent to reroll one skill check that uses one of those two skills.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Rapid Archery')
talent.update_attributes(tier: 3, activation: 'Active (Maneuver)', ranked: false, description: "<p>While your character is armed with a bow (or similar weapon, at your GM’s discretion) they may suffer 2 strain to use this talent. During the next ranged combat check your character makes this turn, the bow gains the Linked quality with a value equal to their ranks in the Ranged skill.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Parry (Improved)')
talent.update_attributes(tier: 3, activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p><em>Your character must have purchased the Parry talent to benefit from this talent.</em> When your character suffers a hit from a melee combat check and uses Parry to reduce the damage from that hit, after the attack is resolved, you may spend [despair] or [threat] [threat] [threat] from the attacker’s check to use this talent. Then, your character automatically hits the attacker once with a Brawl or Melee weapon your character is wielding. The hit deals the weapon’s base damage, plus any damage from applicable talents or abilities. Your character can’t use this talent if the original attack incapacitates them.</p>")
puts "Talent: #{talent.name}"

puts '# Tier 4 #'

talent = Talent.find_or_create_by(name: 'Can’t We Talk About This?')
talent.update_attributes(tier: 4, activation: 'Active (Action)', ranked: false, description: "<p>Your character can use this talent to make an opposed Charm or Deception versus Discipline check targeting a single non-nemesis adversary within medium range. If the check succeeds, the target cannot attack your character (or perform hostile actions against your character) until the end of their next turn. You may spend [advantage] [advantage] to increase the length of the effect by one additional turn, and spend [triumph] to extend the benefits to all of their identified allies within short range.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Deadeye')
talent.update_attributes(tier: 4, activation: 'Active (Incidental)', ranked: false, description: "<p>After your character inflicts a Critical Injury with a ranged weapon and rolls the result, your character may suffer 2 strain to use this talent. Then, you may select any Critical Injury of the same severity to apply to the target instead.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Defensive')
talent.update_attributes(tier: 4, activation: 'Passive', ranked: true, description: "<p>Each rank of Defensive increases your character’s melee defense and ranged defense by one.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Defensive Driving', tier: 4)
talent.update_attributes(activation: 'Passive', ranked: true, description: "<p>Increase the defense of any vehicle your character pilots by one per rank of Defensive Driving.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Enduring')
talent.update_attributes(tier: 4, activation: 'Passive', ranked: true, description: "<p>Each rank of Enduring increases your character’s soak value by one.</p>")
modifications = {
  soak: 1
}
talent.update_attributes(modifications: modifications)
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Field Commander (Improved)')
talent.update_attributes(tier: 4, activation: 'Passive', ranked: false, description: "<p><em>Your character must have purchased the Field Commander talent to benefit from this talent.</em> When your character uses the Field Commander talent, your character affects a number of allies equal to twice the character’s Presence. In addition, you may spend [triumph] to allow one ally to suffer 1 strain to perform an action, instead of a maneuver.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'How Convenient!')
talent.update_attributes(tier: 4, activation: 'Active (Action)', ranked: false, description: "<p>Once per session, your character may use this talent to make a Hard ([difficulty][difficulty][difficulty]) Mechanics check. If successful, one device involved in the current encounter (subject to your GM’s approval) spontaneously fails. This can be because of your character’s actions, or it can simply be incredibly convenient timing!</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Inspiring Rhetoric (Supreme)')
talent.update_attributes(tier: 4, activation: 'Active (Incidental)', ranked: false, description: "<p><em>Your character must have purchased the Inspiring Rhetoric talent to benefit from this talent.</em> Your character may choose to suffer 1 strain to use the Inspiring Rhetoric talent as a maneuver, instead of as an action.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Mad Inventor', tier: 4)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>Once per session, your character may use this talent to make a Mechanics check to attempt to cobble together the functional equivalent of any item using spare parts or salvage. The difficulty of the check is based on the item’s rarity; see Table I.4–1: Mad Inventor Item Rarity GCRB p. 80.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Overcharge', tier: 4)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>Once per encounter, your character may use this talent to make a Hard ([d][d][d]) Mechanics check and choose one of their cybernetic implants that grants them one of the following: +1 to a characteristic rating, +1 rank to a skill, +1 rank of a ranked talent. If your character succeeds, until the end of the encounter, the chosen cybernetic instead provides +2 to the affected characteristic rating (to a maximum of 7), skill (to a maximum of 5), or ranked talent.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Scathing Tirade (Supreme)')
talent.update_attributes(tier: 4, activation: 'Active (Incidental)', ranked: false, description: "<p><em>Your character must have purchased the Scathing Tirade talent to benefit from this talent.</em> Your character may choose to suffer 1 strain to use the Scathing Tirade talent as a maneuver, instead of as an action.</p>")
puts "Talent: #{talent.name}"

puts '# Tier 5 #'

talent = Talent.find_or_create_by(name: 'Dedication')
talent.update_attributes(tier: 5, activation: 'Passive', ranked: true, description: "<p>Each rank of Dedication increases one of your character’s characteristics by one. This talent cannot increase a characteristic above 5. You cannot increase the same characteristic with Dedication twice.</p>")
modifications = {
  characteristic_select: true
}
talent.update_attributes(modifications: modifications)
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Indomitable')
talent.update_attributes(tier: 5, activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p>Once per encounter, when your character would be incapacitated due to exceeding their wound or strain threshold, you may spend a Story Point to use this talent. Then, your character is not incapacitated until the end of their next turn. If your character reduces their strain or wounds to below their threshold before the end of their next turn, they are not incapacitated.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Master')
talent.update_attributes(tier: 5, activation: 'Active (Incidental)', ranked: false, description: "<p>When you purchase this talent for your character, choose one skill. Once per round, your character may suffer 2 strain to use this talent to reduce the difficulty of the next check they make using that skill by two, to a minimum of Easy ([difficulty]).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Overcharge (Improved)', tier: 5)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Your character must have purchased the Overcharge talent to benefit from this talent. When using the Overcharge talent, your character may spend [ad][ad] or [tri] from the Mechanics check to immediately take one additional action. This talent can only be used once per check.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Ruinous Repartee')
talent.update_attributes(tier: 5, activation: 'Active (Action)', ranked: false, description: "<p>Once per encounter, your character may use this talent to make an opposed Charm or Coercion versus Discipline check targeting one character within medium range (or within earshot). If successful, the target suffers strain equal to twice your character’s Presence, plus one additional strain per [success]. Your character heals strain equal to the strain inflicted.</p><p>If incapacitated due to this talent, the target could flee the scene in shame, collapse in a dejected heap, or throw themself at your character in fury, depending on your GM and the nature of your character’s witty barbs.</p>")
puts "Talent: #{talent.name}"


puts '#############################'
puts '# Seeding Terrinoth Talents #'
puts '#############################'
puts '# Tier 1 #'

talent = Talent.find_or_create_by(name: 'Apothercary', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: true, description: "<p>When a patient under your character’s care heals wounds from natural rest, they heal additional wounds equal to twice your character’s ranks in Apothecary.")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Bullrush', tier: 1)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When your character makes a Brawl, Melee (Light), or Melee (Heavy) combat check after using a maneuver to engage a target, you may spend [advantage] [advantage] [advantage] or [triumph] to use this talent to knock the target prone and move them up to one range band away from your character.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Challenge!', tier: 1)
talent.update_attributes(activation: 'Active (Maneuver)', ranked: true, description: "<p>Once per encounter, your character may use this talent to choose a number of adversaries within short range no greater than your character’s ranks in Challenge! (a minion group counts as a single adversary for this purpose). Until the encounter ends or your character is incapacitated, these adversaries add [boost] to combat checks targeting your character and [setback] [setback] to combat checks targeting other characters.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Dark Insight', tier: 1)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When a spell adds a quality to your character's spell with a rating determined by your character's ranks in Knowledge (Lore), your character may use their ranks in Knowledge (Forbidden) instead.")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Dungeoneer', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: true, description: "<p>After your character makes a Perception, Vigilance, or Knowledge (Adventuring) check to notice, identify, or avoid a threat in a cavern, subterranean ruin, or similar location, your character cancels a number of uncanceled [threat] no greater than your character's ranks in Dungeoneer.")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Finesse', tier: 1)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When making a Brawl or Melee (Light) check, your character may use Agility instead of Brawn.")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Painful Blow', tier: 1)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When your character makes a combat check, you may voluntarily increase the difficulty by one to use this talent. If the target suffers one or more wounds from the combat check, the target suffers 2 strain each time they perform a maneuver until the end of the encounter.")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Precision', tier: 1)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When making a Brawl or Ranged check, your character may use Cunning instead of Brawn and Agility.")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Shapeshifter', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When your character is incapacitated due to having exceeded their strain threshold while in their normal form, they undergo the following change as an out-of-turn incidental: they heal all strain, increase their Brawn and Agility by one to a maximum of 5 and reduce their Intellect and Willpower by one to a minimum of 1. They deal +1 damage when making unarmed attacks and their unarmed attacks have a Critical rating of 3, but they cannot use magic skills or make ranged attacks.")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Shield Slam', tier: 1)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When your character uses a shield to attack a minion or rival, you may spend [advantage] [advantage] [advantage] [advantage] or [triumph] to stagger the target until the end of the target's next turn.")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Tavern Brawler', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Your character adds [advantage] to Brawl checks and combat checks using improvised weapons.")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Templar', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Divine is now a career skill for your character. They can only cast one spell using this skill per encounter.")
skill = Skill.find_by(name: 'Divine')
modifications = {
  career_skill: [skill.id]
}
talent.update_attributes(modifications: modifications)
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Tumble', tier: 1)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Once per round on your character's turn, they may suffer 2 strain to disengage from all engaged adversaries.")
puts "Talent: #{talent.name}"

puts '# Tier 2 #'

talent = Talent.find_or_create_by(name: 'Adventurer', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Athletics and Knowledge (Adventuring) are now career skills for your character.</p>")
skill_1 = Skill.find_by(name: 'Athletics')
skill_2 = Skill.find_by(name: 'Adventuring')
modifications = {
  career_skill: [skill_1.id, skill_2.id]
}
talent.update_attributes(modifications: modifications)
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Bard', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Knowledge (Lore) and Verse are now career skills for your character.</p>")
skill_1 = Skill.find_by(name: 'Verse')
skill_2 = Skill.find_by(name: 'Lore')
modifications = {
  career_skill: [skill_1.id, skill_2.id]
}
talent.update_attributes(modifications: modifications)
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Block', tier: 2)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p><em>Your character must have purchased the Parry talent to benefit from this talent.</em> While wielding a shield, your character may use the Parry talent to reduce damage from ranged attacks as well as melee attacks targeting your character.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Blood Sacrifice', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: true, description: "<p><em>Your character must have purchased the Dark Insight talent to benefit from this talent.</em> Before your character makes a magic skill check, they may suffer a number of wounds to use this talent to add an equal number of [success] to the check. The number cannot exceed your character’s ranks in Blood Sacrifice.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Bulwark', tier: 2)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p><em>Your character must have purchased the Parry talent to benefit from this talent.</em> While wielding a weapon with the Defensive quality, your character may use Parry to reduce the damage of an attack targeting an engaged ally.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Chill of Nordros', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p><em>Your character cannot take this talent if they have taken the Flames of Kellos talent.</em> When casting an Attack spell, your character may add the Ice effect without increasing the difficulty. Your character can never add the Fire effect.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Dirty Tricks', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>After your character inflicts a Critical Injury on an adversary, they may use this talent to upgrade the difficulty of that adversary’s next check.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Dominion of the Dimora', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p><em>Your character cannot take this talent if they have taken the Favor of the Fae talent.</em> When casting an Attack spell, your character may add the Impact effect without increasing the difficulty. Your character can never add the Manipulative effect.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Encouraging Song', tier: 2)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>While equipped with a musical instrument, your character may use this talent to make an Average ([difficulty][difficulty]) Charm or Verse check. For each [success] the check generates, one ally within medium range adds [boost] to their next skill check. For each [advantage], one ally benefiting from Encouraging Song heals 1 strain.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Exploit', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: true, description: "<p>When your character makes a combat check with a Ranged or Melee (Light) weapon, they may suffer 2 strain to use this talent to add the Ensnare quality to the attack. The rating of the Ensnare quality is equal to your character’s ranks in Exploit.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Favor of the Fae', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p><em>Your character cannot take this talent if they have taken the Dominion of the Dimora talent.</em> When casting an Attack spell, your character may add the Manipulative effect without increasing the difficulty. Your character can never add the Impact effect.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Flames of Kellos', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p><em>Your character cannot take this talent if they have taken the Chill of Nordros talent.</em> When casting an Attack spell, your character may add the Fire effect without increasing the difficulty. Your character can never add the Ice effect.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Flash of Insigt', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When your character generates [triumph] on a knowledge skill check, roll [boost] [boost] and add the results to the check, in addition to spending the [triumph] as usual.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Grapple', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Your character may suffer 2 strain to use this talent. Until the start of your character's next turn, enemies must spend two maneuvers to disengage from your character.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Heroic Recovery', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When your character acquires this talent, choose one characteristic. Once per encounter, you may spend one Story Point to use this talent to have your character heal strain equal to the rating of the chosen characteristic.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Hunter', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Knowledge (Geography), Ranged, and Survival are now career skills for your character.</p>")
skill_1 = Skill.find_by(name: 'Survival')
skill_2 = Skill.find_by(name: 'Ranged')
skill_3 = Skill.find_by(name: 'Geography')
modifications = {
  career_skill: [skill_1.id, skill_2.id, skill_3.id]
}
talent.update_attributes(modifications: modifications)
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Impaling Strike', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When your character inflicts a Critical Injury with a melee weapon, they may use this talent to immobilize the target until the end of the target’s next turn (in addition to the other effects of the Critical Injury).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Natural Communion', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When your character uses the Conjure magic action, the spell gains the Summon Ally effect without increasing the difficulty. All creatures your character summons must be naturally occurring animals native to the area.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Reckless Charge', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>After using a maneuver to move engage an adversary, your character may suffer 2 strain to use this talent. They then add [success] [success] [threat] [threat] to the results of the next Brawl, Melee (Light), or Melee (Heavy) combat check they make this turn.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Runic Lore', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Knowledge (Lore) and Runes are now career skills for your character.</p>")
skill_1 = Skill.find_by(name: 'Runes')
skill_2 = Skill.find_by(name: 'Lore')
modifications = {
  career_skill: [skill_1.id, skill_2.id]
}
talent.update_attributes(modifications: modifications)
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Shapeshifter (Improved)', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Once per session, your character may make a Hard ([difficulty][difficulty][difficulty]) Discipline check as an out-of-turn incidental either to trigger Shapeshifter or to avoid triggering it when they exceed their strain threshold.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Signature Spell', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When your character gains this talent, decide on a signature spell for them, consisting of a particular magic action and a specific set of one or more effects. When your character casts their signature spell (consisting of the exact combination of action and effects previously chosen), reduce the difficulty of the check by one.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Templar (Improved)', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p><em>Your character must have purchased the Templar talent to benefit from this talent.</em> When your character casts the single Divine spell per encounter granted by the Templar talent, they do not add [setback] for wearing heavy armor (armor with +2 soak or higher), using a shield, or not having at least one hand free (see Table III.2–3: Penalties When Casting Spells, on page 210 of the Genesys Core Rulebook).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Threaten', tier: 2)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: true, description: "<p>After an adversary within short range of your character resolves a combat check that deals damage to one of your character’s allies, your character may suffer 3 strain to use this talent to inflict a number of strain on the adversary equal to your character’s ranks in Coercion. The range of this talent increases by one band per rank of Threaten beyond the first.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Well Traveled', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Knowledge (Geography), Negotiation, and Vigilance are now career skills for your character.</p>")
skill_1 = Skill.find_by(name: 'Vigilance')
skill_2 = Skill.find_by(name: 'Negotiation')
skill_3 = Skill.find_by(name: 'Geography')
modifications = {
  career_skill: [skill_1.id, skill_2.id, skill_3.id]
}
talent.update_attributes(modifications: modifications)
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Wraithbane', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Your character counts the Critical rating of their weapon as one lower to a minimum of 1 when making an attack targeting an undead adversary.</p>")
puts "Talent: #{talent.name}"

puts '# Tier 3 #'

talent = Talent.find_or_create_by(name: 'Backstab', tier: 3)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>Your character may use this talent to attack an unaware adversary using a Melee (Light) weapon. A Backstab is a melee attack, and follows the normal rules for performing a combat check (see page 101 of the Genesys Core Rulebook), using the character’s Skulduggery skill instead of Melee (Light). If the check succeeds, each uncanceled [success] adds +2 damage (instead of the normal +1).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Battle Casting', tier: 3)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Your character does not add [setback] to magic skill checks for wearing heavy armor (armor with +2 soak or higher), using a shield, or not having at least one hand free (see Table III.2–3: Penalties When Casting Spells, on page 210 of the Genesys Core Rulebook).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Body Guard', tier: 3)
talent.update_attributes(activation: 'Active (Maneuver)', ranked: true, description: "<p>Once per round, your character may suffer a number of strain no greater than their ranks in Body Guard to use this talent. Choose one ally engaged with your character; until the end of your character’s next turn, upgrade the difficulty of all combat checks targeting that ally a number of times equal to the strain suffered.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Cavalier', tier: 3)
talent.update_attributes(activation: 'Active (Maneuver)', ranked: false, description: "<p>While riding a mount trained for battle (typically a war mount [see page 105] or flying mount [see page 104]), once per round your character may use this talent to direct the mount to perform an action.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Counterattack', tier: 3)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p><em>Your character must have purchased the Improved Parry talent to benefit from this talent.</em> When your character uses the Improved Parry talent to hit an attacker, they may also activate an item quality of the weapon they used as if they had generated [advantage] [advantage] on a combat check using that weapon.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Dissonance', tier: 3)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>While wielding a musical instrument, your character may use this talent to make an Average ([difficulty][difficulty]) Charm or Verse check. For each [success] the check generates, one enemy of the player’s choosing within medium range suffers 1 wound. For each [advantage], one enemy affected by Dissonance suffers 1 additional wound.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Dual Strike', tier: 3)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When resolving a combined check to attack with two weapons in a melee combat, your character may suffer 2 strain to use this talent to hit with the secondary weapon (instead of spending [advantage] [advantage]).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Easy Prey', tier: 3)
talent.update_attributes(activation: 'Active (Maneuver)', ranked: false, description: "<p>Your character may suffer 3 strain to use this talent. Until the start of your character’s next turn, your character and allies within short range add [boost] [boost] to combat checks against immobilized targets.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Justice of the Citadel', tier: 3)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Once per round on your character’s turn, your character may suffer 3 strain to use this talent to add damage equal to their ranks in Discipline to one hit of a successful melee attack.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Potent Concoctions', tier: 3)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When your character makes an Alchemy check that generates [triumph], roll an additional [proficiency] and add its results to the pool, in addition to spending the [triumph] normally. When your character makes an Alchemy check that generates [despair], roll an additional [challenge] and add its results to the pool, in addition to spending the [despair] normally. Each of these effects can occur only once per check.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Precise Archery', tier: 3)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When making a Ranged combat check targeting a character engaged with one of your character's allies, downgrade the difficulty of the check once (thus negating the penalty for shooting at engaged targets).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Pressure Point', tier: 3)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When your character makes an unarmed Brawl check targeting a living opponent, they may use this talent to deal strain damage instead of wound damage, and inflict additional strain damage equal to their ranks in Medicine.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Shockwave', tier: 3)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Your character treats Melee (Heavy) weapons as possessing the Blast item quality with a rating equal to your character’s ranks in Melee (Heavy). Your character does not suffer damage from their weapon's Blast quality (but allies do!).</p>")
puts "Talent: #{talent.name}"

puts '# Tier 4 #'

talent = Talent.find_or_create_by(name: 'Back-to-Back', tier: 4)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>While engaged with one or more allies, your character and allies they are engaged with add [boost] to combat checks. If one or more allies engaged with your character also have Back-to- Back, the effects are cumulative to a maximum of [boost] [boost].</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Conduit', tier: 4)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Once per encounter, your character may spend a Story Point to perform a magic action as a maneuver.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Death Rage', tier: 4)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Your character adds +2 damage to melee attacks for each Critical Injury they are currently suffering. (Your GM may also impost additional penalties on social skill checks your character makes if they are suffering Critical Injuries due to their frenzied behavior.)</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Venom Soaked Blade', tier: 4)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When making a Melee (Light) combat check using a poisoned weapon, your character treats it as possessing the Burn 2 item quality.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Signature Spell (Improved)', tier: 4)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p><em>Your character must have purchased the Signature Spell talent to benefit from this talent.</em> When your character casts their signature spell, reduce the difficulty of the check by two instead of one.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Unrelenting', tier: 4)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Once per round after resolving a successful Brawl, Melee (Light), or Melee (Heavy) combat check, your character may suffer 4 strain to use this talent to make an additional melee attack as an incidental against the same target. Increase the difficulty of the combat check by one if this attack uses a second weapon, or by two if the attack uses the same weapon.</p>")
puts "Talent: #{talent.name}"

puts '# Tier 5 #'

talent = Talent.find_or_create_by(name: 'Chrushing Blow', tier: 5)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Once per session after rolling a melee attack but before resolving the check, your character may suffer 4 strain to use this talent. While resolving the check, the weapon gains the Breach 1 and Knockdown item qualities, and destroys one item the target is wielding that does not have the Reinforced quality.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Let\'s Talk This Over' , tier: 5)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p>Once per game session, when a combat encounter against one or more sentient beings is about to begin, the character make a Daunting ([difficulty][difficulty][difficulty][difficulty]) Charm check. If successful, the combat encounter instead becomes a social encounter, with the PCs attempting to convince their opposition to back down, come around to their viewpoint, or accept a compromise. The GM is the final arbiter of how the situation resolves without violence (or how the combat encounter continues if the character’s check is unsuccessful).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Retribution!', tier: 5)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p>Once per round when an adversary attacks an ally within medium range, your character may spend one Story Point to use this talent to automatically hit that enemy once with a weapon your character is wielding, if the enemy is within the weapon’s range. The hit deals the weapon’s base damage, plus any damage from applicable talents or abilities.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Whirlwind', tier: 5)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>Your character may suffer 4 strain to use this talent to make a Brawl, Melee (Light), or Melee (Heavy) attack against the engaged adversary who is hardest to hit (as determined by the GM), increasing the difficulty by one. If the combat check succeeds, each adversary engaged with the character suffers one hit from the attack, that deals base damage plus damage equal to the total [success] scored on the check.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Zealous Fire', tier: 5)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Each time your Game Master spends a Story Point, your character heals 2 strain.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Necromancy', tier: 0)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When this character uses the conjure magic action, it gains the Summon Ally effect with no increase in difficulty; all creatures the character summons must be undead.</p>")
puts "Talent: #{talent.name}"

puts '#############################'
puts '# Seeding Android Talents   #'
puts '#############################'
puts '# Tier 1 #'

talent = Talent.find_or_create_by(name: 'Corporate Drone', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Gain Knowledge (Society) or Negotiation as a career skill Once per session, may collect a small favor from any other member of a single corporation.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Custom Code', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Choose one icebreaker or piece of ice that they own. Icebreaker adds [ad] to checks. Piece of ice adds [t] to checks made against it.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Custom Rig', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: true, description: "<p>Choose one computer that they own. The amount of ice or icebreakers that they can have on that computer is increased by 1 per rank of Custom Rig.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Customer Service Experience', tier: 1)
talent.update_attributes(activation: 'Active (Incidental)', ranked: true, description: "<p>After making a Charm check, may suffer 1 strain to cancel [t] per rank in Customer Service Experience.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Deep Pockets', tier: 1)
talent.update_attributes(activation: 'Active (Maneuver)', ranked: false, description: "<p>Once per session, may produce a small narratively useful item from their belongings worth up to 100 credits, max encumbrance 1 (GM's discretion).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Disenfrancisto', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Gain Streetwise or Survival as a career skill. Once per session, may collect a small favor from any other disenfrancisto.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Former Professor', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Gain one Knowledge skill as a career skill. Once per session, may collect a small favor from a member of an institute of higher learning.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Hand on the Throttle', tier: 1)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Once per round while driving or piloting a vehicle, may use this talent to increase or decrease its speed by 1, to a minimum of 0 or a maximum of the vehicle’s max speed.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Iaijutsu Training', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Drawing a Melee weapon for the first time in an encounter, increases the weapon’s damage by 2 for the remainder of the turn.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Knockout Punch', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Add the Stun quality to Brawl combat checks with a rating equal to 2 plus ranks in Coordination. Does not stack with other instances of the Stun quality.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Net Search', tier: 1)
talent.update_attributes(activation: 'Active (Maneuver)', ranked: false, description: "<p>When having access to the Network, upgrade the ability of the next Knowledge check that turn twice, and the difficulty once. Use [des] to let character learn something believable but completely false.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Resourceful Mechanic', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: true, description: "<p>When making a Mechanics check to repair system strain or hull trauma on a vehicle, repair one additional system strain or hull trauma per rank of Resourceful Mechanic.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Street Fighter', tier: 1)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When disorienting or knocking a target prone with a Brawl attack, the target suffers wounds equal to ranks in Skulduggery.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Tri-Maf Contact', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Gain Melee or Skulduggery as a career skill. In addition, once per session your character may collect a small favor from a member of a single orgcrime group.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Union Member', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Gain Athletics, Mechanics, or Operating as a career skill. In addition, once per session, your character may collect a small favor from a member of Humanity Labor or Human First.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Worlds War Vet', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Gain Ranged (Heavy) or Resilience as a career skill. In addition, once per session, your character may collect a small favor from a current or former member of a single country’s military.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Years on the Force', tier: 1)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Gain Perception or Ranged (Light) as a career skill. In addition, once per session your character may collect a small favor from a current or former member of the NAPD or New Angeles city government.</p>")
puts "Talent: #{talent.name}"

puts '# Tier 2 #'

talent = Talent.find_or_create_by(name: 'Bad Cop', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: true, description: "<p>May spend [ad][ad] from a Coercion or Deception check to use this talent to upgrade the ability of a single ally’s subsequent social skill check a number of times equal to your character’s ranks in Bad Cop.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Big Guns', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Encumbrance threshold is 10 plus Brawn, instead of 5 plus Brawn. Cumbersome rating of any weapon they carry is decreased by 1, to a minimum of 3.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Codeslinger', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When performing the activate program maneuver in a hacking encounter, you can choose not to deactivate one other active icebreaker. You may have two icebreakers active at once.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Combat Medicine', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: true, description: "<p>Before making a Medicine check, your character may use this talent to add s equal to their ranks in Combat Medicine to the results. After the check is resolved, the target suffers 2 strain for each rank your character has in Combat Medicine.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Determined Driver', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>You may spend a Story Point to use this talent to have your character heal system strain on a vehicle they are currently driving, piloting, or operating equal to their ranks in Driving, Piloting, or Operating (choose the skill used to direct the vehicle).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Good Cop', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: true, description: "<p>May spend [ad][ad] from a Charm or Negotiation check to use this talent to upgrade the ability of a single ally’s subsequent social skill check a number of times equal to your character’s ranks in Good Cop.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Haughty Demeanor', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Other characters add [t] to social skill checks targeting your character.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Nethunter', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When this character successfully traces another charater during a Network encounter, they gain one additional trace.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Parkour!', tier: 2)
talent.update_attributes(activation: 'Active (Maneuver)', ranked: false, description: "<p>Once per round, your character may suffer 1 strain to use this talent and move to any location within Short range.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Probing Question', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>If your character knows an opponent’s Flaw or Fear motivation, when your character inflicts strain on that opponent using a social skill, the opponent suffers 3 additional strain.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Quick Fix', tier: 2)
talent.update_attributes(activation: 'Active (Maneuver)', ranked: false, description: "<p>You may spend a Story Point to allow your character to use this talent to temporarily repair one damaged item they are engaged with.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Special Use Permit', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Your character does not treat any Ranged (Heavy) weapons as restricted (R).</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Tactical Focus', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When performing a combat check with a Ranged (Heavy) weapon, if your character did not perform a maneuver to ready or stow a weapon or item during this turn, they add [ad] to the results.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Two-Handed Stance', tier: 2)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>When performing a combat check with a Ranged (Light) weapon, if your character has nothing in their other hand, they add [ad] to the results.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Undercity Contacts', tier: 2)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Once per session, you may spend one Story Point use this talent to let your character learn if a character of your choice is in New Angeles, and if so, what district.</p>")
puts "Talent: #{talent.name}"

puts '# Tier 3 #'

talent = Talent.find_or_create_by(name: 'Applied Research', tier: 3)
talent.update_attributes(activation: 'Active (Incidental)', ranked: true, description: "<p>Your character may use this talent before making a check to use any knowledge skill and Intellect instead of the skill and characteristic the check would normally require. Your character may use this talent a number of times per session equal to their ranks in Applied Research.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Bad Habit', tier: 3)
talent.update_attributes(activation: 'Active (Maneuver)', ranked: false, description: "<p>Your character may use this talent to become disoriented for the remainder of the encounter. At the beginning of each of your character’s turns, if they are still disoriented due to this talent, they heal 2 strain.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Dumb Luck', tier: 3)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Once per session, you may spend a Story Point to use this talent after your character suffers a Critical Injury but before the result is rolled. Their opponent must roll two results, and you select which applies to your character.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Hard Boiled', tier: 3)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When your character makes a check to recover strain at the end of an encounter, your character may make a Simple (-) Resilience check instead of Discipline or Cool. If your character does so, they heal 1 strain per [su] and 1 wound per [ad].</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Hold it Steady', tier: 3)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p>Before performing a combat check using a weapon with the Auto-fire quality, your character may use this talent to use the Auto-fire quality without increasing the difficulty of the combat check. If they do so, each time they trigger an additional hit during the attack, they suffer 2 strain.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Laugh it Off', tier: 3)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p>When your character is targeted by a social check that they may use this talent to spend [t][t][t] or [des] to reduce any strain the check inflicts by a number equal to their ranks in Charm. If they do, the character who targeted them suffers an amount of strain equal to the amount of strain reduced.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Martial Weapons Master', tier: 3)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>While armed with a Melee weapon, your character may use this talent to make an Average ([d][d]) Melee check. If successful, your character may force one engaged target to either drop one weapon they are holding or move one range band in a direction of your choosing.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Net Warrior', tier: 3)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>While accessing a system using a brain-machine interface (BMI), your character may use this talent to make an opposed Computers (Hacking) versus Computer (Sysops) check targeting one other character on the system that they are aware of. The target suffers 1 strain per [su], and if they are using a BMI, they also suffer 1 wound per [su].</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Nimble', tier: 3)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>At the start of your character’s turn, you may spend one Story Point to use this talent to allow your character to perform a move maneuver as an incidental. (This does not count against the limit of two maneuvers per turn.) If you use this talent, your character can only perform one additional move maneuver during this turn.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Snare', tier: 3)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>Once per session, your character may use this talent to make a Hard ([d][d][d]) Computers (Sysops) check. If they succeed, once before the end of the encounter, you may spend a Story Point to force one character in the encounter to make a Daunting ([d][d][d][d]) Vigilance check as an incidental. If they fail, they are staggered until the end of their next turn, plus one additional turn per [t][t].</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Suppressing Fire', tier: 3)
talent.update_attributes(activation: 'Active (Maneuver)', ranked: true, description: "<p>If your character does not make a combat check during their turn, they may use this talent to target one character (or minion group) within long range. That character must upgrade the difficulty of any ranged combat checks they make once until the end of your character’s next turn. Your character may choose to affect one additional character for each additional rank of Suppressing Fire.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Takedown', tier: 3)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>Your character may use this talent to make an opposed Brawl vs. Resilience check targeting one engaged opponent. If the check succeeds, the target is knocked prone and immobilized until the end of your character’s next turn. If the target is a minion or rival, your character may spend [tri] to incapacitate (but not kill) the target instead.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Undercity Contacts (Improved)', tier: 3)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Your character must have purchased the Undercity Contacts talent to benefit from this talent. When you use Undercity Contacts, you may choose to spend two Story Points instead of one. If you do, your character learns the target’s specific location.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'You Owe Me One', tier: 3)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Once per session, you may spend two Story Points to use this talent to have one NPC in the current encounter owe your character a favor. If the favor is not resolved by the end of the encounter, it is forgotten.</p>")
puts "Talent: #{talent.name}"

puts '# Tier 4 #'

talent = Talent.find_or_create_by(name: 'Burn Through', tier: 4)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>After making a successful break ice action, your character may suffer 3 strain to use this talent. If they do, they may perform a second override ice action on the same system as an incidental.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Elementary', tier: 4)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>Once per session, your character may use this talent to make a Hard ([d][d][d]) Perception check while present at a crime scene. If they succeed, they identify all prominent physical characteristics of one person who was at the crime scene when the crime was committed (as long as the crime was committed in the past 48 hours). This could include a person’s height, weight, body type, clothing, and if they are human or not. Your character may identify all the physical characteristics of one additional person present at the crime scene per additional [su].</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Offensive Driving', tier: 4)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>While driving or piloting a vehicle, your character may use this talent to select one other vehicle within medium range and make an opposed Driving or Piloting verus Driving or Piloting check targeting the other vehicle’s driver or pilot. If successful, roll twice on the Critical Hit Result table. Choose one Critical Hit result to apply to your character’s vehicle, and the other to apply to the other vehicle. You may spend [tri] to add +20 to one Critical Hit result. Your GM may spend [des] to add +20 to both Critical Hit results.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Parkour! (Improved)', tier: 4)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Your character must have purchased the Parkour! talent to benefit from this talent. Once per round, when using the Parkour! talent, your character may suffer 4 strain instead of 1 strain to move to any location within medium range instead of short range. All other restrictions of Parkour! apply to this movement.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Quick Witted', tier: 4)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p>Once per encounter, after another character makes a social skill check, your character may use this talent to make an Average ([d][d]) Vigilance check. If successful, you may add a number of [su] or [ad] (your choice) equal to your character’s ranks in Charm to the other character’s check. If your character fails, your character suffers 3 strain.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Urban Combatant', tier: 4)
talent.update_attributes(activation: 'Active (Incidental, Out of Turn)', ranked: false, description: "<p>When your character is targeted by a combat check while in an urban environment, you may spend one Story Point to use this talent before the dice pool is rolled. If you do so, your character’s opponent removes all [s] added to the check, and instead adds an equal number of [f] to the results.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'You Owe Me One (Improved)', tier: 4)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>Your character must have purchased the You Owe Me One talent to benefit from this talent. Once per session, you may spend two Story Points to use You Owe Me One to have one NPC in the current encounter ow your character a big favor instead of a favor. If the big favor is not resolved by the end of the encounter, it is forgotten.</p>")
puts "Talent: #{talent.name}"

puts '# Tier 5 #'

talent = Talent.find_or_create_by(name: 'Drone Master', tier: 5)
talent.update_attributes(activation: 'Passive', ranked: false, description: "<p>Your character may control two drones or minion groups of drones no larger than your character’s Willpower. Your character resolves each drone’s (or minion group’s) turn individually, choosing the order in which they activate.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Ghost in the Machine', tier: 5)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>As long as they have some sort of access point to the Network, your character may use this talent to make a Hard ([d][d][d]) Computers (Hacking) check. If they succeed, they may select one drone, vehicle, or piece of equipment involved in the current encounter and dictate its actions until the start of your character’s next turn.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Master Plan', tier: 5)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>Once per session, your character may use this talent to make a Hard ([d][d][d]) Discipline check. If they succeed, they reveal that whatever terrible circumstances they currently find themselves in are all part of a brilliant plan that they established at an earlier point. They then choose one non-nemesis adversary in the encounter and reveal them to be a close friend or ally who has positioned themselves to help your character at this exact moment.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Trick of the Light', tier: 5)
talent.update_attributes(activation: 'Active (Incidental)', ranked: false, description: "<p>When making a combat check with a laser or maser weapon, your character may use this talent to spend [ad] to inflict one additional hit with this weapon, dealing base damage plus damage equal to the total number of [su] scored on the check. This hit may target the original target or another target within short range of the original target.</p>")
puts "Talent: #{talent.name}"

talent = Talent.find_or_create_by(name: 'Web of Knowledge', tier: 5)
talent.update_attributes(activation: 'Active (Action)', ranked: false, description: "<p>Once per session your character may make an Average ([d][d]) Knowledge (Net) check during a Network encounter. If you succeed, your character knows the names, strengths, and other qualities of all ice (active or deactivated) on one system that you currently have access to, as well as all other characters (sysops and runners) that currently are accessing that system.</p>")
puts "Talent: #{talent.name}"
